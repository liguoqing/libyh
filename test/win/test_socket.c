#include "test_socket.h"
#include "api_param_struct.h"
#include "base/thread.h"
#include "base/singnal.h"
#include "net/socket_interface.h"
#include "net/socket_utility.h"
#include "net/tcp_device.h"
#include "io/event_loop.h"


static _int32 yh_test_tcp_device_send_callback(SockSendReq *req);
static _int32 yh_test_tcp_device_close_callback_for_server(SockClosReq *req);
static _int32 yh_test_tcp_device_close_callback_for_client(SockClosReq *req);
static _int32 yh_test_tcp_device_recv_callback_for_server(SockRecvReq *req);
static _int32 yh_test_tcp_device_recv_callback_for_client(SockRecvReq *req);
static _int32 yh_test_asyn_test_sock_op_for_server_helper(
	TcpDevice *tcp_device, _int32 *err_code);
static _int32 yh_test_asyn_test_sock_op_for_client_helper(char *send_data);


/**
 * 测试是否结束的事件
 **/
static YHHandle gWaitForTestFinished = INVALID_HANDLE_VALUE;
static const char *gBeginTag = "begin";


_int32 yh_test_socket_create()
{
    _int32 _ret = YH_SUCCESS;
    Socket *_sock_tcp = NULL, *_sock_udp = NULL;

	_ret = yh_socket_startup();

    _ret = yh_socket_create(YH_TCP, &_sock_tcp);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_sock_tcp->_fd != INVALID_SOCKET);
	YH_ASSERT(_sock_tcp->_poll == 0);
	YH_ASSERT(_sock_tcp->_type == YH_TCP);

    _ret = yh_socket_create(YH_UDP, &_sock_udp);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_sock_udp->_fd != INVALID_SOCKET);
	YH_ASSERT(_sock_udp->_type == YH_UDP);

    _ret = yh_socket_close(_sock_tcp);
    YH_CHECK_VALUE(_ret);

    _ret = yh_socket_close(_sock_udp);
    YH_CHECK_VALUE(_ret);

	_ret = yh_socket_cleanup();

    return _ret;
}


static _int32 yh_test_socket_op_server_in_syn()
{
    _int32 _ret = YH_SUCCESS;
    Socket *_sock_s = NULL;
    Socket *_sock_new = NULL;
    char *_buffer = NULL;
	_int32 _errcode = YH_SUCCESS;
	struct sockaddr_in _new_addr = {0};
#if defined(_DEBUG)
    char _log_sock_new_addr[MAX_IP_STR_LENGTH] = {0};
#endif

	LOG("enter...");

    _ret = yh_socket_create(YH_TCP, &_sock_s);
    YH_CHECK_VALUE(_ret);

    _ret = yh_socket_listen(_sock_s, "127.0.0.1", TCP_LISTEN_PORT);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_sock_s != NULL);
    YH_ASSERT(_sock_s->_fd != INVALID_SOCKET);

    _ret = yh_socket_accept(_sock_s, &_sock_new, &_new_addr);

    if (_ret == YH_SUCCESS) {
        YH_ASSERT(_sock_new != NULL);
        YH_ASSERT(_sock_new->_fd != INVALID_SOCKET);
        YH_ASSERT(_sock_new->_type == YH_TCP);
#if defined(_DEBUG)
        _ret = yh_socket_ip_ulong2str(_new_addr.sin_addr.s_addr,
			&_log_sock_new_addr[0], MAX_IP_STR_LENGTH);
        YH_CHECK_VALUE(_ret);
        LOG("accept a socket[%s:%hu]", _log_sock_new_addr, 
			ntohs(_new_addr.sin_port));
#endif
		YH_MALLOC(_buffer, char *, MAX_SOCKET_BUFFER_SIZE);

		while(1) 
		{
			// block mode
			_ret = yh_socket_recv(_sock_new, _buffer, MAX_SOCKET_BUFFER_SIZE, 
				&_errcode);
			if (_ret == YH_ERR_SOCKET_OP_FAILURE || 
				_ret == YH_ERR_TCP_CONN_RMT_CLOSED) {
				_ret = yh_socket_close(_sock_new);
				YH_CHECK_VALUE(_ret);
				break;
			}

			LOG("%s.\n", _buffer);

			memset(_buffer, 0, MAX_SOCKET_BUFFER_SIZE);
		}
    }
    else {
        LOG("can't accept a socket!");
        YH_ASSERT(FALSE);
    }

	_ret = yh_socket_close(_sock_s);
	YH_CHECK_VALUE(_ret);

    return YH_SUCCESS;
}


static _int32 yh_test_socket_op_client_in_syn()
{
    _int32 _ret = YH_SUCCESS;
    Socket *_sock_c = NULL;
    char *_buffer = NULL;
    const char *_server_ip = "127.0.0.1";
    const _uint16 _server_port = TCP_LISTEN_PORT;

    _ret = yh_socket_create(YH_TCP, &_sock_c);
    YH_CHECK_VALUE(_ret);

    _ret = yh_socket_connect(_sock_c, _server_ip, _server_port);
    YH_CHECK_VALUE(_ret);

	YH_MALLOC(_buffer, char *, MAX_SOCKET_BUFFER_SIZE);

    if (_ret == YH_SUCCESS) {
        while (fscanf(stdin, "%s", _buffer) != EOF) {
			LOG("start to send %d bytes.", strlen(_buffer));
            
			_ret = yh_socket_send(_sock_c, _buffer, strlen(_buffer));
			YH_CHECK_VALUE(_ret);
			
            memset(_buffer, 0, MAX_SOCKET_BUFFER_SIZE);
        }

		_ret = yh_socket_close(_sock_c);
		YH_CHECK_VALUE(_ret);
    }

    YH_DELETE_OBJ(_buffer);

    return YH_SUCCESS;
}


_int32 yh_test_socket_op_in_syn(_int32 type)
{
    _int32 _ret = YH_SUCCESS;

	LOG("type: %d", type);
    
	_ret = yh_socket_startup();
	YH_CHECK_VALUE(_ret);

    if (type == TEST_SOCKET_CLIENT) {
        yh_test_socket_op_client_in_syn();
    }
    else if (type == TEST_SOCKET_SERVER) {
        yh_test_socket_op_server_in_syn();
    }
    else {
        YH_ASSERT(FALSE);
    }

	_ret = yh_socket_cleanup();
	YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 yh_test_tcp_device_connect_callback(SockConnReq *req)
{
    _int32 _ret = YH_SUCCESS;
	TcpDevice *_tcp_device = NULL;
	char *_send_data = NULL;
	char *_recv_buffer = NULL;

    YH_ASSERT(req != NULL);
    YH_ASSERT(req->_base->_device != NULL);
    YH_CHECK_PARAM_VALID(req);

    LOG("req:0x%x, err_code:%d.", req, req->_base->_err_code);

    if (req->_base->_err_code != YH_SUCCESS) {
        LOG("connect failure, err_code:%d ,so reconnect...", 
            req->_base->_err_code);
        // TODO
        // 这种情况，改变socket重连也不能解决问题。。。
        /*if (req->_base->_err_code == ERROR_DUP_NAME) {
            _ret = yh_tcp_device_reconnect(
                (TcpDevice *)req->_base->_device, req);
            CHECK_VALUE(_ret);
            YH_CHECK_VALUE(_ret);
        }*/
        return YH_ERR_SOCKET_OP_FAILURE;
    }

	_tcp_device = (TcpDevice *)req->_base->_device;

    // 获取控制台输入 -- 作为demo
	YH_MALLOC(_send_data, char *, MAX_SOCKET_BUFFER_SIZE);
    _ret = yh_test_asyn_test_sock_op_for_client_helper(_send_data);

    if (_ret == YH_SUCCESS) {
        _ret = yh_tcp_device_send(_tcp_device, _send_data, strlen(_send_data), 
			NULL, yh_test_tcp_device_send_callback);
        YH_CHECK_VALUE(_ret);

		/**
		 * 期望接收刚才发送出去的字符长度
		 **/
		YH_MALLOC(_recv_buffer, char *, MAX_SOCKET_BUFFER_SIZE);
        _ret = yh_tcp_device_recv(_tcp_device, _recv_buffer, strlen(_send_data), 
			NULL, yh_test_tcp_device_recv_callback_for_client);
        YH_CHECK_VALUE(_ret);
    }
    else {		
		if (_tcp_device->_send_buf) {
			YH_DELETE_OBJ(_tcp_device->_send_buf);
			_tcp_device->_send_buf = NULL;
		}
		else {
			YH_DELETE_OBJ(_send_data);
			_send_data = NULL;
		}

        // 应该在这里关闭socket
        // NOTE
        // 这里有一个关键点
        // 如果connect之后立即关闭了，那么是否还能send出去？如果已经send了
        // 此时关闭会发生什么情况？	
        _ret = yh_tcp_device_close(_tcp_device, NULL,
            yh_test_tcp_device_close_callback_for_client);
        YH_CHECK_VALUE(_ret);
    }  

    return _ret;
}


static _int32 yh_test_tcp_device_accept_callback(SockAcceReq *req)
{
    _int32 _ret = YH_SUCCESS;
    TcpDevice *_acce_device = NULL;
    TcpDevice *_listen_device = NULL;
	char *_recv_buffer = NULL;

    YH_ASSERT(req != NULL);
    YH_ASSERT(req->_acc_sock_fd != INVALID_SOCKET);
    YH_ASSERT(req->_base->_device != NULL);

    LOG("req:0x%x, errcode:%d.", req, req->_base->_err_code);

    if (req->_base->_err_code != YH_SUCCESS) {
        return YH_ERR_SOCKET_OP_FAILURE;
    }

    _listen_device = (TcpDevice *)(req->_base->_device);

    _ret = yh_tcp_device_create(_listen_device->_device->loop, 
		req->_acc_sock_fd, &_acce_device);
    YH_CHECK_VALUE(_ret);

	YH_MALLOC(_recv_buffer, char *, MAX_SOCKET_BUFFER_SIZE);
	
	// 验证：
	// recv操作上是否一定会等待接收到所有的数据才返回？自然不是这样的
    // 在新device上发起recv操作
    _ret = yh_tcp_device_recv(_acce_device, _recv_buffer, 
		MAX_SOCKET_BUFFER_SIZE, NULL, 
		yh_test_tcp_device_recv_callback_for_server);
    YH_CHECK_VALUE(_ret);

    // 在老device上重新发起accept操作
	// 为了测试，这里不再继续监听了，等待结束后关闭
    /*_ret = yh_tcp_device_accept(_listen_device, 
        yh_test_tcp_device_accept_callback);
    YH_CHECK_VALUE(_ret);*/
	
	// 为了测试，这里将原来的device直接关掉
	// 我去，这里将原来的socket关闭后会导致客户端的socket无法发送数据
	/*_ret = yh_tcp_device_close(_listen_device, NULL, 
		yh_test_tcp_device_close_callback_for_server);
	YH_CHECK_VALUE(_ret);*/

    return _ret;
}


static _int32 yh_test_tcp_device_send_callback(SockSendReq *req)
{
    _int32 _ret = YH_SUCCESS;

    YH_ASSERT(req != NULL);
    YH_ASSERT(req->_base->_device != NULL);
    YH_CHECK_PARAM_VALID(req);

    LOG("req:0x%x, err_code:%d.", req, req->_base->_err_code);

    if (req->_base->_err_code != YH_SUCCESS) {
        LOG("send error, err_code:%d.", req->_base->_err_code);
        return YH_ERR_SOCKET_OP_FAILURE;
    }

    return _ret;
}


/**
 * server端收到数据后的回调
 * 作为demo，这里将收到的数据立即送回客户端，实现echo功能
 **/
static _int32 yh_test_tcp_device_recv_callback_for_server(SockRecvReq *req)
{
    _int32 _ret = YH_SUCCESS;
	TcpDevice *_device = NULL;

	YH_ASSERT(req != NULL);
	YH_ASSERT(req->_base->_device != NULL);

	LOG("req:0x%x, errcode:%d.", req, req->_base->_err_code);

	_device = (TcpDevice *)req->_base->_device;

	if (req->_base->_err_code != YH_SUCCESS) {
		if (req->_base->_err_code == YH_ERR_TCP_CONN_RMT_CLOSED) {
			LOG("remote client has been closed.");

			if (_device->_send_buf) {
				YH_DELETE_OBJ(_device->_send_buf);
				_device->_send_buf = NULL;
			}

			if (_device->_recv_buf) {
				YH_DELETE_OBJ(_device->_recv_buf);
				_device->_recv_buf = NULL;
			}

			_ret = yh_tcp_device_close(_device, NULL,
				yh_test_tcp_device_close_callback_for_server);
			YH_CHECK_VALUE(_ret);	

			return _ret;
		}

		return YH_ERR_SOCKET_OP_FAILURE;
	}

    LOG("recvd %s[%d].", _device->_recv_buf, strlen(_device->_recv_buf));

	if (!_device->_send_buf) {
		YH_MALLOC(_device->_send_buf, char *, MAX_SOCKET_BUFFER_SIZE);
	}

    memset(_device->_send_buf, 0, MAX_SOCKET_BUFFER_SIZE);
    memcpy(_device->_send_buf, _device->_recv_buf, 
        MIN(MAX_SOCKET_BUFFER_SIZE, strlen(_device->_recv_buf)));

    _ret = yh_tcp_device_send(_device, _device->_send_buf, 
		strlen(_device->_send_buf), NULL, yh_test_tcp_device_send_callback);
    YH_CHECK_VALUE(_ret);

    memset(_device->_recv_buf, 0, strlen(_device->_recv_buf));

    _ret = yh_tcp_device_recv(_device, _device->_recv_buf, 
		MAX_SOCKET_BUFFER_SIZE, NULL,
		yh_test_tcp_device_recv_callback_for_server);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 yh_test_asyn_test_sock_op_for_client_helper(char *send_data) 
{
    _int32 _ret = YH_UNKNOWN_ERR;

	YH_CHECK_PARAM_VALID(send_data);

    while (scanf("%s", send_data) != EOF) {
        if (strlen(send_data) > MAX_SOCKET_BUFFER_SIZE) {
            LOG("send too much data!");
            memset(send_data, 0, MAX_SOCKET_BUFFER_SIZE);
        }
        else {
            _ret = YH_SUCCESS;
            break;
        }     
    }

    return _ret;
}


/**
 * client端收到数据后的回调
 * 作为demo，这里将收到的数据立即在控制台回显，实现echo功能
 **/
static _int32 yh_test_tcp_device_recv_callback_for_client(SockRecvReq *req)
{
    _int32 _ret = YH_SUCCESS;
    TcpDevice *_device = NULL;

    YH_ASSERT(req != NULL);
    YH_ASSERT(req->_base->_device != NULL);

    LOG("req:0x%x, errcode:%d, recvd %d bytes.", 
        req, req->_base->_err_code, req->_recvd_bytes);

    if (req->_base->_err_code != YH_SUCCESS) {
        return YH_ERR_SOCKET_OP_FAILURE;
    }

    _device = (TcpDevice *)req->_base->_device;

    LOG("\n\n\n%s\n\n\n", _device->_recv_buf);

    _ret = yh_test_asyn_test_sock_op_for_client_helper(_device->_send_buf);

    if (_ret == YH_SUCCESS) {
        _ret = yh_tcp_device_send(_device, _device->_send_buf, 
			strlen(_device->_send_buf), NULL, 
			yh_test_tcp_device_send_callback);
        YH_CHECK_VALUE(_ret);

        memset(_device->_recv_buf, 0, MAX_SOCKET_BUFFER_SIZE);

        _ret = yh_tcp_device_recv(_device, _device->_recv_buf, 
			strlen(_device->_send_buf), NULL,
            yh_test_tcp_device_recv_callback_for_client);
        YH_CHECK_VALUE(_ret);
    }
    else {
        LOG("\n\nstart to close socket.");

		if (_device->_send_buf) {
			YH_DELETE_OBJ(_device->_send_buf);
			_device->_send_buf = NULL;
		}

		if (_device->_recv_buf) {
			YH_DELETE_OBJ(_device->_recv_buf);
			_device->_recv_buf = NULL;
		}

        // 应该在这里关闭socket
        // NOTE
        // 这里有一个关键点
        // 如果connect之后立即关闭了，那么是否还能send出去？如果已经send了，
        // 此时关闭会发生什么情况？	
        _ret = yh_tcp_device_close(_device, NULL,
            yh_test_tcp_device_close_callback_for_client);
        YH_CHECK_VALUE(_ret);
    }

    return _ret;
}


static _int32 yh_test_tcp_device_close_callback_helper(SockClosReq *req)
{
    _int32 _ret = YH_SUCCESS;

    TcpDevice *_device = NULL;

    YH_CHECK_PARAM_VALID(req);

    LOG("req:0x%x, errcode:%d.", req, req->_base->_err_code);

    if (req->_base->_err_code != YH_SUCCESS) {
        return YH_ERR_SOCKET_OP_FAILURE;
    }

    _device = (TcpDevice *)req->_base->_device;

    _ret = yh_tcp_device_destroy(_device);
    YH_CHECK_VALUE(_ret);

    return _ret;
}

static _int32 yh_test_tcp_device_close_callback_for_client(SockClosReq *req)
{
    _int32 _ret = YH_SUCCESS;

    _ret = yh_test_tcp_device_close_callback_helper(req);
    YH_CHECK_VALUE(_ret);

    _ret = yh_signal(gWaitForTestFinished);

    return _ret;
}


static _int32 yh_test_tcp_device_close_callback_for_server(SockClosReq *req)
{
	_int32 _ret = YH_SUCCESS;

    _ret = yh_test_tcp_device_close_callback_helper(req);

	_ret = yh_signal(gWaitForTestFinished);

	return _ret;
}


void *__stdcall yh_test_dl_thread_func(yh_thread *this, void *data)
{
    _int32 _api_ret = YH_SUCCESS;
    _ulong _cur_tid = 0;
    EventLoop *_loop = (EventLoop *)data;

	LOG("this: 0x%x, _loop: 0x%x", this, _loop);

    YH_ASSERT(_loop != NULL);

    _api_ret = yh_get_current_thread_id(&_cur_tid);
    YH_ASSERT(_api_ret == YH_SUCCESS);
    YH_ASSERT(_cur_tid != 0);

    LOG("thread_id:%u.", _cur_tid);

    _api_ret = yh_loop_run(_loop);
    YH_ASSERT(_api_ret == YH_SUCCESS);

    return this;
}


static _int32 yh_test_asyn_test_sock_op_for_server_helper(
    TcpDevice *tcp_device, _int32 *err_code)
{
    _int32 _ret = YH_SUCCESS;
    struct sockaddr_in _ser_addr = {0};
    const char *_server_ip = "127.0.0.1";
    const _uint16 _server_port = TCP_LISTEN_PORT;

    _ret = yh_socket_ip_str2ulong(_server_ip, &(_ser_addr.sin_addr.s_addr));
    YH_CHECK_VALUE(_ret);
    _ser_addr.sin_family = AF_INET;
    _ser_addr.sin_port = htons(_server_port);

    _ret = yh_tcp_device_listen(tcp_device, (const SOCKADDR *)&_ser_addr, 
        sizeof(_ser_addr), DEFAULT_TCP_BACKLOG, err_code);
    YH_CHECK_VALUE(_ret);

    _ret = yh_tcp_device_accept(tcp_device, 
        yh_test_tcp_device_accept_callback);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


/**
 * 在主线程中执行，处理test_asyn_sock_op函数
 **/
static _int32 yh_test_handle_test_asyn_sock_op_func_for_client(void *data)
{
    _int32 _ret = 0;
    OverlappedWrapper *_op = (OverlappedWrapper *)data;
	TestAsynSockParam *_param = NULL;
    EventLoop *_loop = NULL;
    TcpDevice *_tcp_device = NULL;
    struct sockaddr_in _ser_addr = {0};
    const char *_server_ip = "127.0.0.1";
    const _uint16 _server_port = TCP_LISTEN_PORT;

	LOG("enter...");

    YH_ASSERT(data != NULL);

	_param = (TestAsynSockParam *)_op->_msg->_data;
	YH_ASSERT(_param != NULL);

    _loop = _param->_loop;
    YH_ASSERT(_loop != NULL);

    LOG("test socket operation handler.");

    _ret = yh_tcp_device_create(_loop, INVALID_SOCKET, &_tcp_device);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_tcp_device != NULL);
    YH_ASSERT(_tcp_device->_device != NULL);
    YH_ASSERT(_tcp_device->_sock != NULL);
    YH_ASSERT(_tcp_device->_device->_type == TCP_DEVICE);
    YH_ASSERT(_tcp_device->_device->loop == _loop);

	/**
	 * NOTE
	 * tcp_device不再维护发送缓冲区，转而由上层来维护，即device是一个无
	 * 状态的对象。在正式环境中，有一个connection对象存在，这里面会维护
	 * 发送的缓存区。
	 **/

    _ret = yh_socket_ip_str2ulong(_server_ip, &(_ser_addr.sin_addr.s_addr));
    YH_CHECK_VALUE(_ret);
    _ser_addr.sin_family = AF_INET;
    _ser_addr.sin_port = htons(_server_port);

    _ret = yh_tcp_device_connect(_tcp_device, &_ser_addr, sizeof(_ser_addr), 
		NULL, yh_test_tcp_device_connect_callback);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


/**
 * 在主线程中执行，处理test_asyn_sock_op_server函数
 **/
static _int32 yh_test_handle_test_asyn_sock_op_func_for_server(void *data)
{
    _int32 _ret = YH_SUCCESS;
    _int32 _err_code = 0;
    OverlappedWrapper *_op = (OverlappedWrapper *)data;
    TestAsynSockParam *_param = NULL;
    EventLoop *_loop = NULL;
    TcpDevice *_tcp_device = NULL;

	LOG("enter...");

    YH_ASSERT(data != NULL);

    _param = (TestAsynSockParam *)_op->_msg->_data;
    YH_ASSERT(_param != NULL);

    _loop = _param->_loop;
    YH_ASSERT(_loop != NULL);

    _ret = yh_tcp_device_create(_loop, INVALID_SOCKET, &_tcp_device);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_tcp_device != NULL);
    YH_ASSERT(_tcp_device->_device != NULL);
    YH_ASSERT(_tcp_device->_device->_type == TCP_DEVICE);
    YH_ASSERT(_tcp_device->_device->loop == _loop);

    _ret = yh_test_asyn_test_sock_op_for_server_helper(_tcp_device, &_err_code);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


/**
 * 1、启动一个looper作为主线程
 * 2、再启动一个线程，当做外部使用者，当前线程即作为外部使用者了
 * 3、然后外部线程请求主线程的服务 -- send hello to server
 * 4、然后外部线程关闭主线程，等待主线程的关闭
 * 5、最后主线程关闭、外部线程结束，测试完成
 **/
static _int32 yh_test_socket_op_client_in_asyn()
{
    _int32 _ret = YH_SUCCESS;
    yh_thread *_dl_thread = NULL;
    EventLoop *_dl_loop = NULL;
    BOOL _dl_loop_running = FALSE;
    OverlappedWrapper *_test_asyn_sock_op = NULL;
    TestAsynSockParam *_test_asyn_sock_param = NULL;

	_ret = yh_create_event(&gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

    _ret = yh_loop_create(&_dl_loop);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_dl_loop != NULL);

    // 启动主线程
    _ret = yh_thread_create(&_dl_thread, yh_test_dl_thread_func, 
		(void *)_dl_loop);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_dl_thread->_data != NULL);

    // 从此处开始本线程当做一个外部使用者
    // 按理说，主线程应该导出接口供外部调用，但是这里只是用来测试
    // 所以，直接向主线程投递消息，转到主线程内部使用，以此来模拟
    // 这种API调用

    // 无法确保主线程已经开启，所以这里需要判断其是否已经开启了
    _ret = yh_loop_is_running(_dl_loop, &_dl_loop_running);
    while (!_dl_loop_running) {
        Sleep(100);
        _ret = yh_loop_is_running(_dl_loop, &_dl_loop_running);
    }

	LOG("The dl thread is running, so call test_asyn_sock_op_func_for_client.");

	YH_MALLOC(_test_asyn_sock_param, TestAsynSockParam *, 
		sizeof(TestAsynSockParam));
    _test_asyn_sock_param->_loop = _dl_loop;

    _ret = yh_create_overlapped_wrapper(&_test_asyn_sock_op, MT_POST_FUNC, 
        yh_test_handle_test_asyn_sock_op_func_for_client, 
		yh_message_default_delete_handler, (void *)_test_asyn_sock_param);
    YH_CHECK_VALUE(_ret);

    _ret = yh_post_queued_completion_status(_dl_loop->_iocp, 0, 0, 
        (LPOVERLAPPED)&(_test_asyn_sock_op->_overlapped));
    YH_CHECK_VALUE(_ret);

    _ret = yh_wait(gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

	_ret = yh_loop_destroy(_dl_loop);
	YH_CHECK_VALUE(_ret);

	_ret = yh_thread_destroy(_dl_thread);
	YH_CHECK_VALUE(_ret);

    LOG("Test success!");

    return _ret;
}


/**
 * 1、启动一个looper作为主线程
 * 2、再启动一个线程，当做外部使用者，当前线程即作为外部使用者了
 * 3、然后外部线程请求主线程的服务 -- listen on a special port
 * 4、然后外部线程关闭主线程，等待主线程的关闭
 * 5、最后主线程关闭、外部线程结束，测试完成
 **/
static _int32 yh_test_socket_op_server_in_asyn()
{
    _int32 _ret = YH_SUCCESS;
	yh_thread *_dl_thread = NULL;
	EventLoop *_dl_loop = NULL;
	BOOL _dl_loop_running = FALSE;
	OverlappedWrapper *_test_asyn_sock_op = NULL;
	TestAsynSockParam *_test_asyn_sock_param = NULL;

	LOG("enter...");

	_ret = yh_create_event(&gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

	_ret = yh_loop_create(&_dl_loop);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_dl_loop != NULL);

	// 启动主线程
	_ret = yh_thread_create(&_dl_thread, yh_test_dl_thread_func, 
		(void *)_dl_loop);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_dl_thread->_data != NULL);

	// 从此处开始本线程当做一个外部使用者
	// 按理说，主线程应该导出接口供外部调用，但是这里只是用来测试
	// 所以，直接向主线程投递消息，转到主线程内部使用，以此来模拟
	// 这种API调用

	// 无法确保主线程已经开启，所以这里需要判断其是否已经开启了
	_ret = yh_loop_is_running(_dl_loop, &_dl_loop_running);
	while (!_dl_loop_running) {
		Sleep(100);
		_ret = yh_loop_is_running(_dl_loop, &_dl_loop_running);
	}

	YH_MALLOC(_test_asyn_sock_param, TestAsynSockParam *, 
		sizeof(TestAsynSockParam));

	_test_asyn_sock_param->_loop = _dl_loop;

	_ret = yh_create_overlapped_wrapper(&_test_asyn_sock_op, MT_POST_FUNC, 
		yh_test_handle_test_asyn_sock_op_func_for_server, 
		yh_message_default_delete_handler ,(void *)_test_asyn_sock_param);
	YH_CHECK_VALUE(_ret);

	_ret = yh_post_queued_completion_status(_dl_loop->_iocp, 0, 0, 
		(LPOVERLAPPED)&(_test_asyn_sock_op->_overlapped));
	YH_CHECK_VALUE(_ret);

    _ret = yh_wait(gWaitForTestFinished);

	_ret = yh_loop_destroy(_dl_loop);
	YH_CHECK_VALUE(_ret);

	_ret = yh_thread_destroy(_dl_thread);
	YH_CHECK_VALUE(_ret);

    return _ret;
}


_int32 yh_test_socket_op_in_asyn(_int32 type)
{
    _int32 _ret = YH_SUCCESS;

    _ret = yh_socket_startup();
    YH_CHECK_VALUE(_ret);

    if (type == 0) {
        _ret = yh_test_socket_op_client_in_asyn();
    }
    else if (type == 1) {
        _ret = yh_test_socket_op_server_in_asyn();
    }
    else {
        YH_ASSERT(FALSE);
    }

    _ret = yh_socket_cleanup();
    YH_CHECK_VALUE(_ret);

    return _ret;
}
