#include "test_message_queue.h"
#include "base/message_queue.h"


/**
 * 处理测试用OP_ACCEPT消息的伪回调函数
 * @param fd 接收到的socket句柄
 */
static _int32 dummy_handle_accept(void *fd)
{
    HANDLE *_fd = (HANDLE *)fd;
    LOG("%d", *_fd);
    return YH_SUCCESS;
}


_int32 yh_test_mq_life()
{
	_int32 _ret = YH_SUCCESS;
	MessageQueue *_mq = NULL;
	
	_ret = yh_mq_init(&_mq);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_mq != NULL);
    YH_ASSERT(_mq->_size == 0);
    YH_ASSERT(_mq->_head == NULL);
    YH_ASSERT(_mq->_tail == NULL);

    _ret = yh_mq_uninit(_mq);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


_int32 yh_test_mq_op()
{
    _int32 _ret = YH_SUCCESS;
    MessageQueue *_mq = NULL;
    Message *_msg = NULL, *_msg_pop = NULL;
    HANDLE *_fd = NULL;
    BOOL _mq_empty = FALSE;

    _ret = yh_mq_init(&_mq);
    YH_ASSERT(_mq != NULL);

    _ret = yh_message_alloc(&_msg);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_msg != NULL);
    YH_ASSERT(_msg->_data == NULL);
    YH_ASSERT(_msg->_delete_handler != NULL);
    YH_ASSERT(_msg->_handler == NULL);
    YH_ASSERT(_msg->_id >= 0);
    YH_ASSERT(_msg->_type == MT_UNKNOWN);

    _msg->_type = MT_SOCK_ACCEPT;
    _msg->_handler = dummy_handle_accept;
    _fd = (HANDLE *)malloc(sizeof(HANDLE));
    memset(_fd, 0, sizeof(HANDLE));
    *_fd = (HANDLE)10;
    _msg->_data = (void *)_fd;

    _ret = yh_mq_put(_mq, _msg);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_mq->_size == 1);
    YH_ASSERT(_mq->_head == _mq->_tail);
    YH_ASSERT(_mq->_head->_next == _mq->_tail);
    YH_ASSERT(_mq->_tail->_prev == _mq->_head);

    _ret = yh_mq_empty(_mq, &_mq_empty);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_mq_empty == FALSE);

    _ret = yh_mq_pop(_mq, &_msg_pop);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_mq->_size == 0);
    YH_ASSERT(_mq->_head == _mq->_tail);
    YH_ASSERT(_mq->_head == NULL);
    YH_ASSERT(_mq->_tail == NULL);

    YH_ASSERT(_msg_pop == _msg);
    YH_ASSERT(_msg_pop->_id == _msg->_id);
    YH_ASSERT(_msg_pop->_type == MT_SOCK_ACCEPT);
    YH_ASSERT(_msg_pop->_handler != NULL);
    YH_ASSERT(_msg_pop->_data != NULL);
    _msg_pop->_handler(_msg_pop->_data);

	_ret = yh_message_delete(_msg_pop);
	YH_CHECK_VALUE(_ret);
	// _msg_pop此时已经无效了，不能再次使用了
	//YH_ASSERT(_msg_pop->_data == NULL);
	_msg_pop = NULL;

    _ret = yh_mq_empty(_mq, &_mq_empty);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_mq_empty == TRUE);

    _ret = yh_mq_uninit(_mq);
    YH_CHECK_VALUE(_ret);
	_mq = NULL;

    return _ret;
}
