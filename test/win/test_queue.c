#include "test_queue.h"
#include "base/define.h"
#include "base/queue.h"


struct TestQueueData_s {
    int     _id;
    char    _name[10];
};


/**
 * 之所以不将其与list公用一个接口，是为了避免测试文件之间的互相依赖
 * 如果后续还有很多的话，可以专门开辟一个测试数据结构体文件
 **/
static _int32 yh_test_init_TestQueueData(int id, const char *name, 
                                        size_t name_len, struct TestQueueData_s **data)
{
    _int32 _ret = YH_SUCCESS;

    struct TestQueueData_s *_data 
        = (struct TestQueueData_s *)malloc(sizeof(struct TestQueueData_s));
    memset(_data, 0, sizeof(struct TestQueueData_s));

    _data->_id = id;
    memcpy(_data->_name, name, MIN(name_len, 10));

    if (_data) {
        *data = _data;
        _ret = YH_SUCCESS;
    }
    else {
        *data = NULL;
        _ret = YH_ERR_OUT_OF_MEMORY;
    }

    return _ret;
}


static _int32 yh_test_uninit_TestQueueData(struct TestQueueData_s *data)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(data);

    memset(data, 0, sizeof(struct TestQueueData_s));

    YH_DELETE_OBJ(data);

    return _ret;
}


_int32 yh_test_queue_op()
{
    _int32 _ret = YH_SUCCESS;
    Queue *_queue = NULL;
    struct TestQueueData_s *_data1, *_data2, *_data3, *_data_tmp;
    const char *_test_data1 = "chinese";
    const char *_test_data2 = "people";
    const char *_test_data3 = "longlive";
    _uint32 _queue_size = 0;
    
    _ret = yh_queue_create(&_queue);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_queue != NULL);
    YH_ASSERT(_queue->_list != NULL);
    YH_ASSERT(_queue->_list->_head == NULL);
    YH_ASSERT(_queue->_list->_size == 0);

    _ret = yh_test_init_TestQueueData(1, _test_data1, strlen(_test_data1),
        &_data1);
    YH_CHECK_VALUE(_ret);

    _ret = yh_test_init_TestQueueData(2, _test_data2, strlen(_test_data2),
        &_data2);
    YH_CHECK_VALUE(_ret);

    _ret = yh_test_init_TestQueueData(3, _test_data3, strlen(_test_data3),
        &_data3);
    YH_CHECK_VALUE(_ret);

    _ret = yh_queue_push(_queue, (void *)_data1);
    YH_CHECK_VALUE(_ret);

    _ret = yh_queue_push(_queue, (void *)_data2);
    YH_CHECK_VALUE(_ret);

    _ret = yh_queue_push(_queue, (void *)_data3);
    YH_CHECK_VALUE(_ret);

    _ret = yh_queue_size(_queue, &_queue_size);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_queue_size == 3);

    _ret = yh_queue_pop(_queue, (void *)&_data_tmp);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_data_tmp->_id == _data3->_id);
    YH_ASSERT(strcmp(_data_tmp->_name, _data3->_name) == 0);

    _ret = yh_queue_size(_queue, &_queue_size);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_queue_size == 2);

    _ret = yh_queue_destroy(_queue);
    YH_CHECK_VALUE(_ret);

	_ret = yh_test_uninit_TestQueueData(_data1);
	_data1 = NULL;

	_ret = yh_test_uninit_TestQueueData(_data2);
	_data1 = NULL;

	_ret = yh_test_uninit_TestQueueData(_data3);
	_data1 = NULL;

    return _ret;
}
