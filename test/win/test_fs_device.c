#include "test_fs_device.h"
#include "base/thread.h"
#include "base/singnal.h"
#include "utility.h"


typedef thread_start_func yh_test_reactor_handler;


typedef struct TestFsDeviceOpFuncParam_s TestFsDeviceOpFuncParam;
typedef struct TestFsDeviceWorkUserData_s TestFsDeviceWorkUserData;


static const _uint32 BUF_SIZE = 1024*1024;


static _int32 yh_test_fs_base_op_handler(void *param);
static _int32 yh_test_fs_handle_open_for_src(FsOpenReq *req);
static _int32 yh_test_fs_handle_open_for_des(FsOpenReq *req);
static _int32 yh_test_fs_handle_read(FsReadReq *req);
static _int32 yh_test_fs_handle_write(FsWriteReq *req);
static _int32 yh_test_fs_handle_close(FsCloseReq *req);


/**
 * 测试是否结束的事件
 **/
static HANDLE gWaitForTestFinished = INVALID_HANDLE_VALUE;


static void *_stdcall yh_test_reactor_run(yh_thread *this, void *data)
{
    _int32 _api_ret = YH_SUCCESS;
    _ulong _cur_tid = 0;
    EventLoop *_loop = (EventLoop *)data;

    YH_ASSERT(_loop != NULL);

    _api_ret = yh_get_current_thread_id(&_cur_tid);
    YH_ASSERT(_api_ret == YH_SUCCESS);
    YH_ASSERT(_cur_tid != 0);

    LOG("thread_id:%u.", _cur_tid);

    _api_ret = yh_loop_run(_loop);
    YH_ASSERT(_api_ret == YH_SUCCESS);

    return this;
}


static _int32 yh_test_init_reactor(EventLoop **reactor, 
                                   yh_thread **dl_thread,
                                   yh_test_reactor_handler handler)
{
    _int32 _ret = YH_SUCCESS;
    BOOL _dl_loop_running = FALSE;

    _ret = yh_loop_create(reactor);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(*reactor != NULL);

	LOG("loop:0x%x.", *reactor);

    _ret = yh_thread_create(dl_thread, handler, (void *)*reactor);
    YH_CHECK_VALUE(_ret);

    // 初始化之后确保reactor正常执行
    _ret = yh_loop_is_running(*reactor, &_dl_loop_running);
    while (!_dl_loop_running) {
        Sleep(100);
        _ret = yh_loop_is_running(*reactor, &_dl_loop_running);
    }

    return _ret;
}


static _int32 yh_test_handle_uninit_reactor(void *op)
{
	YH_CHECK_MALLOC_VALID(op);

	return yh_signal(gWaitForTestFinished);
}


/**
 * 反初始化reactor
 * @param reactor
 * @param dl_thread
 * @param handler 完成之后的回调，之所以需要这个是因为可能某些内存需要
 * 到这时才要释放，或者其他一些事需要这时候完成，留一个接口给外部，灵活
 **/
static _int32 yh_test_uninit_reactor(EventLoop *reactor,
									 yh_thread *dl_thread,
									 yh_message_handler handler)
{
	_int32 _ret = YH_SUCCESS;
	OverlappedWrapper *_overlapped_op = NULL;
	BOOL _dl_loop_running = FALSE;

	YH_CHECK_PARAM_VALID(reactor);
	YH_CHECK_PARAM_VALID(dl_thread);
	YH_CHECK_PARAM_VALID(handler);

	_ret = yh_create_overlapped_wrapper(&_overlapped_op, MT_LOOP_STOP, 
		handler, yh_message_default_delete_handler, NULL);
	YH_CHECK_VALUE(_ret);

	_ret = yh_post_queued_completion_status(reactor->_iocp, 0, 0, 
		(LPOVERLAPPED)&(_overlapped_op->_overlapped));
	YH_CHECK_VALUE(_ret);

	_ret = yh_thread_destroy(dl_thread);
	YH_CHECK_VALUE(_ret);

	// 反初始化之后确保reactor已经关闭
	_ret = yh_loop_is_running(reactor, &_dl_loop_running);
	while (_dl_loop_running) {
		Sleep(100);
		_ret = yh_loop_is_running(reactor, &_dl_loop_running);
	}

	_ret = yh_loop_destroy(reactor);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


_int32 yh_test_fs_base_op()
{
	_int32 _ret = YH_SUCCESS;
    TestFsDeviceOpFuncParam *_param = NULL;
	EventLoop *_dl_loop = NULL;
    yh_thread *_dl_thread = NULL;
    const char *_test_src_file_path = "E:\\temp\\";
	const char *_test_src_file_name = "abc.mp4";
    const char *_test_des_file_path = "E:\\temp\\";
    const char *_test_des_file_name = "abc_cpy.mp4";

	_ret = yh_create_event(&gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

    _ret = yh_test_init_reactor(&_dl_loop, &_dl_thread, yh_test_reactor_run);

    YH_MALLOC(_param, TestFsDeviceOpFuncParam *, 
        sizeof(TestFsDeviceOpFuncParam));

	LOG("param:0x%x.", _param);

    _param->_src_file_name._str = (char *)_test_src_file_name;
    _param->_src_file_name._len = strlen(_test_src_file_name);
    _param->_src_file_path._str = (char *)_test_src_file_path;
    _param->_src_file_path._len = strlen(_test_src_file_path);
    _param->_des_file_name._str = (char *)_test_des_file_name;
    _param->_des_file_name._len = strlen(_test_des_file_name);
    _param->_des_file_path._str = (char *)_test_des_file_path;
    _param->_des_file_path._len = strlen(_test_des_file_path);
    _param->_loop = _dl_loop;

    _ret = yh_post_function(_dl_loop, (void *)_param, 
		yh_test_fs_base_op_handler);
    YH_CHECK_VALUE(_ret);

	_ret = yh_wait(gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

	_ret = yh_test_uninit_reactor(_dl_loop, _dl_thread, 
		yh_test_handle_uninit_reactor);
	YH_CHECK_VALUE(_ret);

	/**
	 * NOTE
	 * 由于这里立即完成，IOCP线程回调过来之后已经结束，所以这个线程里
	 * 申请的内存无法得以释放，需要在这里等待iocp线程先完成，才能结束
	 * 合理的做法也应该是这样的：外部调用线程应该等待内部线程的结束之
	 * 后才能结束自身。
	 **/
	/*gWaitForTestFinished = INVALID_HANDLE_VALUE;
	_ret = yh_wait(&gWaitForTestFinished);*/

	YH_DELETE_OBJ(_param);

	return _ret;
}


/**
 * 完成基本的文件操作
 * 1、打开、关闭
 * 2、从一个文件读取，向另外一个文件写入
 **/
static _int32 yh_test_fs_base_op_handler(void *param)
{
	_int32 _ret = YH_SUCCESS;
	TestFsDeviceOpFuncParam *_param = NULL;
	FSDevice *_src_device = NULL;
    FSDevice *_des_device = NULL;
    char *_send_buf = NULL;
	TestFsDeviceWorkUserData *_work = NULL;	

	YH_CHECK_PARAM_VALID(param);

	LOG("param:0x%x.", param);

	_param = (TestFsDeviceOpFuncParam *)param;
    YH_ASSERT(_param != NULL);

	YH_MALLOC(_work, TestFsDeviceWorkUserData *, 
		sizeof(TestFsDeviceWorkUserData));
	
	_work->_src_file_name._len = _param->_src_file_name._len;
	YH_MALLOC(_work->_src_file_name._str, char *, _param->_src_file_name._len);

	_work->_des_file_name._len = _param->_des_file_name._len;
	YH_MALLOC(_work->_des_file_name._str, char *, _param->_des_file_name._len);

	_ret = yh_fs_device_create(_param->_loop, (void *)_work, &_src_device);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_src_device != NULL);
	YH_ASSERT(_src_device->_device->loop == _param->_loop);
    YH_ASSERT(_src_device->_device->_type == FS_DEVICE);
	_work->_src_fs_device = _src_device;

    _ret = yh_fs_device_create(_param->_loop, (void *)_work, &_des_device);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_des_device != NULL);
    YH_ASSERT(_des_device->_device->loop == _param->_loop);
    YH_ASSERT(_des_device->_device->_type == FS_DEVICE);
	_work->_des_fs_device = _des_device;

	_ret = yh_fs_device_open(_src_device, FO_READ, _param->_src_file_path, 
        _param->_src_file_name, NULL, yh_test_fs_handle_open_for_src);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_src_device->_file_size != 0);
	YH_ASSERT(_src_device->_device->_fd != INVALID_HANDLE_VALUE);
    
    // only for testing!
    _work->_file_size = _src_device->_file_size;
	LOG("the src file size:%llu.", _work->_file_size);

    _ret = yh_fs_device_open(_des_device, FO_WRITE_U, _param->_des_file_path,
        _param->_des_file_name, NULL, yh_test_fs_handle_open_for_des);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_des_device->_file_size == 0);
    YH_ASSERT(_des_device->_device->_fd != INVALID_HANDLE_VALUE);

    YH_MALLOC(_send_buf, char *, BUF_SIZE);

    _ret = yh_fs_device_read(_src_device, 0, BUF_SIZE, _send_buf, BUF_SIZE, 
        (void *)_work, yh_test_fs_handle_read);
    YH_CHECK_VALUE(_ret);
    
    return _ret;
}


/**
 * 当测试结束的时候做一些扫尾工作
 * 
 **/
static _int32 yh_test_fs_finish_cleanup(TestFsDeviceWorkUserData *work)
{
	YH_CHECK_PARAM_VALID(work);

	YH_DELETE_OBJ(work->_des_file_name._str);
	YH_DELETE_OBJ(work->_src_file_name._str);
	YH_DELETE_OBJ(work);

	return yh_signal(gWaitForTestFinished);
}


static _int32 yh_test_fs_handle_open_helper(FsOpenReq *req, 
											const char *file_name)
{
	if (req->_base->_err_code != YH_SUCCESS) {
		LOG("can't open file: %s.", file_name);
		YH_ASSERT(FALSE);
		// do some other things...
		return YH_ERR_CANOT_OPEN_FILE;
	}

    LOG("device:0x%x, open file: %s successfully.", 
        req->_base->_device, file_name);

	return YH_SUCCESS;
}


static _int32 yh_test_fs_handle_open_for_des(FsOpenReq *req)
{
	TestFsDeviceWorkUserData *_work = NULL;

	YH_CHECK_PARAM_VALID(req);
	YH_CHECK_PARAM_VALID(req->_base);
	YH_CHECK_PARAM_VALID(req->_base->_user_data);

	_work = (TestFsDeviceWorkUserData *)req->_base->_user_data;

	return yh_test_fs_handle_open_helper(req, _work->_des_file_name._str);
}


static _int32 yh_test_fs_handle_open_for_src(FsOpenReq *req)
{
	TestFsDeviceWorkUserData *_work = NULL;

	YH_CHECK_PARAM_VALID(req);
	YH_CHECK_PARAM_VALID(req->_base);
	YH_CHECK_PARAM_VALID(req->_base->_user_data);

	_work = (TestFsDeviceWorkUserData *)req->_base->_user_data;

	return yh_test_fs_handle_open_helper(req, _work->_src_file_name._str);
}


static _int32 yh_test_fs_handle_read(FsReadReq *req)
{
	_int32 _ret = YH_SUCCESS;
	TestFsDeviceWorkUserData *_work = NULL;
	FSDevice *_src_device = NULL, *_des_device = NULL;
    char *_write_buf = NULL;

	YH_CHECK_PARAM_VALID(req);
	YH_CHECK_PARAM_VALID(req->_base);
	YH_CHECK_PARAM_VALID(req->_base->_user_data);

	_work = (TestFsDeviceWorkUserData *)req->_base->_user_data;
	YH_ASSERT(_work != NULL);

	_src_device = (FSDevice *)req->_base->_device;
	YH_ASSERT(_src_device != NULL);
    YH_ASSERT(_work->_src_fs_device == _src_device);
    YH_ASSERT(_src_device->_exp_read_len >= req->_read_len);

    _des_device = _work->_des_fs_device;

    LOG("req:0x%x, errcode:%d.", req, req->_base->_err_code);

    if ((req->_base->_err_code != YH_SUCCESS) && 
        (req->_base->_err_code != YH_ERR_READ_FILE_EOF)) {
        LOG("read [%llu, %u] error.", req->_start_pos, req->_read_len);
        YH_ASSERT(FALSE);
        return YH_ERR_READ_FILE_FAIL;
    }

	// FIXME
	// 每次写都会申请 -- 性能会有影响
	// 其实写是可以一个个的写的，但是在写的同时完全可以去读
	// 这里读写缓存分开是有必要的，可以避免两者为了共享内存而产生关联
	YH_MALLOC(_write_buf, char *, req->_read_len);

	memcpy(_write_buf, _src_device->_read_buf, req->_read_len);

	_ret = yh_fs_device_write(_des_device, req->_start_pos, req->_read_len, 
		_write_buf, NULL, yh_test_fs_handle_write);
	YH_CHECK_VALUE(_ret);

	_work->_readed_size += req->_read_len;

	LOG("read [%llu, %u] complete, has read %llu bytes, file_size:%llu.", 
		req->_start_pos, req->_read_len, _work->_readed_size, 
		_work->_file_size);

    if ((req->_base->_err_code == YH_ERR_READ_FILE_EOF) || 
		(_work->_readed_size == _work->_file_size)) {
        LOG("read to the end of file.");
        //YH_ASSERT(req->_start_pos == _work->_file_size);

		YH_DELETE_OBJ(_src_device->_read_buf);

        _ret = yh_fs_device_close(_src_device, NULL, yh_test_fs_handle_close);
        YH_CHECK_VALUE(_ret);

		_ret = yh_fs_device_destroy(_src_device);
		YH_CHECK_VALUE(_ret);

        return _ret;
    }

	// 继续读取，这里按顺序读取
    memset(_src_device->_read_buf, 0, _src_device->_exp_read_len);
    _ret = yh_fs_device_read(_src_device, req->_start_pos + req->_read_len, 
        BUF_SIZE, _src_device->_read_buf, BUF_SIZE, (void *)_work, 
		yh_test_fs_handle_read);
    YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 yh_test_fs_handle_write(FsWriteReq *req)
{
    _int32 _ret = YH_SUCCESS;
    FSDevice *_des_device = NULL;
	TestFsDeviceWorkUserData *_work = NULL;

    YH_CHECK_PARAM_VALID(req);

    _des_device = (FSDevice *)req->_base->_device;
    YH_ASSERT(_des_device != NULL);

	_work = _des_device->_user_data;
	YH_ASSERT(_work != NULL);

	YH_DELETE_OBJ(_des_device->_write_buf);

    if (req->_base->_err_code != YH_SUCCESS) {
        LOG("write [%llu, %u] fail.", req->_start_pos, req->_write_len);
        YH_ASSERT(FALSE);
        return YH_ERR_WRITE_FILE_FAIL;
    }

	_work->_writed_size += req->_write_len;
	LOG("write [%llu, %u] complete, has writed %llu bytes, file_size:%llu.", 
		req->_start_pos, req->_write_len, _work->_writed_size, 
		_work->_file_size);

	if (_work->_writed_size == _work->_file_size) {
		LOG("write file finished, so can be closed.");

		_ret = yh_fs_device_close(_des_device, NULL, yh_test_fs_handle_close);
		YH_CHECK_VALUE(_ret);

		_ret = yh_fs_device_destroy(_des_device);
		YH_CHECK_VALUE(_ret);

		_ret = yh_test_fs_finish_cleanup(_work);
		YH_CHECK_VALUE(_ret);
	}

    return _ret;
}


static _int32 yh_test_fs_handle_close(FsCloseReq *req)
{
    YH_CHECK_PARAM_VALID(req);

    LOG("device:0x%x closed.", req->_base->_device);

    return YH_SUCCESS;
}
