/**
 * 测试socket的发送，指定字节数，从1个字节到n个字节，对方必须能准确的
 * 接收到。
 **/
#ifndef _LIBYH_TEST_SOCKET_CBYTES_H_
#define _LIBYH_TEST_SOCKET_CBYTES_H_


#include "base/define.h"


/**
 * 向默认地址发送指定字节的数据
 * @param cbytes 
 **/
/*_int32 yh_test_send_cbytes(_uint32 cbytes);*/


/**
 * 在默认的地址上接收字节，收到后立即丢弃，输出收到的字节数
 **/
/*_int32 yh_test_recv_cbytes();*/


#endif//_LIBYH_TEST_SOCKET_CBYTES_H_
