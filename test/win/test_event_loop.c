#include "test_event_loop.h"
#include "base/thread.h"
#include "base/singnal.h"
#include "io/iocp.h"


static YHHandle gWaitForTestFinished = INVALID_HANDLE_VALUE;
static void * __stdcall stop_testing_thread(yh_thread *this, void *data);


_int32 yh_test_event_loop_create()
{
    _int32 _ret = YH_SUCCESS;
    EventLoop *_loop = NULL;

    _ret = yh_loop_create(&_loop);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_loop);
    YH_ASSERT(_loop->_iocp);
    YH_ASSERT(_loop->_stop_flag == 0);

	_ret = yh_loop_destroy(_loop);
	YH_CHECK_VALUE(_ret);

    return _ret;
}


_int32 yh_test_event_loop_run()
{
    _int32 _ret = YH_SUCCESS;
    EventLoop *_loop = NULL;
    yh_thread *_stopping_thread = NULL;
    _ulong _main_thread_id = 0;

	_ret = yh_create_event(&gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

    _ret = yh_loop_create(&_loop);
	YH_CHECK_VALUE(_ret);
    
    _ret = yh_get_current_thread_id(&_main_thread_id);
	YH_CHECK_VALUE(_ret);

    _ret = yh_thread_create(&_stopping_thread, stop_testing_thread, (void *)_loop);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_stopping_thread != NULL);
    YH_ASSERT((_ulong)(_stopping_thread->_tid) != _main_thread_id);
    YH_ASSERT(_stopping_thread->_data == (void *)_loop);
    YH_ASSERT(_stopping_thread->_thread_proc == stop_testing_thread);

    _ret = yh_loop_run(_loop);
	YH_CHECK_VALUE(_ret);

	_ret = yh_wait(gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

	_ret = yh_loop_destroy(_loop);
	YH_CHECK_VALUE(_ret);

    _ret = yh_thread_destroy(_stopping_thread);
    _stopping_thread = NULL;

	_ret = yh_destroy_event(gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);
    
    return _ret;
}


_int32 yh_test_event_loop_destroy()
{
    _int32 _ret = YH_SUCCESS;

    return _ret;
}


static void * __stdcall stop_testing_thread(yh_thread *this, void *data)
{
    _int32 _ret = YH_SUCCESS;
    EventLoop *_loop = (EventLoop *)data;
    OverlappedWrapper *_overlapped_op = NULL;

    YH_ASSERT(_loop != NULL);

    LOG("this:%p, data:%p.", this, data);

    _ret = yh_create_overlapped_wrapper(&_overlapped_op, MT_LOOP_STOP, 
		yh_test_handle_stop_loop_msg, yh_message_default_delete_handler, NULL);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_overlapped_op != NULL);
	//YH_ASSERT(_overlapped_op->_msg->_id != 0);
    //YH_ASSERT(_overlapped_op->_msg->_data == data);
	YH_ASSERT(_overlapped_op->_msg->_handler == yh_test_handle_stop_loop_msg);
	YH_ASSERT(_overlapped_op->_msg->_delete_handler != NULL);
    YH_ASSERT(_overlapped_op->_msg->_type == MT_LOOP_STOP);

    _ret = yh_post_queued_completion_status(_loop->_iocp, 0, 0, 
        (LPOVERLAPPED)&(_overlapped_op->_overlapped));
    YH_CHECK_VALUE(_ret);

	yh_signal(gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

    return (void *)this;
}


_int32 yh_test_handle_stop_loop_msg(void *data)
{
    _int32 _ret = YH_SUCCESS;
    OverlappedWrapper *_op = (OverlappedWrapper *)data;
	
    LOG("stop loop, _op:%p.", _op);

	return _ret;
}
