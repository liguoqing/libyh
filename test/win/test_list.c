#include "test_list.h"
#include "base/list.h"


struct TestListData_s {
    int     _id;
    char    _name[10];
};


static _int32 yh_test_init_TestListData(int id, const char *name, 
                                        size_t name_len, struct TestListData_s **data)
{
    _int32 _ret = YH_SUCCESS;

    struct TestListData_s *_data 
        = (struct TestListData_s *)malloc(sizeof(struct TestListData_s));
    memset(_data, 0, sizeof(struct TestListData_s));

    _data->_id = id;
    memcpy(_data->_name, name, MIN(name_len, 10));

    if (_data) {
        *data = _data;
        _ret = YH_SUCCESS;
    }
    else {
        *data = NULL;
        _ret = YH_ERR_OUT_OF_MEMORY;
    }

    return _ret;
}


static _int32 yh_test_uninit_TestListData(struct TestListData_s *data)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(data);

    memset(data, 0, sizeof(struct TestListData_s));

    YH_DELETE_OBJ(data);

    return _ret;
}


static _int32 yh_test_cmp_TestListData(void *data_ori, void *data_cmp)
{
    struct TestListData_s *_data_ori = (struct TestListData_s *)data_ori;
    struct TestListData_s *_data_cmp = (struct TestListData_s *)data_cmp;

    YH_CHECK_PARAM_VALID(data_ori);
    YH_CHECK_PARAM_VALID(data_cmp);

    return _data_ori->_id - _data_cmp->_id;
}


static _int32 yh_test_free_TestListData(void *data)
{
    return yh_test_uninit_TestListData((struct TestListData_s *)data);
}


#if defined(YH_DEBUG_ENABLE)
static _int32 yh_test_print_TestListData(void *data)
{
	struct TestListData_s *_data = (struct TestListData_s *)data;
	
	YH_CHECK_PARAM_VALID(data);

	LOG("_data->_id %d, _data->_name %s", _data->_id, _data->_name);

	return YH_SUCCESS;
}
#endif


_int32 yh_test_list_op()
{
    _int32 _ret = YH_SUCCESS;
    List *_list = NULL;
    ListNode *_head = NULL;
    ListNode *_tail = NULL;
    ListNode *_match = NULL;
    struct TestListData_s *_data = NULL;
    struct TestListData_s *_data_cmp = NULL;
    const char *_data_name = "hello";
    const char *_data_cmp_name = "world";

    _ret = yh_list_create(&_list);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_list != NULL);
    YH_ASSERT(_list->_size == 0);
    YH_ASSERT(_list->_head == NULL);

    _ret = yh_test_init_TestListData(1, _data_name, strlen(_data_name), &_data);
    YH_CHECK_VALUE(_ret);
    _ret = yh_test_init_TestListData(2, _data_cmp_name, strlen(_data_cmp_name), 
        &_data_cmp);
    YH_CHECK_VALUE(_ret);

    _ret = yh_list_insert(_list, (void *)_data, NULL);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_list->_size == 1);
    YH_ASSERT(_list->_head != NULL);
	YH_ASSERT(_list->_head->_data != NULL);
	YH_ASSERT(_list->_head->_prev != NULL);
	YH_ASSERT(_list->_head->_next != NULL);

    _ret = yh_list_insert(_list, (void *)_data_cmp, NULL);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_list->_size == 2);
    YH_ASSERT(_list->_head != NULL);
    YH_ASSERT(_list->_head->_data == _data);
	YH_ASSERT(_list->_head->_next->_data != NULL);
	YH_ASSERT(_list->_head->_next->_prev != NULL);
	YH_ASSERT(_list->_head->_next->_next != NULL);

    _ret = yh_list_insert(_list, (void *)_data_cmp, _list->_head);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_list->_size == 3);
    YH_ASSERT(_list->_head != NULL);
    YH_ASSERT(_list->_head->_data == _data);
    YH_ASSERT(_list->_head->_next->_data == _data_cmp);
    YH_ASSERT(_list->_head->_prev->_data == _data_cmp);

    _head = _list->_head;
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_head != NULL);
    YH_ASSERT(_head->_next != NULL);
    YH_ASSERT(_head->_prev != NULL);
    YH_ASSERT(_head->_data == _data);

#if defined(YH_DEBUG_ENABLE)
	_ret = yh_list_print_data(_list, yh_test_print_TestListData);
	YH_CHECK_VALUE(_ret);
#endif

    _ret = yh_list_tail(_list, &_tail);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_tail != NULL);
    YH_ASSERT(_tail->_next != NULL);
    YH_ASSERT(_tail->_prev != NULL);
    YH_ASSERT(_tail->_data == _data_cmp);

    _ret = yh_list_node_find(_list, _data, yh_test_cmp_TestListData, &_match);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_match != NULL);
    YH_ASSERT(_match->_data == _data);
    YH_ASSERT(_match->_prev != NULL);
    YH_ASSERT(_match->_next != NULL);

    _ret = yh_list_node_delete(_list, _match);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_list->_head != NULL);
    YH_ASSERT(_list->_size == 2);
    YH_ASSERT(_list->_head->_data == _data_cmp);

    _ret = yh_list_destroy(_list);
    YH_CHECK_VALUE(_ret);

    _ret = yh_test_uninit_TestListData(_data_cmp);
    _ret = yh_test_uninit_TestListData(_data);

    return _ret ;
}
