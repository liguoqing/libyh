/**
 * 对socket封装进行测试
 **/


#ifndef _LIBYH_TEST_SOCKET_H_
#define _LIBYH_TEST_SOCKET_H_


#include "base/define_data_type.h"


_int32 yh_test_socket_create();


#define TEST_SOCKET_CLIENT		0
#define TEST_SOCKET_SERVER		1


/**
 * 对socket上的基本操作做测试
 * @param type 0 for client, 1 for server
 **/
_int32 yh_test_socket_op_in_syn(_int32 type);


_int32 yh_test_socket_op_in_asyn(_int32 type);


#endif//_LIBYH_TEST_SOCKET_H_
