#include "test_tcp_connection.h"
#include "base/thread.h"
#include "base/message.h"
#include "base/singnal.h"
#include "net/socket_utility.h"
#include "utility.h"


typedef thread_start_func yh_test_reactor_handler;


static _int32 yh_test_init_reactor(EventLoop **reactor, 
                                   yh_thread **dl_thread,
                                   yh_test_reactor_handler handler);
static _int32 yh_test_uninit_reactor(EventLoop *reactor,
                                     yh_thread *dl_thread);
static _int32 yh_test_wait_for_upload(void *param);
static _int32 yh_test_create_ft_session(TestFileTransferDownWork *work, 
										const struct sockaddr_in *ser_addr,
										_uint32 ser_addr_len,
										TestFileTransferSession **session);
static _int32 yh_test_send_down_file_request(void *param);
static _int32 yh_test_send_file_range_request(TcpConnection *conn);
static _int32 yh_test_serialize_down_file_request_cmd(DownFileReqCmd *cmd,
                                                      char **data,
                                                      _uint32 *data_len);
static _int32 yh_test_serialize_down_file_response_cmd(DownFileRespCmd *cmd,
													   char **data,
													   _uint32 *data_len);
static _int32 yh_test_serialize_file_range_request_cmd(FileRangeReqCmd *cmd,
													   char **data, 
													   _uint32 *data_len);
static _int32 yh_test_serialize_file_range_response_cmd(FileRangeRespCmd *cmd, 
														char **data, 
														_uint32 *data_len);
static _int32 yh_test_send_down_file_resp(TcpConnection *conn, BOOL result);
static _int32 yh_test_handle_connection_recv_cmd_header_s(void *data, 
                                                          _int32 errcode);
static _int32 yh_test_handle_connection_recv_cmd_header_c(void *data, 
														  _int32 errcode);
static _int32 yh_test_handle_connection_recv_cmd_body_s(void *data, 
														_int32 errcode);
static _int32 yh_test_handle_connection_recv_cmd_body_c(void *data, 
														_int32 errcode);
static _int32 yh_test_recv_cmd_header_helper(TestFileTransferSession *session, 
	TcpConnection *conn, yh_tcp_connection_recv_callback callback);
static _int32 yh_test_recv_cmd_body_helper(TestFileTransferSession *session,
										   TcpConnection *conn);
static _int32 yh_test_for_server_helper(TcpDevice *tcp_device, 
										_int32 *err_code);
static _int32 yh_test_handle_down_file_req(TcpConnection *conn);
static _int32 yh_test_handle_down_file_resp(TcpConnection *conn);
static _int32 yh_test_handle_file_range_req(TcpConnection *conn);
static _int32 yh_test_handle_file_range_resp(TcpConnection *conn);
static _int32 yh_test_parse_p2p_cmd_header(char *data, _uint32 data_len,
										   P2pCmdHeader *_header);
static _int32 yh_test_parse_down_file_req(char *data, DownFileReqCmd *cmd);
static _int32 yh_test_parse_down_file_resp(char *data, DownFileRespCmd *cmd);
static _int32 yh_test_parse_file_range_req(char *data, FileRangeReqCmd *cmd);
static _int32 yh_test_parse_file_range_resp(char *data, FileRangeRespCmd *cmd);
static _int32 yh_test_wait_finish();
static _int32 yh_test_signal_finish();
static _int32 yh_test_load_data_from_file(TestFileTransferUpWork *work, 
										  _uint64 pos, _uint32 len,
										  char **cont_buf,
										  yh_fs_device_read_callback handler);
static _int32 yh_test_send_file_range_response(TcpConnection *conn, 
											   _uint64 pos, _uint32 len,
											   char *cont_buf, 
											   _uint32 cont_len);
static _int32 yh_test_write_data_to_file(FSDevice *fs_device, _uint64 pos,
										 char *cont, _uint32 cont_len);
static _int32 yh_test_tcp_device_accept_callback(SockAcceReq *req);
static _int32 yh_test_handle_down_file_open(FsOpenReq *req);
static _int32 yh_test_handle_up_file_read(FsReadReq *req);
static _int32 yh_test_handle_down_file_write(FsWriteReq *req);
static _int32 yh_test_handle_connection_close_for_c(void *data, _int32 errcode);
static _int32 yh_test_handle_connection_close_for_s(void *data, _int32 errcode);
static _int32 yt_test_destroy_ft_work(TestFileTransferDownWork *work);
static _int32 yh_test_destory_ft_session(TestFileTransferSession *session);
static _int32 yh_test_tcp_device_close_callback(SockClosReq *req);
static _int32 yh_test_handle_connection_send_for_c(void *data, _int32 errcode);
static _int32 yh_test_handle_connection_send_for_s(void *data, _int32 errcode);
static _int32 yh_test_fs_handle_close(FsCloseReq *req);


/**
 * tcp连接上的各种回调
 **/
static _int32 yh_test_handle_connection_open(void *data, _int32 errcode);


/**
 * fs上的各种回调
 **/
static _int32 yh_test_handle_up_file_open(FsOpenReq *req);


/**
 * reactor线程的函数体
 **/
static void *_stdcall yh_test_reactor_run(yh_thread *this, void *data);


static YHHandle gWaitForTestFinished = INVALID_HANDLE_VALUE;
static TcpDevice *gListenDevice = NULL;


/**
 * 流程：
 * 1、建立到服务器的连接
 * 2、向服务器发起下载请求[文件名、大小]
 * 3、接收文件、写磁盘、计算下载速度
 * 4、完成后断开连接
 * 5、结束测试
 **/
_int32 yh_test_download_file()
{
    _int32 _ret = YH_SUCCESS;
    EventLoop *_reactor = NULL;
	SendDownFileReqFuncParam *_param = NULL;
    yh_thread *_dl_thread = NULL;
	YHString _file_name;

    _ret = yh_test_init_reactor(&_reactor, &_dl_thread, yh_test_reactor_run);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_reactor != NULL);

	_ret = yh_socket_startup();
	YH_CHECK_VALUE(_ret);

	_file_name._str = "abc.mp4";
	_file_name._len = strlen(_file_name._str);

	YH_MALLOC(_param, SendDownFileReqFuncParam *, 
		sizeof(SendDownFileReqFuncParam));
	YH_MALLOC(_param->_ser_res, TestServerRes *, sizeof(TestServerRes));
	
	_param->_reactor = _reactor;
	_param->_file_name = _file_name;
	_param->_file_size = 1405408ll;

    _ret = yh_post_function(_reactor, (void *)_param,
		yh_test_send_down_file_request);
    YH_CHECK_VALUE(_ret);

    // 等待测试结束
    _ret = yh_test_wait_finish();
    YH_CHECK_VALUE(_ret);

	YH_DELETE_OBJ(_param->_ser_res);
	YH_DELETE_OBJ(_param);

	_ret = yh_socket_cleanup();
	YH_CHECK_VALUE(_ret);

	_ret = yh_test_uninit_reactor(_reactor, _dl_thread);
	YH_CHECK_VALUE(_ret);

    return _ret;
}


/**
 * 流程：
 * 1、开启上传服务、等待客户端请求的到来
 * 2、解析客户端发过来的文件下载请求
 * 3、读文件、发送
 * 4、完成后断开连接
 * 5、周而复始，理论上可以同时完成多条连接的服务，但这里为了简单只一条
 **/
_int32 yh_test_upload_file()
{
    _int32 _ret = YH_SUCCESS;
	UpFileFuncParam *_param = NULL;
	EventLoop *_reactor = NULL;
    yh_thread *_dl_thread = NULL;

	_ret = yh_test_init_reactor(&_reactor, &_dl_thread, yh_test_reactor_run);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_reactor != NULL);

	_ret = yh_socket_startup();
	YH_CHECK_VALUE(_ret);

	YH_MALLOC(_param, UpFileFuncParam *, sizeof(UpFileFuncParam));

	_param->_reactor = _reactor;

	_ret = yh_post_function(_reactor, (void *)_param, yh_test_wait_for_upload);
	YH_CHECK_VALUE(_ret);

	// 等待测试结束
	_ret = yh_test_wait_finish();
	YH_CHECK_VALUE(_ret);

	if (gListenDevice) {
		_ret = yh_tcp_device_close(gListenDevice, NULL, 
			yh_test_tcp_device_close_callback);
		YH_CHECK_VALUE(_ret);
	}

	YH_DELETE_OBJ(_param);

	_ret = yh_socket_cleanup();
	YH_CHECK_VALUE(_ret);

	_ret = yh_test_uninit_reactor(_reactor, _dl_thread);
	YH_CHECK_VALUE(_ret);
	
	LOG("finished..");

    return _ret;
}


static _int32 yh_test_tcp_device_close_callback(SockClosReq *req)
{
	_int32 _ret = YH_SUCCESS;

	TcpDevice *_device = NULL;

	YH_CHECK_PARAM_VALID(req);

	LOG("req:0x%x, errcode:%d.", req, req->_base->_err_code);

	if (req->_base->_err_code != YH_SUCCESS) {
		return YH_ERR_SOCKET_OP_FAILURE;
	}

	_device = (TcpDevice *)req->_base->_device;

	_ret = yh_tcp_device_destroy(_device);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


/**
 * 初始化事件循环
 * @param reactor [out] 代表事件循环的指针
 * @param func [in] reactor的具体执行函数
 * @param data [in] 传入的用户数据
 **/
static _int32 yh_test_init_reactor(EventLoop **reactor, 
                                   yh_thread **dl_thread,
								   yh_test_reactor_handler handler)
{
	_int32 _ret = YH_SUCCESS;
	BOOL _dl_loop_running = FALSE;

	_ret = yh_loop_create(reactor);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(*reactor != NULL);

	_ret = yh_thread_create(dl_thread, handler, (void *)*reactor);
	YH_CHECK_VALUE(_ret);

	// 初始化之后确保reactor正常执行
	_ret = yh_loop_is_running(*reactor, &_dl_loop_running);
	while (!_dl_loop_running) {
		Sleep(100);
		_ret = yh_loop_is_running(*reactor, &_dl_loop_running);
	}

	return _ret;
}


static void *_stdcall yh_test_reactor_run(yh_thread *this, void *data)
{
	_int32 _api_ret = YH_SUCCESS;
	_ulong _cur_tid = 0;
	EventLoop *_loop = (EventLoop *)data;

	YH_ASSERT(_loop != NULL);

	_api_ret = yh_get_current_thread_id(&_cur_tid);
	YH_ASSERT(_api_ret == YH_SUCCESS);
	YH_ASSERT(_cur_tid != 0);

	LOG("thread_id:%u.", _cur_tid);

	_api_ret = yh_loop_run(_loop);
	YH_ASSERT(_api_ret == YH_SUCCESS);

	return this;
}


static _int32 yh_test_uninit_reactor(EventLoop *reactor,
                                     yh_thread *dl_thread)
{
	_int32 _ret = YH_SUCCESS;
    OverlappedWrapper *_overlapped_op = NULL;
	BOOL _dl_loop_running = FALSE;

    YH_ASSERT(reactor != NULL);

    _ret = yh_create_overlapped_wrapper(&_overlapped_op, MT_LOOP_STOP, 
        NULL, yh_message_default_delete_handler, NULL);
    YH_CHECK_VALUE(_ret);

    _ret = yh_post_queued_completion_status(reactor->_iocp, 0, 0, 
        (LPOVERLAPPED)&(_overlapped_op->_overlapped));
    YH_CHECK_VALUE(_ret);

    _ret = yh_thread_destroy(dl_thread);
    YH_CHECK_VALUE(_ret);

	// 反初始化之后确保reactor已经关闭
	_ret = yh_loop_is_running(reactor, &_dl_loop_running);
	while (_dl_loop_running) {
		Sleep(100);
		_ret = yh_loop_is_running(reactor, &_dl_loop_running);
	}

	_ret = yh_loop_destroy(reactor);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


/**
 * server 的主执行函数
 * 1、监听，等待客户端的链接
 * 2、接受链接，还是只接收1条链接
 * 3、文件传输
 **/
static _int32 yh_test_wait_for_upload(void *param)
{
	_int32 _ret = YH_SUCCESS;
	UpFileFuncParam *_param = NULL;
    TcpDevice *_listen_device = NULL;
    _int32 _errcode = 0;

    YH_CHECK_MALLOC_VALID(param);

    _param = (UpFileFuncParam *)param;
    YH_ASSERT(_param->_reactor != NULL);

    _ret = yh_tcp_device_create(_param->_reactor, INVALID_SOCKET, 
        &_listen_device);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_listen_device != NULL);

	gListenDevice = _listen_device;

    _ret = yh_test_for_server_helper(_listen_device, &_errcode);
    YH_CHECK_VALUE(_ret);

	return _ret;
}


/**
 * 服务器端准备接受连接的一般操作
 **/
static _int32 yh_test_for_server_helper(TcpDevice *tcp_device, 
                                        _int32 *err_code)
{
    _int32 _ret = YH_SUCCESS;
    struct sockaddr_in _ser_addr = {0};
    const char *_server_ip = "127.0.0.1";
    const _uint16 _server_port = TCP_LISTEN_PORT;

    _ret = yh_socket_ip_str2ulong(_server_ip, &(_ser_addr.sin_addr.s_addr));
    YH_CHECK_VALUE(_ret);
    _ser_addr.sin_family = AF_INET;
    _ser_addr.sin_port = htons(_server_port);

    _ret = yh_tcp_device_listen(tcp_device, (const SOCKADDR *)&_ser_addr, 
        sizeof(_ser_addr), DEFAULT_TCP_BACKLOG, err_code);
    YH_CHECK_VALUE(_ret);

    _ret = yh_tcp_device_accept(tcp_device, yh_test_tcp_device_accept_callback);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 yh_test_tcp_device_accept_callback(SockAcceReq *req)
{
    _int32 _ret = YH_SUCCESS;
    TcpConnection *_new_conn = NULL;
    TcpDevice *_new_device = NULL;
    TcpDevice *_listen_device = NULL;
	TestFileTransferUpWork *_up_work = NULL;

    YH_ASSERT(req != NULL);
    YH_ASSERT(req->_acc_sock_fd != INVALID_SOCKET);
    YH_ASSERT(req->_base->_device != NULL);
    YH_ASSERT(req->_base->_op == NULL);

    LOG("req:0x%x, errcode:%d.", req, req->_base->_err_code);

    if (req->_base->_err_code != YH_SUCCESS) {
        return YH_ERR_SOCKET_OP_FAILURE;
    }

    _listen_device = (TcpDevice *)(req->_base->_device);

	/**
	 * FIXME
	 * 独立函数创建测试任务
	 **/
	YH_MALLOC(_up_work, TestFileTransferUpWork *, 
		sizeof(TestFileTransferUpWork));	
	YH_MALLOC(_up_work->_session, TestFileTransferSession *, 
		sizeof(TestFileTransferSession));
	YH_MALLOC(_up_work->_data_mgr, TestFileDataManager *,
		sizeof(TestFileDataManager));

	// FIXME
    // 为新接收的连接封装成tcp_connection
    // 我这里是不是应该将这两行合成一个？
    _ret = yh_tcp_device_create(_listen_device->_device->loop, 
        req->_acc_sock_fd, &_new_device);
    YH_CHECK_VALUE(_ret);
	YH_ASSERT(_new_device != NULL);

    _ret = yh_tcp_connection_create(_listen_device->_device->loop, 
        _new_device, (void *)_up_work, &_new_conn);
    YH_CHECK_VALUE(_ret);

	_up_work->_session->_conn = _new_conn;	
	YH_MALLOC(_up_work->_session->_cmd_buffer, char *, MAX_P2P_PACKET_SIZE);
	_up_work->_session->_cmd_buffer_offset = 0;

    _ret = yh_tcp_connection_recv(_new_conn, P2P_CMD_HEADER_SIZE, 
        yh_test_handle_connection_recv_cmd_header_s);
    YH_CHECK_VALUE(_ret);

    // 在老device上重新发起accept操作
    /*_ret = yh_tcp_device_accept(_listen_device, 
        yh_test_tcp_device_accept_callback);
    YH_CHECK_VALUE(_ret);*/

    return _ret;
}


/**
 * 创建一个测试用的业务对象
 **/
static _int32 yt_test_create_ft_work(struct EventLoop_s *reactor,
									 const char *file_name, _uint32 name_len,
                                     _uint64 file_size, 
									 const struct sockaddr_in *ser_addr,
									 _uint32 ser_addr_len,
                                     TestFileTransferDownWork **work)
{
    _int32 _ret = YH_SUCCESS;
    TestFileTransferDownWork *_work = NULL;

    YH_CHECK_PARAM_VALID(file_name);
    YH_CHECK_PARAM_VALID(ser_addr);
    YH_ASSERT(name_len > 0 && name_len < MAX_FILE_NAME_LEN);
    YH_ASSERT(file_size > 0);
    YH_ASSERT(_work == NULL);

    YH_MALLOC(_work, TestFileTransferDownWork *, 
		sizeof(TestFileTransferDownWork));
	YH_MALLOC(_work->_data_mgr, TestFileDataManager *,
		sizeof(TestFileDataManager));
    YH_MALLOC(_work->_file_name._str, char *, name_len);

	_work->_reactor = reactor;
	_work->_data_mgr->_cur_file_pos = 0;
	// 在服务器回应该文件存在之后方才打开
	_work->_data_mgr->_file_device = NULL;

	memcpy(_work->_file_name._str, file_name, name_len);
	_work->_file_name._len = name_len;
    _work->_file_size = file_size;

    _ret = yh_test_create_ft_session(_work, ser_addr, ser_addr_len, 
		&_work->_session);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_work->_session != NULL);
    YH_ASSERT(_work->_session->_state == TSS_IDLE);
    YH_ASSERT(_work->_session->_cmd_buffer != NULL);
    YH_ASSERT(_work->_session->_cmd_buffer_offset == 0);

	*work = _work;

    return _ret;
}


static _int32 yt_test_destroy_ft_work(TestFileTransferDownWork *work)
{
	YH_CHECK_PARAM_VALID(work);

	if (work->_session) {
		yh_test_destory_ft_session(work->_session);
	}

	YH_DELETE_OBJ(work->_file_name._str);
	
	yh_fs_device_close(work->_data_mgr->_file_device, NULL, 
		yh_test_fs_handle_close);

	yh_fs_device_destroy(work->_data_mgr->_file_device);

	YH_DELETE_OBJ(work->_data_mgr);
	YH_DELETE_OBJ(work);

	return YH_SUCCESS;
}


/**
 * 创建用于文件传输业务的session
 * @param work 
 * @param session
 **/
static _int32 yh_test_create_ft_session(TestFileTransferDownWork *work, 
										const struct sockaddr_in *ser_addr,
										_uint32 ser_addr_len,
                                        TestFileTransferSession **session)
{
    _int32 _ret = YH_SUCCESS;
    TestFileTransferSession *_session = NULL;

    YH_CHECK_PARAM_VALID(work);
    YH_ASSERT(_session == NULL);

    YH_MALLOC(_session, TestFileTransferSession *, 
        sizeof(TestFileTransferSession));

	// tcp_connection上的user_data是这里的work
    _ret = yh_tcp_connection_create(work->_reactor, NULL, (void *)work, 
		&_session->_conn);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_session->_conn != NULL);
    YH_ASSERT(_session->_conn->_user_data == work);

    // 默认情况下，一个p2p包的最大值是64K
    YH_MALLOC(_session->_cmd_buffer, char *, MAX_P2P_PACKET_SIZE);
    _session->_cmd_buffer_offset = 0;

	// 当server的描述信息比较多的时候可以用函数独立
	YH_MALLOC(_session->_ser_res, TestServerRes *, sizeof(TestServerRes));
	memcpy(&_session->_ser_res->_addr, ser_addr, ser_addr_len);

    _session->_state = TSS_IDLE;

	*session = _session;

    return _ret;
}


static _int32 yh_test_destory_ft_session(TestFileTransferSession *session)
{
	YH_CHECK_PARAM_VALID(session);

	YH_DELETE_OBJ(session->_ser_res);
	YH_DELETE_OBJ(session->_cmd_buffer);

	if (session->_conn) {
		yh_tcp_connection_destroy(session->_conn);
	}

	YH_DELETE_OBJ(session);

	return YH_SUCCESS;
}


/**
 * 协议交互的开始
 **/
static _int32 yh_test_session_open(TestFileTransferSession *session)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(session);

    _ret = yh_tcp_connection_open(session->_conn, &(session->_ser_res->_addr),
        sizeof(session->_ser_res->_addr), yh_test_handle_connection_open);
    YH_CHECK_VALUE(_ret);

    session->_state = TSS_OPENING;

	return _ret;
}


/**
 * 测试文件传输：
 * 1、创建一个应用层work，作为当前测试的业务管理
 *	1.1、创建一个用于数据存取的对象
 *	1.2、创建一个连接，用于数据的收发
 * 2、开始连接远端
 **/
static _int32 yh_test_send_down_file_request(void *param)
{
    _int32 _ret = YH_SUCCESS;
    TestFileTransferDownWork *_work = NULL;
    SendDownFileReqFuncParam *_param = (SendDownFileReqFuncParam *)param;
	struct sockaddr_in _addr = {0};

    YH_CHECK_PARAM_VALID(param);

	// TODO
	// 可以通过外部的ser_res设置，这里从简
	_addr.sin_family = AF_INET;
	_addr.sin_port = htons(5555);
	_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    _ret = yt_test_create_ft_work(_param->_reactor, _param->_file_name._str, 
        _param->_file_name._len, _param->_file_size, &_addr, 
		sizeof(_addr), &_work);
    YH_CHECK_VALUE(_ret);
	YH_ASSERT(_work != NULL);
    YH_ASSERT(_work->_session != NULL);
    YH_ASSERT(_work->_session->_conn != NULL);
    YH_ASSERT(_work->_session->_state == TSS_IDLE);
    YH_ASSERT(_work->_session->_cmd_buffer_offset == 0);

    _ret = yh_test_session_open(_work->_session);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_work->_session->_conn->_state == TCS_CONNECTING);

    return _ret;
}


static _int32 yh_test_serialize_down_file_request_cmd(DownFileReqCmd *cmd,
                                                      char **data,
                                                      _uint32 *data_len)
{
	_int32 _ret = YH_SUCCESS;
	char *_tmp_data = NULL;
	_uint32 _tmp_data_len = 0;

	YH_CHECK_PARAM_VALID(cmd);

	_tmp_data_len = P2P_CMD_HEADER_SIZE + sizeof(cmd->_file_name._str) +
		cmd->_file_name._len + sizeof(cmd->_file_size);
	YH_ASSERT(_tmp_data_len > 0);
	cmd->_header._cmd_len = _tmp_data_len - P2P_CMD_HEADER_SIZE;

	YH_MALLOC(_tmp_data, char *, _tmp_data_len);

	*data = _tmp_data;
	*data_len = _tmp_data_len;

	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._ver), 
		sizeof(cmd->_header._ver));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._cmd_type),
		sizeof(cmd->_header._cmd_type));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._cmd_len),
		sizeof(cmd->_header._cmd_len));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_file_name._len),
		sizeof(cmd->_file_name._len));
	YH_TEST_SET_DATA(&_tmp_data, cmd->_file_name._str, 
		cmd->_file_name._len);
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_file_size),
		sizeof(cmd->_file_size));
	
	return _ret;
}


static _int32 yh_test_wait_finish()
{
	_int32 _ret = YH_SUCCESS;

	if (gWaitForTestFinished == INVALID_HANDLE_VALUE) {
		_ret = yh_create_event(&gWaitForTestFinished);
		YH_CHECK_VALUE(_ret);
	}
	
	_ret = yh_wait(gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 yh_test_signal_finish()
{
	_int32 _ret = YH_SUCCESS;

	_ret = yh_signal(gWaitForTestFinished);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


/**
 * 1、连接成功后，发送文件下载请求命令
 * 2、准备接收服务器的响应
 **/
static _int32 yh_test_handle_connection_open(void *data, _int32 errcode)
{
    _int32 _ret = YH_SUCCESS;
    TestFileTransferDownWork *_work = (TestFileTransferDownWork *)data;
    TcpConnection *_conn = NULL;
    DownFileReqCmd _cmd = {0};
    char *_cmd_send_buf = NULL;
    _uint32 _cmd_send_len = 0;

    YH_CHECK_PARAM_VALID(data);

	LOG("connect open callback, errcode:%d.", errcode);

    _conn = _work->_session->_conn;
    YH_ASSERT(_conn != NULL);

    _cmd._header._ver = CURRENT_DOWN_FILE_PROTO_VER;
    _cmd._header._cmd_type = CMD_DOWN_FILE_REQ;
    _cmd._file_name._str = _work->_file_name._str;
	_cmd._file_name._len = _work->_file_name._len;
    _cmd._file_size = _work->_file_size;

    // NOTE
	// 这里面会设置命令长度
    _ret = yh_test_serialize_down_file_request_cmd(&_cmd, &_cmd_send_buf, 
        &_cmd_send_len);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_cmd_send_len > 0);
    YH_ASSERT(_cmd_send_buf != NULL);
	YH_ASSERT(_cmd._header._cmd_len > 0);

    _ret = yh_tcp_connection_send(_conn, _cmd_send_buf, _cmd_send_len, 
        yh_test_handle_connection_send_for_c);
    YH_CHECK_VALUE(_ret);

	YH_TEST_SESSION_ENTER_STATE(_work->_session, TSS_REQUEST_SENDED);

    _ret = yh_tcp_connection_recv(_conn, P2P_CMD_HEADER_SIZE,
        yh_test_handle_connection_recv_cmd_header_c);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 yh_test_handle_connection_close_for_s(void *data, _int32 errcode)
{
    _int32 _ret = YH_SUCCESS;
    TestFileTransferUpWork *_work = NULL;
    TestFileTransferSession *_session = NULL;

    LOG("close callback, data:0x%x, errcode:%d.",data, errcode);

    YH_CHECK_PARAM_VALID(data);

    _work = (TestFileTransferUpWork *)data;
    YH_ASSERT(_work != NULL);

    _session = (TestFileTransferSession *)_work->_session;
    YH_ASSERT(_session != NULL);

    _ret = yh_tcp_connection_destroy(_session->_conn);
    YH_CHECK_VALUE(_ret);

	YH_DELETE_OBJ(_session->_cmd_buffer);
	YH_DELETE_OBJ(_session);
	
	_ret = yh_fs_device_close(_work->_data_mgr->_file_device, NULL, 
		yh_test_fs_handle_close);
	YH_CHECK_VALUE(_ret);

	_ret = yh_fs_device_destroy(_work->_data_mgr->_file_device);
	YH_CHECK_VALUE(_ret);

	YH_DELETE_OBJ(_work->_data_mgr);
	YH_DELETE_OBJ(_work);

    _ret = yh_test_signal_finish();

    return _ret;
}


static _int32 yh_test_fs_handle_close(FsCloseReq *req)
{
	YH_CHECK_PARAM_VALID(req);

	LOG("device:0x%x closed.", req->_base->_device);

	return YH_SUCCESS;
}


static _int32 yh_test_handle_connection_close_for_c(void *data, _int32 errcode)
{
    _int32 _ret = YH_SUCCESS;
    TestFileTransferDownWork *_work = NULL;

    LOG("close callback, data:0x%x, errcode:%d.",data, errcode);

    YH_CHECK_PARAM_VALID(data);

    _work = (TestFileTransferDownWork *)data;
    YH_ASSERT(_work != NULL);

    _ret = yt_test_destroy_ft_work(_work);
	YH_CHECK_VALUE(_ret);

	_ret = yh_test_signal_finish();

    return _ret;
}


static _int32 yh_test_handle_connection_send_for_c(void *data, _int32 errcode)
{
	TestFileTransferDownWork *_work = NULL;
	TcpConnection *_conn = NULL;

	YH_CHECK_PARAM_VALID(data);

	LOG("send callback, data:0x%x, errcode:%d.", data, errcode);

	_work = (TestFileTransferDownWork *)data;
	_conn = _work->_session->_conn;

	YH_DELETE_OBJ(_conn->_send_buf);	

	return YH_SUCCESS;
}


static _int32 yh_test_handle_connection_send_for_s(void *data, _int32 errcode)
{
	TestFileTransferUpWork *_work = NULL;
	TcpConnection *_conn = NULL;

	YH_CHECK_PARAM_VALID(data);

	LOG("send callback, data:0x%x, errcode:%d.", data, errcode);

	_work = (TestFileTransferUpWork *)data;
	_conn = _work->_session->_conn;

	YH_DELETE_OBJ(_conn->_send_buf);	

	return YH_SUCCESS;
}


/**
 * 将头部信息从缓存中解析出来
 * @param data
 * @param data_len
 * @param header
 **/
static _int32 yh_test_parse_p2p_cmd_header(char *data, _uint32 data_len,
										   P2pCmdHeader *header)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(data);
	YH_CHECK_PARAM_VALID(header);
	YH_ASSERT(data_len >= P2P_CMD_HEADER_SIZE);

	YH_TEST_GET_DATA(&data, &(header->_ver), sizeof(header->_ver));
	YH_TEST_GET_DATA(&data, &(header->_cmd_type), sizeof(header->_cmd_type));
	YH_TEST_GET_DATA(&data, &(header->_cmd_len), sizeof(header->_cmd_len));

	return _ret;
}


static _int32 yh_test_handle_connection_recv_cmd_header_c(void *data,
														  _int32 errcode)
{
	TestFileTransferDownWork *_work = NULL;

	YH_CHECK_PARAM_VALID(data);

	LOG("recv cmd header, data:0x%x, errcode:%d.", data, errcode);

	_work = (TestFileTransferDownWork *)data;

	return yh_test_recv_cmd_header_helper(_work->_session, 
		_work->_session->_conn, yh_test_handle_connection_recv_cmd_body_c);
}


static _int32 yh_test_handle_connection_recv_error(TcpConnection *conn, 
                                                   _int32 errcode)
{
    _int32 _ret = YH_SUCCESS;
    LOG("errcode %d", errcode);

    YH_CHECK_PARAM_VALID(conn);

    if (errcode == YH_ERR_TCP_CONN_RMT_CLOSED) {
        _ret = yh_tcp_connection_close(conn, 
            yh_test_handle_connection_close_for_s);
        YH_CHECK_VALUE(_ret);
    }

    return _ret;
}


static _int32 yh_test_handle_connection_recv_cmd_header_s(void *data, 
														  _int32 errcode)
{
    _int32 _ret = YH_SUCCESS;
	TestFileTransferUpWork *_work = NULL;

	YH_CHECK_PARAM_VALID(data);

	LOG("recv cmd header, data:0x%x, errcode:%d.", data, errcode);

    _work = (TestFileTransferUpWork *)data;
    YH_ASSERT(_work != NULL);

    if (errcode != YH_SUCCESS) {
        _ret = yh_test_handle_connection_recv_error(_work->_session->_conn, 
            errcode);
        YH_CHECK_VALUE(_ret);
    }
    else {
        _ret = yh_test_recv_cmd_header_helper(_work->_session, 
            _work->_session->_conn, yh_test_handle_connection_recv_cmd_body_s);	
        YH_CHECK_VALUE(_ret);
    }

	return _ret;
}


/**
 * 1、收到命令头部之后，将其拷贝至session的命令缓冲区中
 * 2、继续接收命令的其他部分，设置session的状态
 **/
static _int32 yh_test_recv_cmd_header_helper(TestFileTransferSession *session,
	TcpConnection *conn, yh_tcp_connection_recv_callback callback)
{
    _int32 _ret = YH_SUCCESS;
    TcpConnection *_conn = NULL;
    TestFileTransferSession *_session = NULL;
	P2pCmdHeader _cmd_header = {0};

	YH_CHECK_PARAM_VALID(session);
	YH_CHECK_PARAM_VALID(conn);
	YH_CHECK_PARAM_VALID(callback);

    _session = session;
    _conn = conn;

	_ret = yh_test_parse_p2p_cmd_header(_conn->_recv_buf, 
        _conn->_exp_recv_len, &_cmd_header);
	YH_CHECK_VALUE(_ret);

    // 将接收缓冲区中的数据copy至session中
	// 直到收到一个完整的数据包才开始真正的处理
    memcpy(_session->_cmd_buffer+_session->_cmd_buffer_offset, 
        _conn->_recv_buf, _conn->_exp_recv_len);
    _session->_cmd_buffer_offset += _conn->_exp_recv_len;

	LOG("recvd cmd %d, cmd len %d.", _cmd_header._cmd_type, 
        _cmd_header._cmd_len);

    _ret = yh_tcp_connection_recv(_conn, _cmd_header._cmd_len, callback);
    YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 yh_test_handle_connection_recv_cmd_body_c(void *data,
														_int32 errcode)
{
	TestFileTransferDownWork *_work = NULL;

	YH_CHECK_PARAM_VALID(data);

	LOG("recv cmd body, data:0x%x, errcode:%d.", data, errcode);

	_work = (TestFileTransferDownWork *)data;

	return yh_test_recv_cmd_body_helper(_work->_session, 
		_work->_session->_conn);
}


static _int32 yh_test_handle_connection_recv_cmd_body_s(void *data,
														_int32 errcode)
{
    _int32 _ret = YH_SUCCESS;
	TestFileTransferUpWork *_work = NULL;

	YH_CHECK_PARAM_VALID(data);

	LOG("recv cmd body, data:0x%x, errcode:%d.", data, errcode);

	_work = (TestFileTransferUpWork *)data;
    YH_ASSERT(_work != NULL);

    if (errcode != YH_SUCCESS) {
        _ret = yh_test_handle_connection_recv_error(_work->_session->_conn, 
            errcode);
        YH_CHECK_VALUE(_ret);
    }
    else {
        _ret = yh_test_recv_cmd_body_helper(_work->_session, 
            _work->_session->_conn);
        YH_CHECK_VALUE(_ret);
    }

	return _ret;
}


/**
 * 到这里，一个完整的协议包就算收到了，需要根据具体的命令类型处理
 **/
static _int32 yh_test_recv_cmd_body_helper(TestFileTransferSession *session,
										   TcpConnection *conn)
{
    _int32 _ret = YH_SUCCESS;
    P2pCmdHeader _cmd_header = {0};
    TestFileTransferSession *_session = NULL;
    TcpConnection *_conn = NULL;

	YH_CHECK_PARAM_VALID(session);
	YH_CHECK_PARAM_VALID(conn);

    _session = session;
    _conn = conn;

    YH_ASSERT(_session->_cmd_buffer != NULL);
    YH_ASSERT(_session->_cmd_buffer_offset >= P2P_CMD_HEADER_SIZE);

	// FIXME
	// 这里竟然需要2次解析，应该有好的方法可以避免
    _ret = yh_test_parse_p2p_cmd_header(_session->_cmd_buffer, 
		P2P_CMD_HEADER_SIZE, &_cmd_header);
    YH_CHECK_VALUE(_ret);

    LOG("_cmd_header._cmd_len:%u, _conn->_recv_exp_len:%u.", 
        _cmd_header._cmd_len, _conn->_exp_recv_len);

    /**
     * [FIXME][NOTE]
     * 务必要注意这里的_conn->_recv_exp_len，在接收的时候会由于不完整的
     * 接收而改变。后续思考怎样的方式更好！
     **/
    //YH_ASSERT(_cmd_header._cmd_len == _conn->_recv_exp_len);

    // 将接收缓冲区中的数据copy至session中
    // 至此，一个命令才算接收完整
    memcpy(_session->_cmd_buffer+_session->_cmd_buffer_offset, 
        _conn->_recv_buf, _cmd_header._cmd_len);
    _session->_cmd_buffer_offset += _cmd_header._cmd_len;

    // FIXME 可以使用宏来简化之
    switch(_cmd_header._cmd_type) {
        case CMD_DOWN_FILE_REQ:
            _ret = yh_test_handle_down_file_req(_conn);
            break;
        case CMD_DOWN_FILE_RESP:
            _ret = yh_test_handle_down_file_resp(_conn);
            break;
        case CMD_FILE_RANGE_REQ:
            _ret = yh_test_handle_file_range_req(_conn);
            break;
        case CMD_FILE_RANGE_RESP:
            _ret = yh_test_handle_file_range_resp(_conn);
            break;
        default:
            YH_ASSERT(FALSE);
            break;
    }

	// FIXME
	// 真丑陋！
	// 用完该条连接上的recv_buf需要清空
	memset(_session->_cmd_buffer, 0, MAX_P2P_PACKET_SIZE);
	_session->_cmd_buffer_offset = 0;

    return _ret;
}


// server
static _int32 yh_test_handle_down_file_req(TcpConnection *conn)
{
	_int32 _ret = YH_SUCCESS;
    DownFileReqCmd _cmd = {0};
	TestFileTransferUpWork *_work = NULL;
	TcpDevice *_device = NULL;
	YHString _file_path;

	YH_CHECK_PARAM_VALID(conn);

	_work = (TestFileTransferUpWork *)conn->_user_data;
	YH_ASSERT(_work != NULL);
	YH_ASSERT(_work->_data_mgr != NULL);

	_device = (TcpDevice *)conn->_device;
	YH_ASSERT(_device != NULL);
	YH_ASSERT(_device->_device->loop != NULL);

	/**
	 * FIXME
	 * 文件名称要用固定长度，且不论编程的复杂度，单从系统的安全性角度也
	 * 是不可取的，如果外部传进来一个超长的串，那极易引发溢出攻击！
	 **/
	YH_MALLOC(_cmd._file_name._str, char *, 256);

    _ret = yh_test_parse_down_file_req(_work->_session->_cmd_buffer, &_cmd);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_cmd._header._cmd_type == CMD_DOWN_FILE_REQ);
    YH_ASSERT(_cmd._header._ver >= CURRENT_DOWN_FILE_PROTO_VER);

	// 构建本条连接上的数据管理
	// 默认路径在E://temp下面
	LOG("request file name:%s, file name len is:%d.", 
		_cmd._file_name._str, _cmd._file_name._len);

	_file_path._str = "E://temp//";
	_file_path._len = strlen(_file_path._str);

	if (_cmd._file_name._len != 0) {
		/**
		 * FIXME
		 * 我觉得这里的user_data设置成work会比较好
		 **/
		_ret = yh_fs_device_create(_device->_device->loop, (void *)conn, 
			&_work->_data_mgr->_file_device);
		YH_CHECK_VALUE(_ret);

		_ret = yh_fs_device_open(_work->_data_mgr->_file_device, FO_READ, 
			_file_path, _cmd._file_name, (void *)conn, 
			yh_test_handle_up_file_open);
		YH_CHECK_VALUE(_ret);
	}

	YH_DELETE_OBJ(_cmd._file_name._str);
    
    return _ret;
}


static _int32 yh_test_handle_up_file_open(FsOpenReq *req)
{
	_int32 _ret = YH_SUCCESS;
	FSDevice *_fs_device = NULL;
	TcpConnection *_conn = NULL;
	BOOL _resp_result = TRUE;

	YH_CHECK_PARAM_VALID(req);

	_fs_device = (FSDevice *)req->_base->_device;
	YH_ASSERT(_fs_device != NULL);

	_conn = (TcpConnection *)_fs_device->_user_data;
	YH_ASSERT(_conn != NULL);

	if (req->_base->_err_code != YH_SUCCESS) {
		_resp_result = FALSE;
		LOG("can't open file.");
	}

	_ret = yh_test_send_down_file_resp(_conn, _resp_result);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


/**
 * 向对方发送下载文件请求的响应
 * @param conn 
 * @param result 请求的结果，本地存在该文件，就回应true，否则false
 **/
static _int32 yh_test_send_down_file_resp(TcpConnection *conn, BOOL result)
{
    _int32 _ret = YH_SUCCESS;
    DownFileRespCmd _cmd = {0};
    char *_cmd_send_buf = NULL;
    _uint32 _cmd_send_len = 0;

    YH_CHECK_PARAM_VALID(conn);

    _cmd._header._cmd_type = CMD_DOWN_FILE_RESP;
    _cmd._header._ver = CURRENT_DOWN_FILE_PROTO_VER;
	_cmd._result = result ? (_uint8)1 : (_uint8)0;

    _ret = yh_test_serialize_down_file_response_cmd(&_cmd, &_cmd_send_buf, 
        &_cmd_send_len);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_cmd_send_len > 0);
    YH_ASSERT(_cmd_send_buf != NULL);

    _ret = yh_tcp_connection_send(conn, _cmd_send_buf, _cmd_send_len, 
        yh_test_handle_connection_send_for_s);
    YH_CHECK_VALUE(_ret);

    _ret = yh_tcp_connection_recv(conn, P2P_CMD_HEADER_SIZE,
        yh_test_handle_connection_recv_cmd_header_s);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 yh_test_serialize_down_file_response_cmd(DownFileRespCmd *cmd,
                                                       char **data,
                                                       _uint32 *data_len)
{
	_int32 _ret = YH_SUCCESS;
	char *_tmp_data = NULL;
	_uint32 _tmp_data_len = 0;

	YH_CHECK_PARAM_VALID(cmd);

	_tmp_data_len = P2P_CMD_HEADER_SIZE + sizeof(cmd->_result);
	YH_ASSERT(_tmp_data_len > 0);
	cmd->_header._cmd_len = _tmp_data_len - P2P_CMD_HEADER_SIZE;

	YH_MALLOC(_tmp_data, char *, _tmp_data_len);

	*data = _tmp_data;
	*data_len = _tmp_data_len;

	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._ver), 
		sizeof(cmd->_header._ver));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._cmd_type),
		sizeof(cmd->_header._cmd_type));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._cmd_len),
		sizeof(cmd->_header._cmd_len));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_result),
		sizeof(cmd->_result));

	return _ret;
}


static _int32 yh_test_parse_down_file_req(char *data, DownFileReqCmd *cmd)
{
	_int32 _ret = YH_SUCCESS;
	char *_tmp_buffer = data;

	YH_CHECK_PARAM_VALID(data);
	YH_CHECK_PARAM_VALID(cmd);

	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._ver), 
		sizeof(cmd->_header._ver));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._cmd_type), 
		sizeof(cmd->_header._cmd_type));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._cmd_len), 
		sizeof(cmd->_header._cmd_len));
	// FIXME
	// 后面的需要根据cmd_len来进行解析否则兼容性就无从考虑了
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_file_name._len), 
		sizeof(cmd->_file_name._len));
	YH_TEST_GET_DATA(&_tmp_buffer, cmd->_file_name._str, cmd->_file_name._len);
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_file_size), 
		sizeof(cmd->_file_size));

	return _ret;	
}


// client
static _int32 yh_test_handle_down_file_resp(TcpConnection *conn)
{
    _int32 _ret = YH_SUCCESS;
    DownFileRespCmd _cmd = {0};
    TestFileTransferDownWork *_work = NULL;
    TestFileTransferSession *_session = NULL;
	TcpDevice *_tcp_device = NULL;
	TestFileDataManager *_data_mgr = NULL;
	YHString _file_path;

    YH_CHECK_PARAM_VALID(conn);
    YH_ASSERT(conn->_user_data != NULL);

    _work = (TestFileTransferDownWork *)conn->_user_data;
	YH_ASSERT(_work != NULL);

    _session = _work->_session;
	YH_ASSERT(_session != NULL);

	_tcp_device = conn->_device;
	YH_ASSERT(_tcp_device != NULL);

	_data_mgr = _work->_data_mgr;
	YH_ASSERT(_data_mgr != NULL);

    _ret = yh_test_parse_down_file_resp(_session->_cmd_buffer, &_cmd);
    YH_CHECK_VALUE(_ret);

    if (_cmd._result == 0) {
        LOG("");
        return -1;
    }

	_ret = yh_fs_device_create(_tcp_device->_device->loop, (void *)conn, 
		&_data_mgr->_file_device);
	YH_CHECK_VALUE(_ret);

	_file_path._str = "E:\\temp\\down\\";
	_file_path._len = strlen(_file_path._str);

	_ret = yh_fs_device_open(_data_mgr->_file_device, FO_WRITE, _file_path, 
		_work->_file_name, (void *)conn, yh_test_handle_down_file_open);
	YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 yh_test_handle_down_file_open(FsOpenReq *req)
{
	_int32 _ret = YH_SUCCESS;
	TcpConnection *_conn = NULL;

	YH_CHECK_PARAM_VALID(req);

	_conn = (TcpConnection *)req->_base->_user_data;
	YH_ASSERT(_conn != NULL);

	_ret = yh_test_send_file_range_request(_conn);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 yh_test_send_file_range_request(TcpConnection *conn)
{
    _int32 _ret = YH_SUCCESS;
    FileRangeReqCmd _cmd = {0};
    TestFileTransferDownWork *_work = NULL;
	char *_cmd_send_buf = NULL;
	_uint32 _cmd_send_len = 0;

    YH_CHECK_PARAM_VALID(conn);

    _work = (TestFileTransferDownWork *)conn->_user_data;
    YH_ASSERT(_work != NULL);

    _cmd._header._cmd_type = CMD_FILE_RANGE_REQ;
    _cmd._header._ver = CURRENT_DOWN_FILE_PROTO_VER;
    _cmd._file_range._pos = _work->_data_mgr->_cur_file_pos;
	/**
	 * NOTE
	 * 由于这里是顺序下载的，所以这里用这种方式来控制请求的长度是没问题
	 * 正式的情况下，用BT的文件下载地图是个很好的方法。
	 **/
	LOG("file_size %llu, cur_file_pos %llu", _work->_file_size, 
		_work->_data_mgr->_cur_file_pos);

	if (_work->_file_size - _work->_data_mgr->_cur_file_pos < 
		TEST_REQ_FILE_LEN_PER) {
		_cmd._file_range._len 
			= (_uint32)(_work->_file_size - _work->_data_mgr->_cur_file_pos);
	}
	else {
		_cmd._file_range._len = TEST_REQ_FILE_LEN_PER;
	}

	LOG("request file_range[%llu, %u]", _cmd._file_range._pos, 
		_cmd._file_range._len);

    // NOTE
    // 这里面会设置命令长度
    _ret = yh_test_serialize_file_range_request_cmd(&_cmd, &_cmd_send_buf, 
        &_cmd_send_len);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_cmd_send_len > 0);
    YH_ASSERT(_cmd_send_buf != NULL);

    _ret = yh_tcp_connection_send(conn, _cmd_send_buf, _cmd_send_len, 
        yh_test_handle_connection_send_for_c);
    YH_CHECK_VALUE(_ret);

    _ret = yh_tcp_connection_recv(conn, P2P_CMD_HEADER_SIZE,
        yh_test_handle_connection_recv_cmd_header_c);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 yh_test_serialize_file_range_request_cmd(FileRangeReqCmd *cmd,
                                                       char **data, 
                                                       _uint32 *data_len)
{
	_int32 _ret = YH_SUCCESS;
	char *_tmp_data = NULL;
	_uint32 _tmp_data_len = 0;

	YH_CHECK_PARAM_VALID(cmd);

	_tmp_data_len = P2P_CMD_HEADER_SIZE + sizeof(cmd->_file_range._len) 
		+ sizeof(cmd->_file_range._pos);
	YH_ASSERT(_tmp_data_len > 0);
	cmd->_header._cmd_len = _tmp_data_len - P2P_CMD_HEADER_SIZE;

	YH_MALLOC(_tmp_data, char *, _tmp_data_len);

	*data = _tmp_data;
	*data_len = _tmp_data_len;

	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._ver), 
		sizeof(cmd->_header._ver));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._cmd_type),
		sizeof(cmd->_header._cmd_type));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._cmd_len),
		sizeof(cmd->_header._cmd_len));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_file_range._len),
		sizeof(cmd->_file_range._len));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_file_range._pos),
		sizeof(cmd->_file_range._pos));

	return _ret;
}


static _int32 yh_test_parse_down_file_resp(char *data, DownFileRespCmd *cmd)
{
	_int32 _ret = YH_SUCCESS;
	char *_tmp_buffer = data;

	YH_CHECK_PARAM_VALID(data);
	YH_CHECK_PARAM_VALID(cmd);

	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._ver), 
		sizeof(cmd->_header._ver));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._cmd_type), 
		sizeof(cmd->_header._cmd_type));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._cmd_len), 
		sizeof(cmd->_header._cmd_len));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_result), 
		sizeof(cmd->_result));

	return _ret;
}


// server
static _int32 yh_test_handle_file_range_req(TcpConnection *conn)
{
    _int32 _ret = YH_SUCCESS;
    TestFileTransferUpWork *_work = NULL;
    TestFileTransferSession *_sesson = NULL;
    FileRangeReqCmd _cmd = {0};
    char *_file_cont_buf = NULL;

    YH_CHECK_PARAM_VALID(conn);

    _work = (TestFileTransferUpWork *)conn->_user_data;
    YH_ASSERT(_work != NULL);
    _sesson = (TestFileTransferSession *)_work->_session;
    YH_ASSERT(_sesson != NULL);

    _ret = yh_test_parse_file_range_req(_sesson->_cmd_buffer, &_cmd);
    YH_CHECK_VALUE(_ret);

    _ret = yh_test_load_data_from_file(_work, _cmd._file_range._pos, 
		_cmd._file_range._len, &_file_cont_buf, 
		yh_test_handle_up_file_read);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


/**
 * 针对具体的错误进行处理，TODO
 **/
static _int32 yh_test_handle_error(_int32 errcode)
{
	LOG("errcode:%d.", errcode);
	return YH_SUCCESS;
}


static _int32 yh_test_handle_up_file_read(FsReadReq *req)
{
	_int32 _ret = YH_SUCCESS;
	FSDevice *_fs_device = NULL;
	TcpConnection *_conn = NULL;
	
	YH_CHECK_PARAM_VALID(req);

	_fs_device = (FSDevice *)req->_base->_device;
	YH_ASSERT(_fs_device != NULL);

	_conn = (TcpConnection *)_fs_device->_user_data;
	YH_ASSERT(_conn != NULL);

	LOG("req:0x%x, read %u bytes, errcode:%d.", req, req->_read_len, 
		req->_base->_err_code);

	if (req->_base->_err_code != YH_SUCCESS) {
		LOG("read [%llu, %u] error.", req->_start_pos, req->_read_len);
		YH_ASSERT(FALSE);
		_ret = yh_test_handle_error(YH_ERR_READ_FILE_FAIL);
		return _ret;
	}

	/**
	 * TODO
	 * 是读到多少发送多少呢，还是请求多少要等到读完整再次发送呢？
	 * 应该是后者，这里应该需要等待并且缓存到需要读取的字节数
	 **/
	YH_ASSERT(_fs_device->_exp_read_len == req->_read_len);

	_ret = yh_test_send_file_range_response(_conn, req->_start_pos, 
		req->_read_len, _fs_device->_read_buf, req->_read_len);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 yh_test_send_file_range_response(TcpConnection *conn, 
                                               _uint64 pos, _uint32 len,
                                               char *cont_buf, _uint32 cont_len)
{
    _int32 _ret = YH_SUCCESS;
    FileRangeRespCmd _cmd = {0};
    char *_send_buf = NULL;
    _uint32 _send_buf_len = 0;

    YH_CHECK_PARAM_VALID(conn);
    YH_CHECK_PARAM_VALID(cont_buf);

    _cmd._header._cmd_type = CMD_FILE_RANGE_RESP;
    _cmd._header._ver = CURRENT_DOWN_FILE_PROTO_VER;
    _cmd._file_cont = cont_buf;
    _cmd._file_cont_len = cont_len;
    _cmd._result = 1;
    _cmd._file_range._pos = pos;
    _cmd._file_range._len = len;

    _ret = yh_test_serialize_file_range_response_cmd(&_cmd, &_send_buf, 
        &_send_buf_len);
    YH_CHECK_VALUE(_ret);

	YH_DELETE_OBJ(cont_buf);

    _ret = yh_tcp_connection_send(conn, _send_buf, _send_buf_len, 
        yh_test_handle_connection_send_for_s);
    YH_CHECK_VALUE(_ret);

    _ret = yh_tcp_connection_recv(conn, P2P_CMD_HEADER_SIZE, 
        yh_test_handle_connection_recv_cmd_header_s);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 yh_test_serialize_file_range_response_cmd(FileRangeRespCmd *cmd, 
                                                        char **data, 
                                                        _uint32 *data_len)
{
	_int32 _ret = YH_SUCCESS;
	char *_tmp_data = NULL;
	_uint32 _tmp_data_len = 0;

	YH_CHECK_PARAM_VALID(cmd);

	_tmp_data_len = P2P_CMD_HEADER_SIZE + sizeof(cmd->_result) + 
		sizeof(cmd->_file_range._len) + sizeof(cmd->_file_range._pos) + 
		sizeof(cmd->_file_cont_len) + cmd->_file_cont_len;
	YH_ASSERT(_tmp_data_len > 0);
	cmd->_header._cmd_len = _tmp_data_len - P2P_CMD_HEADER_SIZE;

	YH_MALLOC(_tmp_data, char *, _tmp_data_len);

	*data = _tmp_data;
	*data_len = _tmp_data_len;

	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._ver), 
		sizeof(cmd->_header._ver));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._cmd_type),
		sizeof(cmd->_header._cmd_type));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_header._cmd_len),
		sizeof(cmd->_header._cmd_len));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_result),
		sizeof(cmd->_result));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_file_range._len),
		sizeof(cmd->_file_range._len));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_file_range._pos),
		sizeof(cmd->_file_range._pos));
	YH_TEST_SET_DATA(&_tmp_data, &(cmd->_file_cont_len),
		sizeof(cmd->_file_cont_len));
	YH_TEST_SET_DATA(&_tmp_data, cmd->_file_cont, cmd->_file_cont_len);

	return _ret;
}


static _int32 yh_test_load_data_from_file(TestFileTransferUpWork *work, 
										  _uint64 pos, _uint32 len,
                                          char **cont_buf,
										  yh_fs_device_read_callback handler)
{
	_int32 _ret = YH_SUCCESS;


	YH_CHECK_PARAM_VALID(work);
	YH_ASSERT(len != 0);

	LOG("pos:%llu, len:%u.", pos, len);

	YH_MALLOC(*cont_buf, char *, len);

	_ret = yh_fs_device_read(work->_data_mgr->_file_device, pos, len, 
		*cont_buf, len, NULL, handler);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 yh_test_parse_file_range_req(char *data, FileRangeReqCmd *cmd)
{
	_int32 _ret = YH_SUCCESS;
	char *_tmp_buffer = data;

	YH_CHECK_PARAM_VALID(data);
	YH_CHECK_PARAM_VALID(cmd);

	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._ver), 
		sizeof(cmd->_header._ver));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._cmd_type), 
		sizeof(cmd->_header._cmd_type));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._cmd_len), 
		sizeof(cmd->_header._cmd_len));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_file_range._len), 
		sizeof(cmd->_file_range._len));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_file_range._pos), 
		sizeof(cmd->_file_range._pos));

	return _ret;
}


static _int32 yh_test_handle_down_file_finished(TcpConnection *conn)
{
	_int32 _ret = YH_SUCCESS;
	TestFileTransferDownWork *_work = NULL;
	TestFileTransferSession *_session = NULL;

	YH_CHECK_PARAM_VALID(conn);

	_work = (TestFileTransferDownWork *)conn->_user_data;
	YH_ASSERT(_work != NULL);

	_session = _work->_session;	
	YH_ASSERT(_session != NULL);

	_ret = yh_tcp_connection_close(conn, 
        yh_test_handle_connection_close_for_c);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


// client
static _int32 yh_test_handle_file_range_resp(TcpConnection *conn)
{
    _int32 _ret = YH_SUCCESS;
    FileRangeRespCmd _cmd = {0};
    TestFileTransferDownWork *_work = NULL;
    TestFileTransferSession *_session = NULL;

    YH_CHECK_PARAM_VALID(conn);
    YH_ASSERT(conn->_user_data != NULL);

    _work = (TestFileTransferDownWork *)conn->_user_data;
    _session = _work->_session;

    _ret = yh_test_parse_file_range_resp(_session->_cmd_buffer, &_cmd);
    YH_CHECK_VALUE(_ret);

    if (_cmd._result == 0) {
        LOG("error from cmd file_range_response, so return.");
        return -1;
    }

    // 将数据存到文件中
    _ret = yh_test_write_data_to_file(_work->_data_mgr->_file_device, 
		_cmd._file_range._pos, _cmd._file_cont, _cmd._file_cont_len);
    YH_CHECK_VALUE(_ret);

    // 看是否已经传输完毕
    if (_work->_file_size <= _cmd._file_range._len + 
        _work->_data_mgr->_cur_file_pos) {
        LOG("transmission finished.");

		/**
		 * NOTE
		 * 由于目前的测试环境是单线程的，所以这里使用了一个临时变量来
		 * 标志文件下载完成，如果是多线程的，主线程的析构需要等待辅助
		 * 线程的结束或者处理，比如等待文件线程的读写完成事件
		 **/
		_work->_down_finished = TRUE;
    }
    else {
        _work->_data_mgr->_cur_file_pos += _cmd._file_cont_len;
        _ret = yh_test_send_file_range_request(conn);
        YH_CHECK_VALUE(_ret);    
    }

    return _ret;
}


static _int32 yh_test_write_data_to_file(FSDevice *fs_device, _uint64 pos,
										 char *cont, _uint32 cont_len)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(fs_device);
	YH_CHECK_PARAM_VALID(cont);

	_ret = yh_fs_device_write(fs_device, pos, cont_len, cont, NULL,  
		yh_test_handle_down_file_write);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 yh_test_handle_down_file_write(FsWriteReq *req)
{
	_int32 _ret = YH_SUCCESS;
	FSDevice *_des_device = NULL;
	TcpConnection *_conn = NULL;
	TestFileTransferDownWork *_down_work = NULL;

	YH_CHECK_PARAM_VALID(req);
	
	_des_device = (FSDevice *)req->_base->_device;
	YH_ASSERT(_des_device != NULL);

	_conn = (TcpConnection *)_des_device->_user_data;
	YH_ASSERT(_conn != NULL);

	_down_work = (TestFileTransferDownWork *)_conn->_user_data;
	YH_ASSERT(_down_work != NULL);

	YH_DELETE_OBJ(_des_device->_write_buf);

	if (req->_base->_err_code != YH_SUCCESS) {
		LOG("write [%llu, %u] fail.", req->_start_pos, req->_write_len);
		YH_ASSERT(FALSE);
		return YH_ERR_WRITE_FILE_FAIL;
	}

	LOG("write [%llu, %u] complete.", req->_start_pos, req->_write_len);

	/**
	 * NOTE
	 * 如果文件处理和业务处理不在一个线程中，这里应该是一个事件通知来
	 * 告知上层可以结束了。
	 **/
	if (_down_work->_down_finished) {
		_ret = yh_test_handle_down_file_finished(_conn);
		YH_CHECK_VALUE(_ret);
	}

	return _ret;
}


static _int32 yh_test_parse_file_range_resp(char *data, FileRangeRespCmd *cmd)
{
	_int32 _ret = YH_SUCCESS;
	char *_tmp_buffer = data;

	YH_CHECK_PARAM_VALID(data);
	YH_CHECK_PARAM_VALID(cmd);

	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._ver), 
		sizeof(cmd->_header._ver));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._cmd_type), 
		sizeof(cmd->_header._cmd_type));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_header._cmd_len), 
		sizeof(cmd->_header._cmd_len));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_result), 
		sizeof(cmd->_result));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_file_range._len), 
		sizeof(cmd->_file_range._len));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_file_range._pos), 
		sizeof(cmd->_file_range._pos));
	YH_TEST_GET_DATA(&_tmp_buffer, &(cmd->_file_cont_len), 
		sizeof(cmd->_file_cont_len));
    if (cmd->_file_cont_len) {
        LOG("recvd %d bytes file content from server.", cmd->_file_cont_len);
        YH_MALLOC(cmd->_file_cont, char *, cmd->_file_cont_len);
    }
	YH_TEST_GET_DATA(&_tmp_buffer, cmd->_file_cont, 
		cmd->_file_cont_len);

	return _ret;
}
