/**
 * 自定义的各类API所需参数结构体
 *
 */

#ifndef _DOWNLOAD_API_PARAM_STRUCT_H_
#define _DOWNLOAD_API_PARAM_STRUCT_H_


#include "base/define.h"


struct EventLoop_s;


typedef enum CreateWorkType_s CreateWorkType;
typedef struct TestAsynSockParam_s TestAsynSockParam;
typedef struct TestTransFileParam_s TestTransFileParam;
typedef struct CreateWorkParam_s CreateWorkParam;


struct TestAsynSockParam_s {
    struct EventLoop_s			*_loop;
};


struct TestTransFileParam_s {
	struct EventLoop_s			*_loop;
	const char					*_filename;
	_uint32						_filename_len;
};


/**
 * 创建work的参数封装对象
 * @param _ctype 创建work的类型
 *  @see CreateWorkType_s
 * @param _data	创建work时，携带的外部参数
 */
enum CreateWorkType_s {BY_URL, BY_ED2K, BY_BT_TORRENT};
struct CreateWorkParam_s {
	CreateWorkType	_type; 
	void			*_data;
};


#endif//_DOWNLOAD_API_PARAM_STRUCT_H_
