/**
 * 测试消息队列的实现
 *
 */

#ifndef _LIBYH_TEST_MESSAGE_QUEUE_H_
#define _LIBYH_TEST_MESSAGE_QUEUE_H_


#include "base/define_data_type.h"


/**
 * 测试MessageQueue的生命周期管理
 * 创建、删除
 */
_int32 yh_test_mq_life();


/**
 * 测试MessageQueue相关操作
 * 插入、弹出、大小、判空
 */
_int32 yh_test_mq_op();


/**
 * 测试mq在多线程环境中的使用
 **/
//_int32 yh_test_mq_op_in_multi_threads();


#endif//_LIBYH_TEST_MESSAGE_QUEUE_H_