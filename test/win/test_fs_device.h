/**
 * 测试文件设备的读写
 **/


#ifndef _LIBYH_TEST_FS_DEVICE_H_
#define _LIBYH_TEST_FS_DEVICE_H_


#include "base/define_data_type.h"
#include "fs/fs_device.h"


/**
 * 测试fs设备的基本操作
 **/
_int32 yh_test_fs_base_op();


struct TestFsDeviceOpFuncParam_s {
    EventLoop		*_loop;
    YHString		_src_file_path;
    YHString		_src_file_name;
    YHString        _des_file_path;
    YHString        _des_file_name;
};


struct TestFsDeviceWorkUserData_s {
    FSDevice		*_src_fs_device;	// 读取的文件设备
    FSDevice      *_des_fs_device;    // 写入的文件设备
    _uint64			_readed_size;		// 已经读了多少字节
    _uint64			_writed_size;		// 已经写了多少字节
    YHString		_src_file_name;
    YHString		_des_file_name;
    _uint64         _file_size;         // 原始文件的大小，只是为了测试
};


#endif//_LIBYH_TEST_FS_DEVICE_H_
