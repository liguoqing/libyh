/**
 * 一个新的下载库雏形设计
 *
 */
#if defined(_DEBUG) && defined(WIN32)
    #define _CRTDBG_MAP_ALLOC
    #include <stdio.h>
    #include <crtdbg.h>
#elif defined(LINUX)
// 不知道linux上怎么调试内存泄露
#endif

#include "test_message_queue.h"
#include "test_event_loop.h"
#include "test_socket.h"
#include "test_list.h"
#include "test_queue.h"
#include "test_tcp_connection.h"
#include "test_fs_device.h"


void enable_crt_mem_check()
{
#if defined(_DEBUG) && defined(WIN32)
	_CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF);
#endif
}


#define TEST_INVALID_OP					-1
#define TEST_NO_OP						0
#define TEST_FOR_SOCKET_SERVER_SYN      1       
#define TEST_FOR_SOCKET_CLIENT_SYN      2
#define TEST_FOR_BASE_LIB_LIST			3
#define TEST_FOR_BASE_LIB_QUEUE			4
#define TEST_FOR_SOCKET_SERVER_ASYN		5
#define TEST_FOR_SOCKET_CLIENT_ASYN		6
#define TEST_FOR_TRANSFER_FILE_SERVER	7
#define TEST_FOR_TRANSFER_FILE_CLIENT	8
#define TEST_FOR_FILE_DEVICE			9


#define MAX_ARGV_OP_SIZE				3
#define MAX_ARGV_VALUE_SIZE				16


/**
 * 解析main函数的参数
 * @param argc 参数的个数
 * @param argvs 所有的参数，包括程序名称
 * @return 
 *  error: TEST_INVALID_OP
 *  nopar: TEST_NO_OP
 *  -tser: TEST_FOR_SOCKET_SERVER
 *  -tcli: TEST_FOR_SOCKET_CLIENT
 *	-tlis: TEST_FOR_BASE_LIB_LIST
 *  -tque: TEST_FOR_BASE_LIB_QUEUE
 *	-taser: TEST_FOR_SOCKET_SERVER_ASYN
 *	-tacli: TEST_FOR_SOCKET_CLIENT_ASYN
 *	-ttfs: TEST_FOR_TRANSFER_FILE_SERVER
 *	-ttfc: TEST_FOR_TRANSFER_FILE_CLIENT
 *	-tfd: TEST_FOR_FILE_DEVICE
 **/
static _int32 parse_main_params(int argc, char **argvs)
{
    const char *_test_type_op = "-t";			// 建立什么类型的测试程序
    const char *_socket_type_value_1 = "ser";   // creating for a server 
    const char *_socket_type_value_2 = "cli";   // creating for a client 
	const char *_base_lib_list = "lis";			// 基础库list的测试
    const char *_base_lib_queue = "que";        // 基础库queue的测试
	const char *_socket_server_asyn = "aser";
	const char *_socket_client_asyn = "acli";
	const char *_trans_file_server = "tfs";
	const char *_trans_file_client = "tfc";
	const char *_file_device = "fd";

	// TODO 
	// 将这些测试解析用数组来保存
	// 下一步用配置文件来保存
	/*const char *_test_type_values[] = {
		"ser", "cli", "lis"
	};*/	

    char *_argv = NULL;
    char _argv_op[MAX_ARGV_OP_SIZE] = {0};
    char _argv_value[MAX_ARGV_VALUE_SIZE] = {0};
    int i = 1;

    LOG("argc:%d.", argc);

    if (argc > 1) {
        for (; i < argc; ++ i) {
            _argv = argvs[i];
            strncpy(_argv_op, _argv, 2);
            LOG("argv:%s, argv_op:%s.", _argv, _argv_op);
            if (strcmp(_argv_op, _test_type_op) == 0) {
                strncpy(_argv_value, _argv+2, MIN(MAX_ARGV_VALUE_SIZE, 
					strlen(_argv)-2));
                LOG("argv_value:%s.", _argv_value);

                if (strcmp(_argv_value, _socket_type_value_1) == 0) {
                    LOG("-tser, return 1.");
                    return TEST_FOR_SOCKET_SERVER_SYN;
                }
                else if (strcmp(_argv_value, _socket_type_value_2) == 0) {
                    LOG("-tcli, return 2.");
                    return TEST_FOR_SOCKET_CLIENT_SYN;
                }
				else if (strcmp(_argv_value, _base_lib_list) == 0) {
					LOG("-tlis, return 3.");
					return TEST_FOR_BASE_LIB_LIST;
				}
                else if (strcmp(_argv_value, _base_lib_queue) == 0) {
                    LOG("-tque, return %d.", TEST_FOR_BASE_LIB_QUEUE);
                    return TEST_FOR_BASE_LIB_QUEUE;
                }
				else if (strcmp(_argv_value, _socket_server_asyn) == 0) {
					LOG("-tque, return %d.", TEST_FOR_SOCKET_SERVER_ASYN);
					return TEST_FOR_SOCKET_SERVER_ASYN;
				}
				else if (strcmp(_argv_value, _socket_client_asyn) == 0) {
					LOG("-tque, return %d.", TEST_FOR_SOCKET_CLIENT_ASYN);
					return TEST_FOR_SOCKET_CLIENT_ASYN;
				}
				else if (strcmp(_argv_value, _trans_file_server) == 0) {
					LOG("-tque, return %d.", TEST_FOR_TRANSFER_FILE_SERVER);
					return TEST_FOR_TRANSFER_FILE_SERVER;
				}
				else if (strcmp(_argv_value, _trans_file_client) == 0) {
					LOG("-tque, return %d.", TEST_FOR_TRANSFER_FILE_CLIENT);
					return TEST_FOR_TRANSFER_FILE_CLIENT;
				}
				else if (strcmp(_argv_value, _file_device) == 0) {
					LOG("-tfd, return %d.", TEST_FOR_FILE_DEVICE);
					return TEST_FOR_FILE_DEVICE;
				}
            }
            else {
                LOG("invalid op.");
                return TEST_INVALID_OP;
            }

            memset(_argv_op, 0, MAX_ARGV_OP_SIZE);
            memset(_argv_value, 0, MAX_ARGV_VALUE_SIZE);
        }        
    }
    else {
        LOG("no param, return 0.");
        return TEST_NO_OP;
    }

    return TEST_NO_OP;
}


/**
 * @param argc 
 * @param argvs 
 *  download -t[ser|cli|lis|que|aser|acli]
 **/
int main(int argc, char **argvs)
{
	_int32 ret = 0;
    //const _int32 product_id = 10;
    _int32 _create_op = 0;

	_create_op = parse_main_params(argc, argvs);
	_create_op = TEST_FOR_FILE_DEVICE;	// for test
    
	/*if (_create_op == TEST_INVALID_OP || _create_op == TEST_NO_OP) {
		LOG("[hint] download -t[ser|cli|lis|que|aser|acli]");
		return 0;
	}*/

	enable_crt_mem_check();

	//_CrtSetBreakAlloc(64);
	
	//pass
	//ret = yh_test_event_loop_create();

	//pass
	//ret = yh_test_event_loop_run();

	//pass
	//ret = yh_test_socket_create();

	switch (_create_op) {
		case TEST_FOR_SOCKET_SERVER_SYN:
			//pass
			ret = yh_test_socket_op_in_syn(TEST_SOCKET_SERVER);
			break;
		case TEST_FOR_SOCKET_CLIENT_SYN:
			//pass
			ret = yh_test_socket_op_in_syn(TEST_SOCKET_CLIENT);
			break;
		case TEST_FOR_BASE_LIB_LIST:
			//pass
			ret = yh_test_list_op();
			break;
        case TEST_FOR_BASE_LIB_QUEUE:
			//pass
            ret = yh_test_queue_op();
            break;
		case TEST_FOR_SOCKET_SERVER_ASYN:
			//pass
			//由于只是为了测试socket_device的用法，所以没有继续管理
			//监听的socket设备，因此这个socket设备会导致内存泄露，忽略！
			ret = yh_test_socket_op_in_asyn(TEST_SOCKET_SERVER);
			break;
		case TEST_FOR_SOCKET_CLIENT_ASYN:
			//pass
			ret = yh_test_socket_op_in_asyn(TEST_SOCKET_CLIENT);
			break;
		case TEST_FOR_TRANSFER_FILE_SERVER:
			ret = yh_test_upload_file();
			break;
		case TEST_FOR_TRANSFER_FILE_CLIENT:
			ret = yh_test_download_file();
			break;
		case TEST_FOR_FILE_DEVICE:
			//pass
			//但是效率比较低，比系统的文件copy要低
			ret = yh_test_fs_base_op();
			break;
		default:
			//YH_ASSERT(FALSE);
			break;
	}

#if defined(_DEBUG) && defined(WIN32)
     _CrtDumpMemoryLeaks();
#endif

	return ret;
}
