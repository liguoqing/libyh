/**
 * 测试event_loop
 *
 */


#ifndef _LIBYH_TEST_EVENT_LOOP_H
#define _LIBYH_TEST_EVENT_LOOP_H


#include "io/event_loop.h"


_int32 yh_test_event_loop_create();


_int32 yh_test_event_loop_run();


_int32 yh_test_event_loop_destroy();


/**
 * 用于处理停止Loop的消息函数
 * @param data 消息所携带的数据
 **/
_int32 yh_test_handle_stop_loop_msg(void *data);


#endif//_LIBYH_TEST_EVENT_LOOP_H
