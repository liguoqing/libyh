#ifndef _LIBYH_TEST_TCP_CONNECTION_H_
#define _LIBYH_TEST_TCP_CONNECTION_H_


#include "base/define.h"
#include "fs/fs_device.h"
#include "io/event_loop.h"
#include "net/tcp_connection.h"


#define CURRENT_DOWN_FILE_PROTO_VER 1
#define MAX_FILE_NAME_LEN           512
// 最大的p2p包大小为64K
#define MAX_P2P_PACKET_SIZE         64*1024
#define P2P_CMD_HEADER_SIZE			sizeof(P2pCmdHeader)
#define TEST_REQ_FILE_LEN_PER       32*1024


#define CMD_DOWN_FILE_REQ           1
#define CMD_DOWN_FILE_RESP          2
#define CMD_FILE_RANGE_REQ          3
#define CMD_FILE_RANGE_RESP         4


typedef struct SendDownFileReqFuncParam_s SendDownFileReqFuncParam;
typedef struct UpFileFuncParam_s UpFileFuncParam;
typedef struct TestFileTransferDownWork_s TestFileTransferDownWork;
typedef struct TestFileTransferUpWork_s TestFileTransferUpWork;
typedef struct TestFileDataManager_s TestFileDataManager;
typedef struct P2pCmdHeader_s P2pCmdHeader;
typedef struct Range_s Range;
typedef struct TestServerRes_s TestServerRes;
typedef struct TestFileTransferSession_s TestFileTransferSession;
typedef enum TestFileTransferSessionState_s TestFileTransferSessionState;
typedef struct DownFileReqCmd_s DownFileReqCmd;
typedef struct DownFileRespCmd_s DownFileRespCmd;
typedef struct FileRangeReqCmd_s FileRangeReqCmd;
typedef struct FileRangeRespCmd_s FileRangeRespCmd;


/**
 * @param buffer char **类型，写完之后会被修改
 * @param data 待写入buffer的数据的地址
 * @param size 数据的长度
 **/
#define YH_TEST_SET_DATA(buffer, data, size) \
	do { \
		memcpy(*buffer, data, size);\
		*buffer += size; \
	} while(0)


#define YH_TEST_GET_DATA(buffer, data, size) \
	do { \
		memcpy(data, *buffer, size); \
		*buffer += size; \
	} while(0)


#define YH_TEST_SESSION_ENTER_STATE(session, state) \
	do { \
		session->_state = state; \
	} while(0)


/**
 * 场景：请求服务器上的文件，作为客户端使用
 **/
_int32 yh_test_download_file();


/**
 * 场景：提供文件下载服务，作为服务端使用
 **/
_int32 yh_test_upload_file();


/**
 * 通常这种对象都是与具体的应用场景相关的
 * 发起下载文件的一端--客户端
 * file_name的内存只在这里维护一份
 **/
struct TestFileTransferDownWork_s {
    // 测试环境只有一条session
	struct EventLoop_s		*_reactor;
    TestFileTransferSession *_session;
	TestFileDataManager     *_data_mgr;
    YHString                _file_name;
    _uint64                 _file_size; // 放到数据管理比较合理
	BOOL					_down_finished;
};


/**
 * 接收下载请求，提供上传文件的一端--服务器端
 **/
struct TestFileTransferUpWork_s {
    TestFileTransferSession *_session;
    TestFileDataManager     *_data_mgr;
};


enum TestFileTransferSessionState_s {
	TSS_IDLE,
	TSS_OPENING,
	TSS_OPENED,
	TSS_REQUEST_SENDED,
	TSS_REQUEST_RESPONSED,
	TSS_RANGE_REQED,
	TSS_RANGE_RESPONSED,
	TSS_CLOSING,
	TSS_CLOSED
};


/**
 * 专门用于该场景的协议逻辑
 * @param _conn 一条session对应一条connection
 * @param _cmd_bufer 其中存放一个完整的命令，默认值64K，不够可以扩充
 * @param _cmd_buffer_offset 
 **/
struct TestFileTransferSession_s {
	struct TcpConnection_s		    *_conn;
	char						    *_cmd_buffer;
	_uint32						    _cmd_buffer_offset;
    TestServerRes                   *_ser_res;
	TestFileTransferSessionState	_state;
};


struct TestFileDataManager_s {
    FSDevice			*_file_device;
    _uint64				_cur_file_pos;  // 当前文件正在写的点
};


struct SendDownFileReqFuncParam_s {
	EventLoop			*_reactor;
    YHString			_file_name;
    _uint64				_file_size;
    TestServerRes		*_ser_res; 
};


struct UpFileFuncParam_s {
	EventLoop			*_reactor;
};


struct TestServerRes_s {
    struct sockaddr_in  _addr;
};


/**
 * 协议头部
 * NOTE 头部固定长度，一般不会扩充协议的头部，而且这3个字段很好的说明了
 * 这种2进制协议的内容，如果扩充，可以在具体的命令中扩充
 * _ver 协议版本号
 * _cmd_type 协议类型
 * _cmd_len 协议的长度，不包含头部长度
 **/
struct P2pCmdHeader_s {
    _uint32     _ver;
    _uint32     _cmd_type;
    _uint32     _cmd_len;
};


struct Range_s {
    _uint64     _pos;
    _uint32     _len;
};


/**
 * FIXME
 * 只是为了能验证代码，file_size默认在请求的时候就已知，这也不是没有道理
 * 至少迅雷就是这样的。
 **/
struct DownFileReqCmd_s {
    P2pCmdHeader    _header;
    YHString        _file_name;
    _uint64         _file_size;
};


struct DownFileRespCmd_s {
    P2pCmdHeader    _header;
    _uint8          _result;
};


struct FileRangeReqCmd_s {
    P2pCmdHeader    _header;
    Range           _file_range;
};


struct FileRangeRespCmd_s {
    P2pCmdHeader    _header;
    _uint8          _result;
    Range           _file_range;
    _uint32         _file_cont_len;
    void            *_file_cont;
};


#endif//_LIBYH_TEST_TCP_CONNECTION_H_
