/**
 * 用在引擎中的一些常用函数
 **/
#ifndef _LIBYH_UTILITY_H_
#define _LIBYH_UTILITY_H_


#include "base/define.h"
#include "base/thread.h"
#include "io/event_loop.h"


/**
 * yh_post_function将函数投递之后的回调
 * @param param 外部传递的参数，保证在回调后也能使用
 **/
typedef _int32 (* yh_post_function_handler) (void *);


/**
 * 向事件循环投递函数，使得函数的执行在事件循环线程中
 * @param reactor
 * @param param 所要执行的函数中携带的参数
 * @param func 将要执行的函数
 **/
_int32 yh_post_function(EventLoop *reactor, 
						void *param, 
						yh_post_function_handler func);


/**
 * 初始化reactor
 * @param reactor 代表主业务线程的反应器
 * @param dl_thread 代表主业务线程
 * @param dl_thread_handler 代表业务线程的入口函数
 **/
_int32 yh_init_reactor(EventLoop **reactor, 
                       yh_thread **dl_thread,
                       thread_start_func dl_thread_handler);


/**
 * 反初始化reactor
 * @param reactor 
 * @param dl_thread
 **/
_int32 yh_uninit_reactor(EventLoop *reactor, 
                         yh_thread *dl_thread);


/**
 * 默认的reactor执行函数
 * @param this
 * @param data 外部传递到ractor线程的数据
 **/
static void *_stdcall yh_default_reactor_run(yh_thread *this, void *data);


#endif//_LIBYH_UTILITY_H_