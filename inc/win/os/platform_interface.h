/**
 * 基础库 -- 各个平台相关的接口定义
 *
 */

#ifndef _LIBYH_PLATFORM_INTERFACE_H_
#define _LIBYH_PLATFORM_INTERFACE_H_


#include "base/define.h"


/**
 * 获取当前时间，有详细的时间结构体
 * 这是windows上的模拟版本
 */
_int32 yh_gettimeofday(struct timeval *tp, void *tzp);


/**
 * linux函数localtime_r的windows版本
 *
 */
_int32 yh_localtime_r(const long *timep, struct tm *result);


/**
 * 取当前线程的id
 * NOTE:
 * 有一个比较有意思的地方，我不知道哪里出问题了：
 * 这里直接使用_ulong而不是unsigned long竟然编译不过去。。。
 **/
_int32 yh_current_thread_id(unsigned long *id);


#endif //_LIBYH_PLATFORM_INTERFACE_H_