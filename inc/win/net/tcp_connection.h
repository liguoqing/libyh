#ifndef _LIBYH_TCP_CONNECTION_H_
#define _LIBYH_TCP_CONNECTION_H_


#include "base/define.h"
#include "net/tcp_device.h"


typedef _int32 (*yh_tcp_connection_open_callback)(void *, _int32 errcode);
typedef _int32 (*yh_tcp_connection_close_callback)(void *, _int32 errcode);
typedef _int32 (*yh_tcp_connection_send_callback)(void *, _int32 errcode);
typedef _int32 (*yh_tcp_connection_recv_callback)(void *, _int32 errcode);


typedef struct TcpConnection_s TcpConnection;
typedef enum TcpConnectionState_s TcpConnectionState;


/**
 * 构造TcpConnection
 * @param loop 
 * @param device 可以为NULL，如果不为NULL，connection中封装该device
 * @param user_data
 * @param conn 
 **/
_int32 yh_tcp_connection_create(struct EventLoop_s *loop, 
                                TcpDevice *device,
                                void *user_data,
								TcpConnection **conn);


_int32 yh_tcp_connection_destroy(TcpConnection *conn);


/**
 * 建立连接，通常都是发起connect操作
 **/
_int32 yh_tcp_connection_open(TcpConnection *conn,
                              const struct sockaddr_in *addr, 
							  _uint32 addr_len,
                              yh_tcp_connection_open_callback callback);


_int32 yh_tcp_connection_close(TcpConnection *conn,
                               yh_tcp_connection_close_callback callback);


_int32 yh_tcp_connection_send(TcpConnection *conn,
                              char *data, 
							  _uint32 data_len, 
                              yh_tcp_connection_send_callback callback);


/**
 * NOTE:
 * 如果期望接收的长度要大于conn的默认接收缓存，那么在内部自动增长接收！
 **/
_int32 yh_tcp_connection_recv(TcpConnection *conn,
							  _uint32 exp_recv_len,
                              yh_tcp_connection_recv_callback callback);


#define YH_TCP_CONN_ENTER_STATE(conn, state) \
	do { \
		conn->_state = state; \
	} while(0)


enum TcpConnectionState_s {
    TCS_IDLE,
    TCS_CONNECTING,
    TCS_CONNECTED_SUC,
    TCS_WORKING,                // 正在传输中
    TCS_CLOSING,
    TCS_CLOSED,
	TCS_FAILED
};


/**
 * @param _device 真正的发送设备
 * @param _send_buf 该缓存由外部分配和释放，本处只是指向，内部维护是不是
 *  更好？
 * @param _exp_send_len 准备要发送的数据长度，只有所有的数据发送完成后，
 *	或者发送失败才回调
 * @param _recv_buf 该缓存归Connection持有，内部使用，从系统缓冲区拷贝到
 *  此处，然后再交给上层处理，在io线程中处理
 * @param _recv_len 有默认值[那是否还有存在的必要？]
 * @param _exp_recv_len 期待接收的长度
 * @param _user_data 如果使用者需要携带点私货，这个是很有必要的
 * @param _state connection应该是有状态的
 **/
struct TcpConnection_s {
	TcpDevice				*_device;
    char                    *_send_buf;
    _uint32                 _exp_send_len;
    char                    *_recv_buf;
	_uint32					_exp_recv_len;
    void                    *_user_data;
    TcpConnectionState      _state;
};


#endif//_LIBYH_TCP_CONNECTION_H_
