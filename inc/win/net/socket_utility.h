/**
 * 网络库 -- 一些与socket有关的常用函数
 **/


#ifndef _LIBYH_SOCKET_UTILITY_H_
#define _LIBYH_SOCKET_UTILITY_H_


#include "base/define_data_type.h"


/**
 * 将ip从长整型转为点分字符串类型
 * @param lip
 * @param sip
 * @param size sip 的最大长度
 **/
_int32 yh_socket_ip_ulong2str(_ulong lip, char *sip, _uint32 size);


/**
 * 将ip从点分字符串类型转换为长整型
 * @param sip
 * @param lip
 **/
_int32 yh_socket_ip_str2ulong(const char *sip, _ulong *lip);


#endif//_LIBYH_SOCKET_UTILITY_H_
