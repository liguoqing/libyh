/**
 * 对各种windows socket api的封装
 **/


#ifndef _LIBYH_WINSOCK_H_
#define _LIBYH_WINSOCK_H_


#include "base/define.h"


typedef struct MSWSockApi_s MSWSockApi;


/**
 * 初始化windows socket的环境
 * 1、初始化windows库
 * 2、获得winsock api函数指针
 **/
_int32 yh_init_wsock_env();


/**
 * 
 **/
_int32 yh_unit_wsock_env();


/**
 * 取得wsock api的函数指针
 **/
_int32 yh_get_wsock_api_table(MSWSockApi *table);


struct MSWSockApi_s {
	LPFN_ACCEPTEX		_acceptex;
	LPFN_CONNECTEX		_connectex;
	LPFN_DISCONNECTEX	_disconnectex;
};


#endif//_LIBYH_WINSOCK_H_
