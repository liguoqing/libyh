/**
 * 网络库 -- 网络socket层接口
 * 2014/1/16：
 * 这几天对于socket的封装有个疑问，就是是否需要把socket单独作为一个模块
 * 封装下，因为device还会对socket上的接口重新封装，这样是不是有重复的嫌疑。
 * 主要问题还是没有弄清楚，上层怎么使用，什么地方会用！
 * 待定，后续想清楚再重构。
 * 2014/3/14:
 * 还是决定封装socket
 **/
#ifndef _LIBYH_SOCKET_INTERFACE_H_
#define _LIBYH_SOCKET_INTERFACE_H_


#include "base/define.h"


#if defined(LINUX)
    #define INVALID_SOCKET  (YHSocket)~0
#endif


struct EventLoop_s;


typedef enum SocketType_s SocketType;
typedef struct Socket_s Socket;


_int32 yh_socket_cleanup();


_int32 yh_socket_startup();


/**
 * 创建socket
 * @param type 创建的类型
 * @param sock 创建后返回的socket
 * @return 
 **/
_int32 yh_socket_create(SocketType type, Socket **sock);


/**
 * 根据存在的socket fd，创建socket
 * @param type 创建的类型
 * @param fd 已经存在的socket fd
 * @param sock 
 **/
_int32 yh_socket_create_with_fd(SocketType type, YHSocket fd, Socket **sock);


/**
 * 向指定的ip和端口发起连接
 * @param sock 准备发起连接的socket
 * @param ip 准备去连接的ip
 * @param port 端口
 **/
_int32 yh_socket_connect(Socket *sock, const char *ip, _uint16 port);


/**
 * 开始监听
 **/
_int32 yh_socket_listen(Socket *sock, const char *ip, _uint16 port);


/**
 * 用于接收新的socket，同步的
 * @param sock 监听的socket
 * @param sock_new 接收到的socket
 **/
_int32 yh_socket_accept(Socket *sock, 
						Socket **sock_new,
						struct sockaddr_in *sock_new_addr);


/**
 * FIXME
 * 这个函数完成了两个功能，不好，拆分之
 * 关闭并销毁socket，如果返回成功，则sock就不能再次使用
 **/
_int32 yh_socket_close(Socket *sock);


_int32 yh_socket_send(Socket *sock, const char *data, _uint32 len);


_int32 yh_socket_recv(Socket *sock, char *buffer, _uint32 len, _int32 *errcode);


enum SocketType_s {YH_TCP, YH_UDP};


/**
 * 如果_poll是空的，则此处的使用是同步的，否则就是异步的
 * @param _poll 标识该socket上的操作投递到哪个selector上的
 **/
struct Socket_s {
    struct EventLoop_s  *_poll;        
    YHSocket		    _fd;           // socket物理句柄
    SocketType          _type;         // socket支持的协议类型
};


#endif//_LIBYH_SOCKET_INTERFACE_H_
