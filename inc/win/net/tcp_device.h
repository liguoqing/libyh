/**
 * tcp socket的封装
 **/
#ifndef _LIBYH_TCP_DEVICE_H_
#define _LIBYH_TCP_DEVICE_H_


#include "base/define.h"
#include "base/queue.h"
#include "net/socket_interface.h"
#include "net/winsock.h"
#include "io/io_request.h"
#include "io/io_device.h"


typedef struct TcpDevice_t TcpDevice;
typedef enum TcpDeviceState_t TcpDeviceState;


/**
 * TcpDevice是异步的，当上层向该device投递一个操作时，需要知道操作的结果
 * 该处定义的就是外部所实现的回调原型
 **/
typedef _int32 (*yh_tcp_device_connect_callback)(SockConnReq *req);
typedef _int32 (*yh_tcp_device_accept_callback)(SockAcceReq *req);
typedef _int32 (*yh_tcp_device_send_callback)(SockSendReq *req);
typedef _int32 (*yh_tcp_device_recv_callback)(SockRecvReq *req);
typedef _int32 (*yh_tcp_device_close_callback)(SockClosReq *req);


/**
 * 取tcp_device的socket fd
 **/
#define TCP_DEVICE_SOCK_FD(device) device->_sock->_fd


/**
 * 创建一个TCP类型的设备
 * @param loop 
 * @param fd 为了有些时候需要外部首先创建socket，而后再次创建device。
 *	如果，fd为INVALID_SOCKET，则内部创建，否则直接使用这个socket fd创建。
 * @param device
 **/
_int32 yh_tcp_device_create(EventLoop *loop, 
							YHSocket fd, 
							TcpDevice **device);


_int32 yh_tcp_device_destroy(TcpDevice *device);


/**
 * 重新将某个socket关联到device上，主要是由于device上的socket出错，
 * 需要重连，此处的socket是重新建立的，这个API的作用有几何，未知。
 * 主要是，我在尝试重连的时候，发现socket经常会出现iocp:52错误，故此
 * 尝试重新建立一个socket，发现还是错误，该问题暂且记下，后续弄清。
 * @param device
 * @param sock
 **/
_int32 yh_tcp_device_reset_socket(TcpDevice *device);


/**
 * 向远端地址发起连接
 * 在使用过程中，发现有些时候需要携带自定义的数据，增加user_data
 * @param device
 * @param addr
 * @param addr_len
 * @param user_data 使用者需要在回调之后用到的数据，生命周期归使用者管理
 * @param callback
 **/
_int32 yh_tcp_device_connect(TcpDevice *device, 
							 const struct sockaddr_in *addr, 
                             _uint32 addr_len, 
							 void *user_data,
							 yh_tcp_device_connect_callback callback);


/**
 * 当第一次连接失败时发起重连，不同点在于本地端口会更新
 * @param device 发起重连的设备
 * @param req 上一次使用的请求，这里务必注意其生命周期
 **/
_int32 yh_tcp_device_reconnect(TcpDevice *device, SockConnReq *req);


/**
 * 这个接口是同步的
 **/
_int32 yh_tcp_device_listen(TcpDevice *device, 
							const struct sockaddr *addr,
							_uint32 addr_len, 
							_int32 backlog, 
							_int32 *err_code);


_int32 yh_tcp_device_accept(TcpDevice *device, 
							yh_tcp_device_accept_callback callback);


/**
 * 当发送完成出现错误的时候，会返回YH_ERR_SOCKET_OP_FAILURE错误码
 * 具体的错误码以及错误处理可以在callback中得知并完成。
 * 上层调用获得返回错误时，可以直接返回，等待回调中再继续处理错误。
 **/
_int32 yh_tcp_device_send(TcpDevice *device, 
						  char *data,
						  _uint32 len,
						  void *user_data,
                          yh_tcp_device_send_callback callback);


/**
 * NOTE
 * 只有当收到的数据长度为0或者为device->_expect_recv_len才会callback
 **/
_int32 yh_tcp_device_recv(TcpDevice *device, 
						  char *buffer,
						  _uint32 exp_recv_len,
						  void *user_data,
                          yh_tcp_device_recv_callback callback);


/**
 * 关闭设备，确保该设备上的所有的请求都已经完成，或者已经取消
 * 即经过close之后，设备即为无效的状态，
 * 所有回调到设备上的消息都不能再次处理
 **/
_int32 yh_tcp_device_close(TcpDevice *device, 
                           void *user_data,
						   yh_tcp_device_close_callback callback);


/**
 * TDS_IDLE: 初始化之后的状态，无任何操作
 * TDS_CONNECTING: 已经发起connect的状态
 * TDS_CONNECTED: 已经连上的状态，即connect已经得到回调
 * TDS_CONNECTED_FAIL: 连接失败
 * TDS_LISTENNING: 发起listen操作
 * TDS_ACCEPTED: 发起了accpet操作
 * TDS_TRANSMITED: 可以进行传输的状态
 * TDS_CLOSING: 已经发起关闭但还未完全关闭
 * TDS_CLOSED: 设备完全关闭
 **/
enum TcpDeviceState_t {
	TDS_IDLE, 
	TDS_CONNECTING, 
	TDS_CONNECT_ERR,
    TDS_CONNECTED, 
	TDS_LISTENING, 
	TDS_ACCEPTED, 
	TDS_TRANSMITED, 
	TDS_FAILED,
	TDS_HALF_CLOSED,
	TDS_CLOSING,
    TDS_CLOSED
};


/**
 * tcp device是socket的一层薄封装，所以接口跟socket很类似，为什么会需要？
 * 是因为，我需要将socket和file都等效成一类的device，io_device。至于有
 * 什么其他的优势，需要等后面再细细体会。
 **/
struct TcpDevice_t {
    IODevice            *_device;
    Socket				*_sock;
    char                *_recv_buf;
	_uint32				_expect_recv_len;	// 期望接收的长度
    char                *_send_buf;
	_uint32				_expect_send_len;	// 期望发送的长度
    Queue               *_pending_reqs;     //该设备上尚未完成的请求
    HANDLE              _wait_close;        //等待设备上所有请求完成的事件
    TcpDeviceState		_state;             //设备当前所处的状态    
};

#endif//_LIBYH_TCP_DEVICE_H_
