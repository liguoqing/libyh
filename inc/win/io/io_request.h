/**
 * 定义向io设备上投递的io请求
 * TODO
 * 1, 用宏来简化这里的申请和释放
 **/


#ifndef _LIBYH_IO_REQUEST_H_
#define _LIBYH_IO_REQUEST_H_


#include "base/define.h"
#include "base/message.h"
#include "io/io_device.h"
#include "io/iocp.h"


typedef struct UserDefinedData_s UserDefinedData;
typedef struct IORequest_s  IORequest;
typedef struct IOSockConnReq_s SockConnReq;
typedef struct IOSockAcceReq_s SockAcceReq;
typedef struct IOSockSendReq_s SockSendReq;
typedef struct IOSockRecvReq_s SockRecvReq;
typedef struct IOSockClosReq_s SockClosReq;
typedef struct IOSockDiscReq_s SockDiscReq;
typedef struct IOFsOpenReq_s FsOpenReq;
typedef struct IOFsCloseReq_s FsCloseReq;
typedef struct IOFsReadReq_s FsReadReq;
typedef struct IOFsWriteReq_s FsWriteReq;


/**
 * 构造socket connect op的请求结构体
 * @param op [out]代表请求的异步io，向iocp投递的时候使用
 * @param handler
 * @param device 意味着该请求是发到哪个设备上的
 * @param callback 请求完成后发起请求模块中对应的回调
 * @param addr 准备连接的地址
 * @param addr_len
 * @param req [out]构造后的输出
 **/
_int32 yh_create_sock_conn_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
                               const struct sockaddr_in *addr,
                               _uint32 addr_len,
							   void *user_data,
							   SockConnReq **req);


_int32 yh_destroy_sock_conn_req(SockConnReq *req);


_int32 yh_create_sock_acce_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
							   SockAcceReq **req);


_int32 yh_destroy_sock_acce_req(SockAcceReq *req);


_int32 yh_create_sock_send_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
							   void *user_data,
							   SockSendReq **req);


_int32 yh_destroy_sock_send_req(SockSendReq *req);


_int32 yh_create_sock_recv_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
							   void *user_data,
							   SockRecvReq **req);


_int32 yh_destroy_sock_recv_req(SockRecvReq *req);


_int32 yh_create_sock_clos_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
                               void *user_data,
							   SockClosReq **req);


_int32 yh_destroy_sock_clos_req(SockClosReq *req);


_int32 yh_create_sock_disc_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
							   SockDiscReq **req);


/**
 * @param handler 这个可以为空，因为现在的fs open是同步的
 **/
_int32 yh_create_fs_open_req(OverlappedWrapper **op, 
                             yh_message_handler handler,
                             IODevice *device,
                             void *callback,
                             void *user_data,
                             FsOpenReq **req);


_int32 yh_destroy_fs_open_req(FsOpenReq *req);


_int32 yh_create_fs_close_req(OverlappedWrapper **op, 
                              yh_message_handler handler,
                              IODevice *device,
                              void *callback,
							  void *user_data,
                              FsCloseReq **req);


_int32 yh_destroy_fs_close_req(FsCloseReq *req);


_int32 yh_create_fs_read_req(OverlappedWrapper **op, 
                             yh_message_handler handler,
                             IODevice *device,
                             _uint64 start_pos,
                             void *callback,
                             void *user_data,
                             FsReadReq **req);


_int32 yh_destroy_fs_read_req(FsReadReq *req);


_int32 yh_create_fs_write_req(OverlappedWrapper **op, 
                              yh_message_handler handler,
                              IODevice *device,
                              _uint64 start_pos,
                              void *callback,
                              void *user_data,
                              FsWriteReq **req);


_int32 yh_destroy_fs_write_req(FsWriteReq *req);


/**
 * FIXME
 * 这里用宏来定义确实不错，后续优化，可以少写点东西
 * @param _op 该request所要执行的操作
 * @param _device 该request所要发往的设备
 * @param _callback [in]该request完成后的回调
 * @param _err_code [out]出现的错误码
 * @param _user_data [in]请求自定义的数据
 **/
struct IORequest_s {
	OverlappedWrapper			*_op;     
	IODevice                    *_device; 
	void						*_callback;
	_int32						_err_code;
	void						*_user_data;
};


struct IOSockConnReq_s {
	IORequest						*_base;	
    struct sockaddr_in              _conn_addr; // 准备连接的远端地址
};


struct IOSockAcceReq_s {
	IORequest		*_base;
	YHSocket		_acc_sock_fd;
	void			*_buffer;
};


struct IOSockSendReq_s {
	IORequest		*_base;
};


struct IOSockRecvReq_s {
	IORequest		*_base;
	_uint32			_recvd_bytes;
};


struct IOSockClosReq_s {
	IORequest		*_base;

};


struct IOSockDiscReq_s {
	IORequest		*_base;

};


struct IOFsOpenReq_s {
    IORequest       *_base;
};


struct IOFsCloseReq_s {
    IORequest       *_base;
};


/**
 * @param _read_len 在回调时返回的真正读到的字节数，传出的参数
 * 之所以加这一个参数是为了上层使用上的方便，同时避免外部使用者得知内部
 * 通过什么手段来返回的真正的读取字节数，tcp_device的设计亦是如此
 **/
struct IOFsReadReq_s {
    IORequest       *_base;
    _uint64         _start_pos;
    _uint32         _read_len;
};


struct IOFsWriteReq_s {
    IORequest       *_base;
    _uint64         _start_pos;
    _uint32         _write_len;
};


/**
 * 异步场景下很多时候都会传递一个用户自定义的数据，但很多时候都不清楚
 * 这个user_data到底是个什么结构，所以导致异步传递的其他数据很不容易理解
 * 所以，我这里将user_data封装了一个结构，这样可以更容易的理解这个结构。
 * @param _desc 如果你愿意，可以在这里加上一个对data的注释
 * @param _data 携带的数据
 * @param _handler[option] 这里是否可以加上一个数据的解码函数或者处理器
 **/
struct UserDefinedData_s {
	char			*_desc;
	void			*_data;
};


#endif//_LIBYH_IO_REQUEST_H_
