/**
 * 基础库 -- 事件循环
 *
 */

#ifndef _LIBYH_EVENT_LOOPER_H_
#define _LIBYH_EVENT_LOOPER_H_


#include "base/define.h"
#include "base/message_queue.h"
#include "io/iocp.h"


typedef enum EventLoopState_s   EventLoopState;
typedef struct EventLoop_s EventLoop;


/**
 * 创建一个loop
 * @param loop 返回值，表征用于创建的loop
 */
_int32 yh_loop_create(EventLoop **loop);


/**
 * 销毁当前的loop
 * 这个接口不应该由外部调用，应该设置为内部的
 * 这个loop应该交给外部释放 20140304
 * @param loop 待销毁的loop，执行后该loop不能再次使用，
 *	否则会导致未定义行为
 */
_int32 yh_loop_destroy(EventLoop *loop);


/**
 * 开始运行loop
 * @param loop 开始执行的loop
 */
_int32 yh_loop_run(EventLoop *loop);


/**
 * 监测当前loop是否正处于running状态
 * @param loop
 * @param running 是否正处于running状态，TRUE 正处于，FALSE 非
 * @remark loop的状态只会处于一个线程读一个线程写的状态，可以认为是线程安全的
 **/
_int32 yh_loop_is_running(EventLoop *loop, BOOL *running);


/**
 * 由于直接投递到iocp中可能会出现错误，使得iocp接收不到该异步操作的完成
 * 因此，这里做一个模拟
 **/
_int32 yh_simulate_handle_async_op(EventLoop *loop,
								   OverlappedWrapper *op_wrapper);


/**
 * 事件循环的状态机
 * IDLE:        刚初始化完成的状态
 * RUNNING:     正在运行的状态
 * STOPPED:     已经停止的状态
 **/
enum EventLoopState_s {IDLE, RUNNING, STOPPED};


struct EventLoop_s {
    // NOTE:
    // [1] loop中为何要知道实现中是使用了iocp呢？
    // 后续实现要抽象这里的poll机制
    // [2] 有了state之后，stop_flag就不再需要了，
    // 后续要去掉stop_flag
    HANDLE					_iocp;
	MessageQueue			*_queue;
    _int32                  _stop_flag;
    EventLoopState          _state;
};


#endif//_LIBYH_EVENT_LOOPER_H_