/**
 * 引擎库 -- 对io设备的封装
 * io设备包括：网络socket、文件fd、管道等
 **/


#ifndef _LIBYH_IO_DEVICE_H_
#define _LIBYH_IO_DEVICE_H_


#include "base/define.h"
#include "io/event_loop.h"


typedef enum IODeviceType_t IODeviceType;
typedef struct IODevice_t IODevice;


/**
 * 所有IO设备的基类
 * @param loop 
 * @param fd
 * @param device 
 **/
_int32 yh_device_create(EventLoop *loop, 
						YHHandle fd, 
						IODeviceType type, 
						IODevice **device);


/**
 * 重置该device上的fd
 * @param device
 * @param fd
 **/
_int32 yh_device_reset_fd(IODevice *device, YHHandle fd);


/**
 * 销毁一个设备
 **/
_int32 yh_device_destroy(IODevice *device);


/**
 * 将io设备加入event loop中监视起来，即将其与io多路选择器关联
 * @param loop 
 * @param device
 **/
_int32 yh_device_add_to_loop(EventLoop *loop, IODevice *device);


/**
 * 将io设备从loop中移除，即不再关注该设备上的io事件并将其派发
 **/
_int32 yh_device_remove_from_loop(EventLoop *loop, IODevice *device);


enum IODeviceType_t { FS_DEVICE, TCP_DEVICE, UDP_DEVICE };


struct IODevice_t {
    EventLoop       *loop;  // 设备所绑定到的loop
    IODeviceType    _type;  // 设备的类型
	YHHandle		_fd;	// 每个设备都应该有一个fd
};


#endif//_LIBYH_IO_DEVICE_H_
