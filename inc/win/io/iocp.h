#ifndef _LIBYH_IOCP_H_
#define _LIBYH_IOCP_H_


#if defined(WIN32)
#include "base/define.h"
#include "base/message.h"


typedef struct OverlappedWrapper_s OverlappedWrapper;


/**
 * 创建一个新的IOCP
 * @param iocp 创建成功后返回的完成端口句柄，如果创建失败，则为空
 * @param c_key iocp的名称
 */
_int32 yh_iocp_create(YHHandle *iocp, ULONG_PTR c_key);


_int32 yh_iocp_destroy(YHHandle iocp);


/**
 * 将一个句柄关联到当前iocp上
 * @param 
 */
_int32 yh_iocp_associate_fd(YHHandle iocp, YHHandle fd, ULONG_PTR c_key);


/**
 * [TODO]
 * 增加错误码返回
 * 从IOCP的完成队列中取一个io完成通知
 * @param iocp
 * @param num_of_bytes_ptr
 * @param completion_key
 * @param overlapped_ptr
 * @param millisecond
 */
_int32 yh_get_queued_completion_status(YHHandle iocp, 
                                       LPDWORD num_of_bytes_ptr, 
                                       PULONG_PTR c_key,
                                       LPOVERLAPPED *overlapped_ptr,
                                       DWORD millisecond);


/**
 * 向IOCP中投递一个完成通知
 * @param iocp 
 * @param num_of_bytes 
 * @param completion_key 
 * @param overlapped_ptr
 */
_int32 yh_post_queued_completion_status(YHHandle iocp,
                                        DWORD num_of_bytes,
                                        ULONG_PTR c_key,
                                        LPOVERLAPPED overlapped_ptr);


/**
 * @param o_wrapper 返回值
 * @param type 创建该结构的用途或者类型
 * @param handler 当该op发生时的回调处理，允许为空，
 *	handler的参数是OverlappedWrapper
 * @param d_handler 释放时data的回收函数
 * @param data 当op触发时携带的数据，其生命周期的管理非常关键
 *		对于data的生命周期，有这样的考虑：
 *		1，不能全部使用默认的释放函数，显而易见，单纯的free不能释放所有
 *		类型的结构体；
 *		2，生命周期不一定跟op是一致的，比如在tcp_device中的req，其本质上
 *		是一个op，但是为了能在回调时得知具体的req，所以在其内部变量op中
 *		也需要携带req（即op的data成员），这样如果op结束，data立即销毁，
 *		那会导致req本身失效，这样回调时的处理就不正常。所以，两者的生命
 *		周期不一定都是一致的，取决于是否给op设置析构函数，注意：如果不
 *		需要op来管理data，必须将d_handler设置为NULL！如果遗忘，这种bug
 *		还是比较难查的！
 */
_int32 yh_create_overlapped_wrapper(OverlappedWrapper **o_wrapper, 
									MessageType type,
									yh_message_handler handler,
									yh_message_data_delete_handler d_handler,
									void *data);


_int32 yh_destroy_overlapped_wrapper(OverlappedWrapper *o_wrapper);


/**
 * 将OVERLAPPED结构体转换为一个内部使用的OverlappedWrapper的操作
 * @param overlaped 
 * @param o_wrapper 返回的OverlappedWrapper结构
 */
_int32 yh_overlaped_to_overlapped_wrapper(OVERLAPPED *overlaped, 
										  OverlappedWrapper **o_wrapper);


/**
 * @param _overlapped 封装的重叠结构，IOCP就是要这个东西
 * @param _trans_bytes 异步IO传输的数据，可能是网络、文件等
 * @param _errcode 操作中出现的错误 GetLastError()获得
 * @param _msg 自定义的数据
 **/
struct OverlappedWrapper_s {
    OVERLAPPED      _overlapped;
	DWORD           _trans_bytes;
	int				_err_code;
	Message         *_msg;
};


#endif//#if defined(WIN32)

#endif//_LIBYH_IOCP_H_
