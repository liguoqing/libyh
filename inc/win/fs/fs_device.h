#ifndef _LIBYH_FS_DEVICE_H_
#define _LIBYH_FS_DEVICE_H_


#include "base/define.h"
#include "base/queue.h"
#include "io/io_device.h"
#include "io/io_request.h"


#define FS_DEVICE_ENTER_STATE(device, state)	device->_state = state


typedef struct FSDevice_s FSDevice;
typedef enum FSDeviceCreateFlag_s FSDeviceCreateFlag;
typedef enum FSDeviceState_t FSDeviceState;


typedef _int32 (*yh_fs_device_open_callback) (FsOpenReq *req);
typedef _int32 (*yh_fs_device_read_callback) (FsReadReq *req);
typedef _int32 (*yh_fs_device_write_callback) (FsWriteReq *req);
typedef _int32 (*yh_fs_device_close_callback) (FsCloseReq *req);


/**
 * 创建文件设备
 * @param loop 
 * @param user_data 使用文件设备的过程中携带的数据，在回调的时候可能使用
 * @param device
 **/
_int32 yh_fs_device_create(EventLoop *loop, 
						   void *user_data, 
						   FSDevice **device);


_int32 yh_fs_device_destroy(FSDevice *device);


/**
 * 打开文件设备
 * NOTE file_path + file_name 可以直接构成一个完整的文件位置
 * @param device 准备打开的文件设备
 * @param file_path 最后要包含"//"
 * @param file_name 最后要包含文件名后缀
 * @o_flag 打开文件的方式
 **/
_int32 yh_fs_device_open(FSDevice *device, 
                         FSDeviceCreateFlag flag,
						 YHString file_path, 
						 YHString file_name,
                         void *user_data,
                         yh_fs_device_open_callback handler);


/**
 * 文件读取
 * @param device
 * @param start_pos 文件读取的起始点
 * @param exp_read_len 期望读取的长度
 * @param buffer 读取之后存放的位置
 * @param buf_len buffer的大小
 * @param handler 读完之后的回调
 **/
_int32 yh_fs_device_read(FSDevice *device, 
                         _uint64 start_pos, 
                         _uint32 exp_read_len,
						 char *buffer, 
                         _uint32 buf_len, 
                         void *user_data,
                         yh_fs_device_read_callback handler);


/**
 * 写文件
 * @param device
 * @param start_pos 准备写入的文件起始点
 * @param exp_write_len 内容的长度
 * @param data 准备写入文件的内容
 * @param handler 写完之后的回调
 **/
_int32 yh_fs_device_write(FSDevice *device, 
                          _uint64 start_pos, 
                          _uint32 exp_write_len,
						  char *data, 
                          void *user_data,
                          yh_fs_device_write_callback handler);


_int32 yh_fs_device_close(FSDevice *device,
                          void *user_data,
                          yh_fs_device_close_callback handler);


/**
 * FDS_IDLE: 初始化之后的状态，无任何操作
 * FDS_WORKING: 可读写状态
 * FDS_CLOSING: 已经发起关闭但还未完全关闭
 * FDS_CLOSED: 设备完全关闭
 * FDS_FAILED: 错误状态，可能读、写、打开操作错误
 **/
enum FSDeviceState_t {
	FDS_IDLE,
	FDS_WORKING,
	FDS_CLOSING,
    FDS_CLOSED,
    FDS_FAILED
};


struct FSDevice_s {
	IODevice		*_device;
	_uint64			_file_size;
	char			*_read_buf;
	_uint32			_exp_read_len;
	char			*_write_buf;
	_uint32			_exp_write_len;
	Queue           *_pending_reqs;     //该设备上尚未完成的请求
	HANDLE          _wait_close;        //等待设备上所有请求完成的事件
    FSDeviceState   _state;
	void			*_user_data;		
};


/**
 * FO_READ "r" 文件必须存在，只读
 * FO_WRITE "w" 文件不存在则创建，存在则清空，可读写
 * FO_APPEND "a" 文件不存在则创建，存在则附加，可读写
 * FO_READ_U "r+" 文件必须存在，可读可写
 * FO_WRITE_U "w+" 文件不存在则创建，存在则清空，可读可写
 * FO_APPEND_U "a+" 文件不存在则创建，存在则附加，可读可写
 **/
enum FSDeviceCreateFlag_s {
    FO_READ,        //
    FO_WRITE,       //w 
    FO_APPEND,      //a 
    FO_READ_U,      //r+
    FO_WRITE_U,     //w+
    FO_APPEND_U,    //a+
};


#endif//_LIBYH_FS_DEVICE_H_
