/**
 * 工具类 -- 常用的小工具函数
 **/


#ifndef _LIBYH_TOOLS_H_
#define _LIBYH_TOOLS_H_


#include "base/define_data_type.h"


_int32 yh_memcpy(char *src, const char *des, _uint32 size);


#endif//_LIBYH_TOOLS_H_
