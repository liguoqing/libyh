/**
 * 基础库 --
 *
 */

#ifndef _LIBYH_MESSAGE_QUEUE_H_
#define _LIBYH_MESSAGE_QUEUE_H_


#include "define.h"
#include "message.h"


typedef struct MessageQueue_s MessageQueue;
typedef struct MessageQueueNode_s MessageQueueNode;


/**
 * 初始化消息队列
 * 队列是FIFO的，因为这里的消息必然就应该是先到先处理。为了这个目的，
 * 不再提供插入这种形式的接口，如果是一个通用队列可以，但这里特指消息队列。
 * 所以，这里的put就是在头部插入，pop就是在尾部弹出，其实这里是一个栈的结构。
 * @param mq 传出参数，初始化完成的消息队列
 */
_int32 yh_mq_init(MessageQueue **mq);


/**
 * 向消息队列中加入一个节点，在头部
 * @param mq 指定的消息队列
 * @param msg 待插入的消息
 */
_int32 yh_mq_put(MessageQueue *mq, Message *msg);


/**
 * 从消息队列中弹出一个节点，在尾部
 * @param mq 指定的消息队列
 * @param msg 弹出的消息内容
 */
_int32 yh_mq_pop(MessageQueue *mq, Message **msg);


/**
 * 判断消息队列是否为空
 * @param mq 
 * @param empty 标识队列是否为空，TRUE则为空
 */
_int32 yh_mq_empty(MessageQueue *mq, BOOL *empty);


/**
 * 获取消息队列的大小
 * @param mq
 * @param size 队列的大小
 */
_int32 yh_mq_size(MessageQueue *mq, _uint32 *size);


/**
 * 反初始化消息队列
 *
 */
_int32 yh_mq_uninit(MessageQueue *mq);


_int32 yh_mq_node_alloc(MessageQueueNode **node);


_int32 yh_mq_node_delete(MessageQueueNode *node);


struct MessageQueueNode_s {
	Message				*_msg;
	MessageQueueNode	*_next, *_prev;
};


struct MessageQueue_s {
	MessageQueueNode	*_tail, *_head;
	_uint32				_size;
};


#endif//_LIBYH_MESSAGE_QUEUE_H_
