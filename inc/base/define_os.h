/*
 * 基础库 -- 特定os平台的宏
 */

#ifndef _LIBYH_DEFINE_OS_H_
#define _LIBYH_DEFINE_OS_H_

#if defined(WIN32)
    #include <WinSock2.h>
	#include <MSWSock.h>
    #include <Windows.h>
    #include <WinBase.h>
    #include <process.h>
#endif

#include "os/platform_interface.h"

#endif //_LIBYH_DEFINE_OS_H_