/**
 * 基础库 -- list
 * MEMO:
 * 1, List 中加入对data的释放不太合理，为什么？
 * 比如，一个数据被多次放入到list中，当前的list是允许的，那么如果list自身
 * 做这种类型的释放时就会导致重复内存的释放。所以，除非不允许加入相同的
 * 数据，否则data的管理就得交给外部。相比较不允许放同一份内存的限制，
 * 我觉得data交给外部来管理更加靠谱，list自身不应该关注数据是什么，因此
 * 也就不应该关心这块内存[data]是否相同。2014.1.20 23:21
 **/


#ifndef _LIBYH_LIST_H_
#define _LIBYH_LIST_H_


#include "base/define_data_type.h"


/**
 * 用于对比两个data是否相同
 * @param data_ori 原始数据
 * @param data_cmp 用于对比的数据
 * @return 0 标识两者相等，<0 则 ori < cmp，>0 则 ori > cmp
 **/
typedef _int32 (*yh_list_data_cmp_func)(void *data_ori, void *data_cmp);


/**
 * 对data进行释放
 **/
typedef _int32 (*yh_list_data_free_handler)(void *data);


typedef struct ListNode_s ListNode;
typedef struct List_s List;


_int32 yh_list_create(List **list);


_int32 yh_list_destroy(List *list);


/**
 * 向list的after之后插入一个节点
 * @param list 
 * @param data 节点的数据
 * @param after 新建的节点将插入到该节点之后
 *  1, 当after为NULL的时候，直接放在list的尾部
 *	2, data的所有权归外部持有，node只是复制指针地址，
 *  当free_handler不为NULL的时候，list负责释放内存，
 *  否则内存由外部负责释放
 **/
_int32 yh_list_insert(List *list, void *data, ListNode *after);


/**
 * 返回list的尾部节点
 **/
_int32 yh_list_tail(List *list, ListNode **tail);


/**
 * 从list中查找与data相等的第一个元素
 **/
_int32 yh_list_node_find(List *list, void *data, yh_list_data_cmp_func cmp, 
                         ListNode **match_node);


/**
 * 从list中删除所有为data的元素
 * @deprecated:
 *  list既然不再关心数据时什么，为何这里还能进行比对？感觉不太好
 *
 * @refactor 针对节点进行删除
 * @param list
 * @param node 准备移除的节点
 **/
_int32 yh_list_node_delete(List *list, ListNode *node);


#if defined(YH_DEBUG_ENABLE)
typedef void (*yh_list_print_func)(void *data);	
_int32 yh_list_print_data(List *list, yh_list_print_func print);
#endif


/**
 * NOTE
 * 由于_data只能是外部定义的结构，在list内部是不知道其释放方式的，
 * 所以只能由外部将释放函数传进来，
 * 如果这是一个嵌入式链表就可以不用这么复杂了
 **/
struct ListNode_s {
    void                        *_data;
    ListNode                    *_next, *_prev;
};


/**
 * NOTE
 * 这样设计的确会有一个问题就是转了一圈却不知道list是否为空了
 * 只能用size来标识，这样可能会感觉不怎么优雅？TODO
 * 或者可以增加两个节点，一个_head，一个_tail，当两者相等的时候就认为空
 **/
struct List_s {
    ListNode    *_head;
    _uint32      _size;
};


#endif//_LIBYH_LIST_H_
