/*
 * 基础库 -- 线程封装
 */

#ifndef _LIBYH_THREAD_H_
#define _LIBYH_THREAD_H_

#include "define.h"


struct yh_thread {
    // NOTE:
    // 这里的_tid的类型有点混淆，请明确下
#if defined(WIN32)
	HANDLE		_tid;
#else
	_int32		_tid;
#endif
	void		*_data;             // 外部传入的线程数据，线程结构本身并不持有
	void		*_thread_proc;
	_int32		_exit_value;
};
typedef struct yh_thread yh_thread;


typedef void *(__stdcall *thread_start_func)(yh_thread *, void *);


/**
 * 创建线程
 * @param new_thread 创建后的线程返回值
 * @param func 线程的执行函数
 * @param data 传给线程执行函数的数据内容
 */
_int32 yh_thread_create(yh_thread **new_thread, thread_start_func func, 
						void *data);


/**
 * 释放线程所占用的空间
 * @param new_thread
 */
_int32 yh_thread_destroy(yh_thread *new_thread);


/**
 * 获得当前线程的ID
 * @param thd 当前线程体
 * 
 */
_int32 yh_get_current_thread_id(_ulong *tid);


#if defined(WIN32)
typedef CRITICAL_SECTION	yh_mutex_s;
#elif 
#endif


typedef yh_mutex_s yh_mutex;


/**
 * 初始化mutex
 * @param mutex 
 */
_int32 yh_mutex_init(yh_mutex *mutex);


_int32 yh_mutex_destroy(yh_mutex *mutex);


_int32 yh_mutex_lock(yh_mutex *mutex);


_int32 yh_mutex_unlock(yh_mutex *mutex);


#endif // _LIBYH_THREAD_H_
