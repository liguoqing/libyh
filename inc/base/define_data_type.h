/*
 * 基础库 -- 私有文件 -- 数据结构的通用定义
 */

#ifndef _LIBYH_DEFIINE_DATA_TYPE_H_
#define _LIBYH_DEFIINE_DATA_TYPE_H_

/**
 *	通用数据类型定义
 **/
#define TRUE					1
#define FALSE					0

#define _int8					char
#define _uint8					unsigned char

#define _int16                  short
#define _uint16                 unsigned short

#define _int32					int						
#define _uint32					unsigned int

#define _int64					long long int
#define _uint64					unsigned long long int

#define _long                   long
#define _ulong                  unsigned long

typedef struct YHString_s       YHString;


/**
 * 特定操作系统的数据类型
 **/
#if defined(WIN32)
	#define YHHandle			HANDLE
	#define YHSocket			SOCKET
#elif defined(LINUX)
	#define YHHandle			_int32
	#define YHSocket			socket
#endif


struct YHString_s {
    _uint32     _len;
    char        *_str;
};


#endif // _LIBYH_DEFIINE_DATA_TYPE_H_