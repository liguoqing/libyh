/**
 * 基础库 -- 信号系统
 *
 */

#ifndef _LIBYH_SINGNAL_H_
#define _LIBYH_SINGNAL_H_


#include "base/define_os.h"
#include "base/define_data_type.h"


_int32 yh_create_event(YHHandle *ev);


_int32 yh_destroy_event(YHHandle ev);


/**
 * 等待某个信号的发生
 * @param p 等待发生的信号，如果*p为无效句柄则创建之，反之直接在上面等待
 **/
_int32 yh_wait(YHHandle p);


/**
 * 激活某个信号
 * @param p 激活的信号
 **/
_int32 yh_signal(YHHandle p);


#endif//_LIBYH_SINGNAL_H_
