/*
 * 基础库 -- 一些常用的工具宏
 */

#ifndef _LIBYH_DEFINE_UTILITY_H_
#define _LIBYH_DEFINE_UTILITY_H_

#include <assert.h>
#include "define_error.h"


/**
 * 确保返回值一定是正确的，否则输出日志到控制台
 **/
#define YH_CHECK_VALUE(code)												\
	do {																	\
		if (code != YH_SUCCESS) {											\
			LOG("assert error:%d in file:%s, func:%s-%d.\n",				\
				code, __FILE__, __FUNCTION__, __LINE__);					\
			assert(FALSE);													\
		}																	\
	} while(0)


/**
 * [TODO]
 * 在release版本增加错误返回
 **/
#define YH_ASSERT(expr)														\
	do {																	\
		assert(expr);														\
	} while(0)


/**
 * 确保句柄是有效的，否则返回指定参数
 **/
#define YH_CHECK_HANDLE_VALID(handle, err)	\
	do { \
		if (handle == INVALID_HANDLE_VALUE) return err; \
	} while(0)


// NOTE:
// 有一个新的想法，这里的参数应该是一个条件
// 如果条件成立则继续执行，否则就返回参数无效
// 这样可以保证该宏的使用场景更加广，更加灵活
// eg, param :-> (sock->_fd != INVALID_SOCKET)
// if (!param) return YH_ERR_INVALID_PARAM;
#define YH_CHECK_PARAM_VALID(param)											\
	do {																	\
		if (param == NULL) {												\
			return YH_ERR_INVALID_PARAM;									\
		}																	\
	} while(0)


/**
 * NOTE:
 * 通常如果内存出现问题，程序就直接崩溃算了，不要再做其他的清理
 * 行为了。比如，其他申请内存的回收。
 **/
#define YH_CHECK_MALLOC_VALID(buf)											\
	do {																	\
		if (buf == NULL) {													\
			YH_ASSERT(FALSE);												\
			return YH_ERR_OUT_OF_MEMORY;									\
		}																	\
	} while(0)


#define YH_DELETE_OBJ(obj)													\
	do {																	\
		free(obj);															\
		obj = 0;															\
	} while(0)


#define YH_MALLOC(buf, type, size)                                          \
    do {                                                                    \
        buf = (type)malloc(size);                                           \
        YH_CHECK_MALLOC_VALID(buf);                                         \
        memset(buf, 0, size);                                               \
    } while(0)


/**
 * 重定义__FILE__宏，只取出文件名 *
 */
#if defined(WIN32) && defined(_MSC_VER)
    #define __YH_FILE__     (strrchr(__FILE__, '\\')+1)
#elif defined(GCC)
    #define __YH_FILE__     __FILE__
#endif


#ifndef MAX
#define MAX(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif


#endif //_LIBYH_DEFINE_UTILITY_H_