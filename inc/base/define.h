/*
 * 基础库 -- 通用常量定义
 */

#ifndef _LIBYH_DEFIINE_H_
#define _LIBYH_DEFIINE_H_


// 只有这些警告是允许关闭的
#if defined(_MSC_VER)
    #pragma warning(disable:4127)   //do {} while(0)
    #pragma warning(disable:4055)   //type cast from function pointer to void *
    #pragma warning(disable:4054)   //type case from void * to funciton pointer
#elif defined(GCC)
#endif


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <stddef.h>

#include "define_macro.h"					// 定义工程中使用的所有宏开关
#include "define_os.h"						// 特定操作系统的定义
#include "define_data_type.h"				// 数据结构
#include "define_error.h"					// 错误码
#include "define_utility.h"					// 一些常用的工具宏
#include "define_config.h"					// 一些常用的配置定义
#include "common.h"                         // 一些公用的函数定义

#include "base/log.h"

#endif // _LIBYH_DEFIINE_H_