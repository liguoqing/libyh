#ifndef _LIBYH_MESSAGE_H_
#define _LIBYH_MESSAGE_H_


#include "base/define.h"


typedef enum MessageType_s MessageType;
typedef struct Message_s Message;


/**
 * 通用的消息处理回调函数类型
 * @param data 待处理的数据地址
 */
typedef _int32 (* yh_message_handler)(void *); 


/**
 * 通用的消息中释放data的回调函数类型
 * @param data 待释放的数据指针
 */
typedef _int32 (* yh_message_data_delete_handler)(void *);


_int32 yh_message_alloc(Message **msg);


_int32 yh_message_delete(Message *msg);


_int32 yh_message_default_delete_handler(void *data);


enum MessageType_s {
    MT_UNKNOWN		= 0,
    MT_LOOP_STOP	= 1,
    MT_POST_FUNC	= 2,       // 用于函数调用的线程转换
	
	MT_SOCK_ACCEPT	= 3,
	MT_SOCK_CONNECT	= 4,
	MT_SOCK_RECV	= 5,
	MT_SOCK_SEND	= 6,
	MT_SOCK_CLOSE	= 7,

    MT_FS_OPEN		= 8,
    MT_FS_READ		= 9,
    MT_FS_WRITE		= 10,
    MT_FS_CLOSE		= 11,
	MT_MAX
};


/**
 * TODO：
 * [1] _id 应该是线程安全的
 * 
 * @param _handler 当消息触发时的处理函数
 * @param _delete_handler 消息销毁时的析构函数
 **/
struct Message_s {
	_int32				            _id;                    
	MessageType						_type;
	void				            *_data;
	yh_message_handler              _handler;
    yh_message_data_delete_handler  _delete_handler;
};


#endif//_LIBYH_MESSAGE_H_
