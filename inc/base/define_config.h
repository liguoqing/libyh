/**
 * 基础库 -- 通用的配置文件
 *
 */

#ifndef _LIBYH_DEFINE_CONFIG_H_
#define _LIBYH_DEFINE_CONFIG_H_

#if defined(WIN32)
	// 暂时使用这个目录，可能还需要权限设置
	#define LOG_FILE_PATH	"d:\\tmp\\download.log"
#elif defined(LINUX)
	#define LOG_FILE_PATH	"/tmp/download.log"
#endif


#if defined(YH_DEBUG_ENABLE)
	// 用于缓存日志的某一行，注意并不是缓存所有的日志，只是临时用来设置日志行的格式
	#define LOG_BUF_SIZE	2*1024
#endif


// 外部建立work时，携带的参数的最大缓存区长度
#define MAX_CREATE_PARAM_BUF_SIZE			1024

// 255.255.255.255
#define MAX_IP_STR_LENGTH                   16

// 同时接收的连接个数
#define DEFAULT_TCP_BACKLOG                 5

// socket上的默认发送缓冲区大小
#define MAX_SOCKET_BUFFER_SIZE              127*1024
// TcpConnection上的默认接收缓冲区大小
#define DEFAULT_CONNECTION_RECV_BUF_SIZE    64*1024 // 64k

// tcp_listen_port
#define TCP_LISTEN_PORT                     5555
#define UDP_LISTEN_PORT                     6666
#define DUMY_LOCAL_TCP_PORT					7777

// 完整的文件路径名称长度
#define MAX_FILE_FULL_PATH_LEN              1024

#endif //_LIBYH_DEFINE_CONFIG_H_
