/**
 * 基础库 -- 所有的预处理宏定义
 *
 */

#ifndef _LIBYH_DEFINE_MACRO_H_
#define _LIBYH_DEFINE_MACRO_H_


#if defined(_DEBUG) || defined(DEBUG)
	#undef  YH_DEBUG_ENABLE
	#define YH_DEBUG_ENABLE
#endif


#endif
