/**
 * ������ -- ����
 **/


#ifndef _LIBYH_QUEUE_H_
#define _LIBYH_QUEUE_H_


#include "base/define_data_type.h"
#include "base/list.h"


typedef struct Queue_s Queue;


#define QUEUE_SIZE(q)


_int32 yh_queue_create(Queue **queue);


_int32 yh_queue_destroy(Queue *queue);


_int32 yh_queue_push(Queue *queue, void *data);


_int32 yh_queue_pop(Queue *queue, void **data);


_int32 yh_queue_size(Queue *queue, _uint32 *size);


struct Queue_s {
    List        *_list;
};


#endif//_LIBYH_QUEUE_H_
