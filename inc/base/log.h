/**
 * 基础库 -- 日志库定义
 *
 */

#ifndef _LIBYH_LOG_H_
#define _LIBYH_LOG_H_

#include "base/define.h"

#if defined(YH_DEBUG_ENABLE)

    /* Individual switches for different debug levels:
     * 0 is for internals debugging (lower priority), 2 is for higher level 
     * information (higher priority). */
    #define YH_DEBUG_0
    #define YH_DEBUG_1
    #define YH_DEBUG_2

#else
	
	#undef YH_DEBUG_0
	#undef YH_DEBUG_1
	#undef YH_DEBUG_2

	#ifndef YH_DEBUG_ALWAYS_KEEP_ASSERTIONS
        /* Define NDEBUG to disable standard C assertions.
         * NB: NDEBUG must appear before the #include <assert.h> */
        #ifndef NDEBUG
            #define NDEBUG
        #endif
    #endif

#endif

/* Provide a definition for this constant in every executable. */
extern const char *kOurProductName;

#ifdef __cplusplus
#include <cstdio>
#include <cassert>
#include <ctime>
extern "C" {
#else
#include <stdio.h>
#include <assert.h>
#include <time.h>
#endif

/* =============================================================================
 * stderr is used as default log file, but you can hijack it if you wish.    
 */

typedef struct 
{
    FILE *logfile;
    char logfile_path[1024];
} yh_debug_info;

extern yh_debug_info gCLDebugStat;

_int32 yh_debug_init(const char *in_log_file_path);

/* ========================================================================== */

#define YH_LOG_OPEN()                                                          \
    if (gCLDebugStat.logfile_path && gCLDebugStat.logfile_path[0]) {           \
        gCLDebugStat.logfile = fopen(gCLDebugStat.logfile_path, "a");          \
        if (gCLDebugStat.logfile == NULL)                                      \
            gCLDebugStat.logfile = stderr;                                     \
    } else {                                                                   \
        gCLDebugStat.logfile = stderr;                                         \
    }
#define YH_LOG(STF, ...)                                                       \
	if (gCLDebugStat.logfile) {                                                \
		struct tm local_time = {0};											   \
		struct timeval now = {0};											   \
        _ulong tid = 0;                                                        \
		yh_gettimeofday(&now, NULL);										   \
        yh_localtime_r(&now.tv_sec, &local_time);                              \
        yh_current_thread_id(&tid);                                            \
		fprintf((gCLDebugStat.logfile),						                   \
            "[%s:%u]-[%04d-%02d-%02d %02d:%02d:%02d:%03ld]"					   \
			"-[%s]-[%s:%d]" STF "\n",									       \
			kOurProductName, tid,                                              \
            local_time.tm_year + 1900, local_time.tm_mon + 1,                  \
			local_time.tm_mday, local_time.tm_hour, local_time.tm_min,		   \
			local_time.tm_sec, now.tv_usec / 1000,							   \
			(__YH_FILE__), (__FUNCTION__), (__LINE__), ## __VA_ARGS__);		   \
	}																		   
#define YH_LOG_CLOSE()                                                         \
    if (gCLDebugStat.logfile && gCLDebugStat.logfile != stderr &&              \
        gCLDebugStat.logfile != stdout) {                                      \
        fclose(gCLDebugStat.logfile);                                          \
    }

#ifdef WIN32
    #ifdef YH_DEBUG_USE_INDESIGN_SDK
        #define yh_debug_trace      TRACE
    #else
        #define yh_debug_trace      OutputDebugStringA
    #endif
    #define YH_DEBUG_BEGIN()        char buf[1024]
    #define YH_DEBUG_PRINT          sprintf(buf,
    #define YH_DEBUG_END()          yh_debug_trace(buf)
#else
    #define YH_DEBUG_BEGIN()    
    #define YH_DEBUG_PRINT          fprintf(stderr, 
    #define YH_DEBUG_END()
#endif

/* =============================================================================
 * Logging macros
 */
#ifdef YH_DEBUG_ENABLE
#define LOG(STF, ...)      do {                                                \
    YH_LOG_OPEN();                                                             \
    YH_LOG(STF, ## __VA_ARGS__)												   \
    YH_LOG_CLOSE();                                                            \
} while (0)
#else 
#define LOG(STF, ...)	   do { \
} while (0)
#endif

/*
#define LOG0(ST)           do {                                                \
    YH_LOG_OPEN();                                                             \
    YH_LOG "%s: [%d] %s\n", kOurProductName, __LINE__, ST);                    \
    YH_LOG_CLOSE();                                                            \
} while (0)*/

/* =============================================================================
 * Debugging macros
 */
#ifdef YH_DEBUG_0

#define debug0msg(STF, ...)      do {                                          \
    YH_DEBUG_BEGIN();                                                          \
    YH_DEBUG_PRINT  "#%s# %s[%d] " STF "\n",                                   \
            kOurProductName, __FILE__, __LINE__, ## __VA_ARGS__);              \
    YH_DEBUG_END();                                                            \
} while (0)

#define debug0msg0(ST)           do {                                          \
    YH_DEBUG_BEGIN();                                                          \
    YH_DEBUG_PRINT  "#%s# %s[%d] %s\n",                                        \
            kOurProductName, __FILE__, __LINE__, ST);                          \
    YH_DEBUG_END();                                                            \
} while (0)

#define debug_enter(ST)      do {                                              \
	YH_DEBUG_BEGIN();                                                          \
	YH_DEBUG_PRINT  "#%s# %s[%d]::%s ENTER\n",                                 \
	kOurProductName, __FILE__, __LINE__, ST);								   \
	YH_DEBUG_END();                                                            \
} while (0)

#define debug_exit(ST)      do {                                               \
	YH_DEBUG_BEGIN();                                                          \
	YH_DEBUG_PRINT  "#%s# %s[%d]::%s EXIT\n",                                  \
	kOurProductName, __FILE__, __LINE__, ST);								   \
	YH_DEBUG_END();                                                            \
} while (0)

#else

#define debug0msg(STF, ...)
#define debug0msg0(ST)
#define debug_enter(ST)
#define debug_exit(ST)

#endif

/* -------------------------------------------------------------------------- */

#ifdef YH_DEBUG_1

#define debug1msg(STF, ...)      do {                                          \
	YH_DEBUG_BEGIN();                                                          \
	YH_DEBUG_PRINT  "#%s# %s[%d] " STF "\n",                                   \
	kOurProductName, __FILE__, __LINE__, ## __VA_ARGS__);					   \
	YH_DEBUG_END();                                                            \
} while (0)

#define debug1msg0(ST)           do {                                          \
	YH_DEBUG_BEGIN();                                                          \
	YH_DEBUG_PRINT  "#%s# %s[%d] %s\n",                                        \
	kOurProductName, __FILE__, __LINE__, ST);								   \
	YH_DEBUG_END();                                                            \
} while (0)

#else

#define debug1msg(STF, ...)
#define debug1msg0(ST)

#endif

/* -------------------------------------------------------------------------- */

#ifdef YH_DEBUG_2

#define debug2msg(STF, ...)      do {                                          \
	YH_DEBUG_BEGIN();                                                          \
	YH_DEBUG_PRINT  "#%s# %s[%d] " STF "\n",                                   \
	kOurProductName, __FILE__, __LINE__, ## __VA_ARGS__);					   \
	YH_DEBUG_END();                                                            \
} while (0)

#define debug2msg0(ST)           do {                                          \
	YH_DEBUG_BEGIN();                                                          \
	YH_DEBUG_PRINT  "#%s# %s[%d] %s\n",                                        \
	kOurProductName, __FILE__, __LINE__, ST);								   \
	YH_DEBUG_END();                                                            \
} while (0)

#else

#define debug1msg(STF, ...)
#define debug1msg0(ST)

#endif

/* ========================================================================== */

#ifdef __cplusplus
}
#endif

#endif //_LIBYH_LOG_H_
