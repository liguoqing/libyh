/**
 * 基础库 -- 
 *
 */

#ifndef _LIBYH_IPC_H_
#define _LIBYH_IPC_H_


#include "define.h"
#include "message_queue.h"


/**
 * 向某个消息队列投递一个消息，用于线程间的通信
 * @param mq
 * @param msg
 */
_int32 yh_post_message(MessageQueue *mq, Message *msg);


#endif//_LIBYH_IPC_H_