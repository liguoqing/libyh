/*
 * ������ -- �����붨��
 */

#ifndef _LIBYH_ERROR_H_
#define _LIBYH_ERROR_H_

#if defined(WIN32) && defined(_MSC_VER)
	#pragma warning(disable:4996)
#endif

#define YH_SUCCESS							0
#define YH_UNKNOWN_ERR						-1
#define YH_ERR_OUT_OF_MEMORY				1
#define YH_ERR_INVALID_PARAM				2


#define YH_ERR_CANOT_CREATE_IOCP			100
#define YH_ERR_CANOT_GET_IOCP_STATUS		101
#define YH_ERR_CANOT_POST_IOCP_STATUS		102


/**
 * error code for net library
 **/
#define YH_ERR_NET_LIB_BASE                 1024
#define YH_ERR_SOCKET_ENV_INVALID           YH_ERR_NET_LIB_BASE + 1  
#define YH_ERR_SOCKET_CREATE                YH_ERR_NET_LIB_BASE + 2
#define YH_ERR_SOCKET_FD_INVALID            YH_ERR_NET_LIB_BASE + 3
#define YH_ERR_SOCKET_OP_FAILURE            YH_ERR_NET_LIB_BASE + 4
#define YH_ERR_TCP_DEVICE_WRONG_STATE       YH_ERR_NET_LIB_BASE + 100
#define YH_ERR_TCP_DEVICE_INNER_FAILURE     YH_ERR_NET_LIB_BASE + 101
#define YH_ERR_TCP_CONN_RMT_CLOSED          YH_ERR_NET_LIB_BASE + 102


/**
 * error code for base library
 **/
#define YH_ERR_COMM_LIB_BASE                2048                    
#define YH_ERR_LIST_NO_CMP_DATA             YH_ERR_COMM_LIB_BASE + 1
#define YH_ERR_QUEUE_CREATE_FAILURE         YH_ERR_COMM_LIB_BASE + 2
#define YH_ERR_SIGNAL_INVALID_HANDLE        YH_ERR_COMM_LIB_BASE + 3


/**
 * error code for fs library
 **/
#define YH_ERR_FS_LIB_BASE					3096
#define YH_ERR_CANOT_OPEN_FILE				YH_ERR_FS_LIB_BASE + 1
#define YH_ERR_READ_FILE_FAIL               YH_ERR_FS_LIB_BASE + 2
#define YH_ERR_WRITE_FILE_FAIL              YH_ERR_FS_LIB_BASE + 3
#define YH_ERR_CLOSE_FILE_FAIL              YH_ERR_FS_LIB_BASE + 4
#define YH_ERR_FS_DEVICE_WRONG_STATE		YH_ERR_FS_LIB_BASE + 5
#define YH_ERR_FS_DEVICE_CANT_CANCEL        YH_ERR_FS_LIB_BASE + 6
#define YH_ERR_CANOT_GET_FILE_SIZE			YH_ERR_FS_LIB_BASE + 7
#define YH_ERR_READ_FILE_EOF                YH_ERR_FS_LIB_BASE + 8
#define YH_ERR_WRITE_FILE_EOF               YH_ERR_FS_LIB_BASE + 9


/**
 * error code for specific os
 **/
#define YH_ERR_PLAFORM_BASE					1024 * 10
#define YH_ERR_WIN_IOCP_ASSOCIATE			YH_ERR_PLAFORM_BASE + 1


#endif