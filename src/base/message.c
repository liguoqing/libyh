#include "base/message.h"


static _uint32 gMsgId = 0;


_int32 yh_message_alloc(Message **msg)
{
	YH_ASSERT(*msg == NULL);
	YH_MALLOC(*msg, Message *, sizeof(Message));

    // TODO：
	// 这里需要线程安全！
    (*msg)->_id = gMsgId++;

    (*msg)->_data = NULL;
    (*msg)->_handler = NULL;
    (*msg)->_type = MT_UNKNOWN;
    (*msg)->_delete_handler = yh_message_default_delete_handler;

    return YH_SUCCESS;
}


_int32 yh_message_delete(Message *msg)
{
    _int32 _ret = YH_SUCCESS;
    YH_CHECK_PARAM_VALID(msg);

	/**
	 * 如果_delete_handler为空，意味着当前data并不是由message来负责
	 * 释放的，这种情况是可能的。
	 **/
    if (msg->_delete_handler) {
        _ret = msg->_delete_handler(msg->_data);
        if (_ret != YH_SUCCESS) return _ret;
		msg->_data = NULL;
    }

    YH_DELETE_OBJ(msg);

    return _ret;
}


_int32 yh_message_default_delete_handler(void *data)
{
    if (data) free(data);
    return YH_SUCCESS;
}
