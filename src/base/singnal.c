#include "base/singnal.h"


_int32 yh_create_event(YHHandle *ev)
{
#if defined(WIN32)
	if (*ev == INVALID_HANDLE_VALUE) {
		*ev = CreateEvent(NULL, TRUE, FALSE, NULL);

		if (*ev == INVALID_HANDLE_VALUE) {
			return YH_ERR_SIGNAL_INVALID_HANDLE;
		}
	}	
#elif defined(LINUX)
#endif

	return YH_SUCCESS;
}


_int32 yh_destroy_event(YHHandle ev)
{
#if defined(WIN32)
	if (ev != INVALID_HANDLE_VALUE) {
		CloseHandle(ev);
	}
#elif defined(LINUX)
#endif

	return YH_SUCCESS;
}


_int32 yh_wait(YHHandle p)
{
#if defined(WIN32)
    if (p != INVALID_HANDLE_VALUE) {
		WaitForSingleObject(p, INFINITE);
    }
#elif defined(LINUX)
#endif

    return YH_SUCCESS;
}


/**
 * FIXME
 * 这里的handle也应该设为指针，这样可以同时将该事件置为空，外部可以重用
 * 该事件handle
 **/
_int32 yh_signal(HANDLE p)
{
    if (p == INVALID_HANDLE_VALUE) {
        return YH_ERR_SIGNAL_INVALID_HANDLE;
    }

#if defined(WIN32)
    SetEvent(p);
#elif defined(LINUX)
#endif

    return YH_SUCCESS;
}
