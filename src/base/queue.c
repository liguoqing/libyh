#include "base/queue.h"
#include "base/define.h"


_int32 yh_queue_create(Queue **queue)
{
    _int32 _ret = YH_SUCCESS;

    YH_ASSERT(*queue == NULL);

    *queue = (Queue *)malloc(sizeof(Queue));
    if (!(*queue)) {
        return YH_ERR_OUT_OF_MEMORY;
    }

    memset(*queue, 0, sizeof(Queue));

    _ret = yh_list_create(&((*queue)->_list));
    YH_CHECK_VALUE(_ret);

    return _ret;
}


_int32 yh_queue_destroy(Queue *queue)
{
    _int32 _ret = YH_SUCCESS;
    _uint32 _size = 0;
    void *_data = NULL;

    YH_CHECK_PARAM_VALID(queue);

    _ret = yh_queue_size(queue, &_size);
    YH_CHECK_VALUE(_ret);

    do {
        _ret = yh_queue_pop(queue, &_data);
    } while (_data && (_ret == YH_SUCCESS));

    if (_ret == YH_SUCCESS) {
		_ret = yh_list_destroy(queue->_list);
		YH_CHECK_VALUE(_ret);

        YH_DELETE_OBJ(queue);
    }

    return _ret;
}


_int32 yh_queue_push(Queue *queue, void *data)
{
    _int32 _ret = YH_SUCCESS;
    ListNode *_tail = NULL;

    YH_CHECK_PARAM_VALID(queue);
    YH_CHECK_PARAM_VALID(data);

    _ret = yh_list_tail(queue->_list, &_tail);
    YH_CHECK_VALUE(_ret);

    // insert after tail
    _ret = yh_list_insert(queue->_list, data, _tail);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


_int32 yh_queue_pop(Queue *queue, void **data)
{
    _int32 _ret = YH_SUCCESS;
    ListNode *_tail = NULL;
    *data = NULL;

    YH_CHECK_PARAM_VALID(queue);
    
    _ret = yh_list_tail(queue->_list, &_tail);
    YH_CHECK_VALUE(_ret);

    if (_tail) {
        *data = _tail->_data;
        _ret = yh_list_node_delete(queue->_list, _tail);
    }

    return _ret;
}


_int32 yh_queue_size(Queue *queue, _uint32 *size)
{
    YH_CHECK_PARAM_VALID(queue);
    YH_CHECK_PARAM_VALID(size);

    *size = queue->_list->_size;
    
    return YH_SUCCESS;
}
