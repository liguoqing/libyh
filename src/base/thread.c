#include "base/thread.h"
#include "base/define.h"


static void *dummy_worker(void *opaque)
{
	yh_thread *thd = (yh_thread *)opaque;
	thread_start_func func = (thread_start_func)thd->_thread_proc;
	return func(thd, thd->_data);
}


_int32 yh_thread_create(yh_thread **new_thread, thread_start_func func, void *data)
{
	unsigned temp = 0;
    HANDLE handle = 0;

    (*new_thread) = (yh_thread *)malloc(sizeof(yh_thread));

    if ((*new_thread) == NULL) {
		printf("malloc thread struct error.\n");
        return YH_UNKNOWN_ERR;
    }

    (*new_thread)->_data = data;
    (*new_thread)->_thread_proc = (void *)func;

#if defined(WIN32)
    /* Use 0 for default Thread Stack Size, because that will
     * default the stack to the same size as the calling thread.
     */
    if ((handle = (HANDLE)_beginthreadex(NULL, 0, 
						(unsigned int (__stdcall *)(void *))dummy_worker,
                        (*new_thread), 0, &temp)) == 0) {
		printf("beginthreadex error.\n");
        return YH_UNKNOWN_ERR;
    }
#else 
#endif

	(*new_thread)->_tid = handle;

	return YH_SUCCESS;
}


_int32 yh_thread_destroy(yh_thread *new_thread)
{
    _int32 _ret = YH_SUCCESS;
    YH_CHECK_PARAM_VALID(new_thread);

    YH_DELETE_OBJ(new_thread);

    return _ret;
}


_int32 yh_get_current_thread_id(_ulong *tid)
{
    YH_CHECK_PARAM_VALID(tid);

#if defined(WIN32)
    {
        DWORD _tid = GetCurrentThreadId();
        *tid = (_ulong)_tid;
    }
#endif

    return YH_SUCCESS;
}


_int32 yh_mutex_init(yh_mutex *mutex)
{
	InitializeCriticalSection(mutex);
	return YH_SUCCESS;
}


_int32 yh_mutex_destroy(yh_mutex *mutex)
{
	DeleteCriticalSection(mutex);
	return YH_SUCCESS;
}


_int32 yh_mutex_lock(yh_mutex *mutex)
{
	EnterCriticalSection(mutex);
	return YH_SUCCESS;
}


_int32 yh_mutex_unlock(yh_mutex *mutex)
{
	LeaveCriticalSection(mutex);
	return YH_SUCCESS;
}
