#include "base/list.h"
#include "base/define.h"


static _int32 yh_list_node_free(ListNode *list_node);


_int32 yh_list_create(List **list)
{
    *list = (List *)malloc(sizeof(List));
    if (!(*list)) {
        return YH_ERR_OUT_OF_MEMORY;
    }

    memset(*list, 0, sizeof(List));

    return YH_SUCCESS;
}


_int32 yh_list_destroy(List *list)
{
    _int32 _ret = YH_SUCCESS;
    ListNode *_node = NULL;
	_uint32 _idx = 0;

    YH_CHECK_PARAM_VALID(list);

    _node = list->_head;
	_idx = list->_size;

    while (_idx--) {
        ListNode *_tmp_node = _node->_next;
        _ret = yh_list_node_free(_node);
        YH_CHECK_VALUE(_ret);
        _node = _tmp_node;
    }

	YH_DELETE_OBJ(list);

    return _ret;
}


_int32 yh_list_insert(List *list, void *data, ListNode *after)
{
    ListNode *_after = after;
    ListNode *_new_node = NULL;

    YH_CHECK_PARAM_VALID(list);
    YH_CHECK_PARAM_VALID(data);

    _new_node = (ListNode *)malloc(sizeof(ListNode));
    if (!_new_node) return YH_ERR_OUT_OF_MEMORY;
    memset(_new_node, 0, sizeof(ListNode));

    if (list->_size == 0) {
		YH_ASSERT(after == NULL);
        list->_head = _new_node;
        list->_head->_data = data;
        list->_head->_next = list->_head;
        list->_head->_prev = list->_head;
    }
    else {
        YH_ASSERT(list->_head != NULL);

        if (!_after) {
            _after = list->_head->_prev;
        }
        _new_node->_next = _after->_next;
        _new_node->_prev = _after;
        _new_node->_data = data;
        _after->_next->_prev = _new_node;
        _after->_next = _new_node;
    }

	list->_size += 1;

    return YH_SUCCESS;
}


_int32 yh_list_tail(List *list, ListNode **tail)
{
    YH_CHECK_PARAM_VALID(list);

    if (list->_size == 0) {
        YH_ASSERT(list->_head == NULL);
        *tail = NULL;
    }
    else {
        *tail = list->_head->_prev;
    }

    return YH_SUCCESS;
}


_int32 yh_list_node_find(List *list, void *data, yh_list_data_cmp_func cmp, 
                    ListNode **match_node)
{
    YH_CHECK_PARAM_VALID(list);
    YH_CHECK_PARAM_VALID(data);
    YH_CHECK_PARAM_VALID(cmp);

    if (list->_size != 0) {
        ListNode *_node = list->_head;

        while (_node) {
            if (cmp(_node->_data, data) == 0) {
                *match_node = _node;
                break;
            }

            _node = _node->_next;
        }
    }
    else {
        *match_node = NULL;
    }

    return YH_SUCCESS;
}


_int32 yh_list_node_delete(List *list, ListNode *node)
{
    _int32 _ret = YH_SUCCESS;
    ListNode *_tmp_node = NULL;
    _uint32 _idx = 0;
	BOOL _find = FALSE;

    YH_CHECK_PARAM_VALID(list);
    YH_CHECK_PARAM_VALID(node);

    if (list->_size == 0) {
        return YH_ERR_INVALID_PARAM;
    }

    _tmp_node = list->_head;
    _idx = list->_size;    
    while (_idx--) {
        if (_tmp_node == node) {
            if (node == list->_head) {
				if (list->_size == 1) {
					_ret = yh_list_node_free(list->_head);
					list->_head = NULL;
				}
				else {
					_tmp_node = list->_head->_next;
					list->_head->_prev->_next = list->_head->_next;
					list->_head->_next->_prev = list->_head->_prev;
					_ret = yh_list_node_free(list->_head);
					list->_head = _tmp_node;
				}
            }
            else {
                node->_prev->_next = node->_next;
                node->_next->_prev = node->_prev;
                _ret = yh_list_node_free(node);
            }
			_find = TRUE;
            break;
        }
        else {
            _tmp_node = _tmp_node->_next;
        }
    }

    if (_find) {
		list->_size --;
		_ret = YH_SUCCESS;
    }
    else {
		_ret = YH_ERR_LIST_NO_CMP_DATA;        
    }   

    return _ret;
}


static _int32 yh_list_node_free(ListNode *list_node)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(list_node);

    YH_DELETE_OBJ(list_node);
    
    return _ret;
}


#if defined(YH_DEBUG_ENABLE)
_int32 yh_list_print_data(List *list, yh_list_print_func print)
{
	_uint32 _idx = 0;
	ListNode *_node = NULL;

	YH_CHECK_PARAM_VALID(list);
	YH_CHECK_PARAM_VALID(print);

	_node = list->_head;
	_idx = list->_size;

	while (_idx--) {
		print(_node->_data);
		_node = _node->_next;
	}

	return YH_SUCCESS;
}
#endif

