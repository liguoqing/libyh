#include "base/log.h"

yh_debug_info gCLDebugStat = {0, ""};

_int32 yh_debug_init(const char *in_log_file_path)
{
	gCLDebugStat.logfile = stderr;
	strncpy(gCLDebugStat.logfile_path, in_log_file_path, strlen(in_log_file_path));
	return YH_SUCCESS;
}
