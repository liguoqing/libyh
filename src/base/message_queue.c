#include "base/message_queue.h"


_int32 yh_mq_init(MessageQueue **mq)
{
	_int32 _ret = YH_SUCCESS;

	YH_ASSERT(*mq == NULL);

	*mq = (MessageQueue *)malloc(sizeof(MessageQueue));
	if (!*mq) {
		return YH_ERR_OUT_OF_MEMORY;
	}

	memset(*mq, 0, sizeof(MessageQueue));

	return _ret;
}


_int32 yh_mq_put(MessageQueue *mq, Message *msg)
{
    _int32 _ret = YH_SUCCESS;
    MessageQueueNode *_cur_head = NULL, *_node = NULL;

	YH_CHECK_PARAM_VALID(mq);
	YH_CHECK_PARAM_VALID(msg);

    _ret = yh_mq_node_alloc(&_node);
    YH_CHECK_VALUE(_ret);

    _node->_msg = msg;

    _cur_head = mq->_head;
    mq->_head = _node;

	if (!_cur_head) {
        // 空队列
		YH_ASSERT(mq->_tail == NULL);
		YH_ASSERT(mq->_size == 0);

		mq->_tail = mq->_head;
        mq->_tail->_prev = mq->_head;
	}
	else {
		_cur_head->_next = mq->_head;
        mq->_head->_prev = _cur_head;
	}
    
    mq->_head->_next = mq->_tail;
	mq->_size ++;

	return YH_SUCCESS;
}


_int32 yh_mq_pop(MessageQueue *mq, Message **msg)
{
    _int32 _ret = YH_SUCCESS;
    MessageQueueNode *_node = NULL;

	YH_CHECK_PARAM_VALID(mq);

	if (mq->_size == 0) {
        YH_ASSERT(mq->_head == NULL);
        YH_ASSERT(mq->_tail == NULL);

        *msg = NULL;
	}
	else {
		_node = mq->_tail;
        mq->_size --;

        if (mq->_size == 0) {
            mq->_tail = mq->_head = NULL;
        }
        else {
            mq->_tail = mq->_tail->_next;
            mq->_tail->_prev = mq->_head;
            mq->_head->_next = mq->_tail;
        }

        *msg = _node->_msg;
        _ret = yh_mq_node_delete(_node);
        YH_CHECK_VALUE(_ret);
	}

	return _ret;
}


_int32 yh_mq_empty(MessageQueue *mq, BOOL *empty)
{
	YH_CHECK_PARAM_VALID(mq);

    // NOTE
    // mq->_head == mq->_tail 并不能代表此时队列是空的
    // 比如，当队列中只有一个元素时
	*empty = (mq->_size == 0) ? TRUE : FALSE;

	return YH_SUCCESS;
}


_int32 yh_mq_size(MessageQueue *mq, _uint32 *size)
{
    YH_CHECK_PARAM_VALID(mq);

    *size = mq->_size;

	return YH_SUCCESS;
}


_int32 yh_mq_uninit(MessageQueue *mq)
{
	_int32 _ret = YH_SUCCESS;
    Message *_cur_msg = NULL;

	YH_CHECK_PARAM_VALID(mq);

    while (mq->_size) {
        _ret = yh_mq_pop(mq, &_cur_msg);
        if (_cur_msg) {
            _ret = yh_message_delete(_cur_msg);
            YH_CHECK_VALUE(_ret);
            _cur_msg = NULL;
        }
    }

    YH_DELETE_OBJ(mq);

	return _ret;
}


_int32 yh_mq_node_alloc(MessageQueueNode **node)
{
    *node = (MessageQueueNode *)malloc(sizeof(MessageQueueNode));
    if (*node == NULL) {
        return YH_ERR_OUT_OF_MEMORY;
    }

    memset(*node, 0, sizeof(MessageQueueNode));

    //NOTE
    //这里有个选择，是直接分配呢，还是外部设置，感觉两种需求都需要
    //从目前来看，外部插入是比较合理的，所以这里使用外部的msg空间
    //return yh_message_alloc(&((*node)->_msg));
    return YH_SUCCESS;
}


_int32 yh_mq_node_delete(MessageQueueNode *node)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(node);

    //NOTE
    //跟分配一样，这里同样是两个抉择
    //_ret = yh_message_delete(node->_msg);
    //if (_ret != YH_SUCCESS) return _ret;

    YH_DELETE_OBJ(node);

    return _ret;
}
