#include "os/platform_interface.h"
#include "base/thread.h"
#include <time.h>
#if !defined(WIN32)
    #include <sys/time.h>
#endif

_int32 yh_gettimeofday(struct timeval *tp, void *tzp)
{
#if defined(WIN32)
    __time32_t clock;
    struct tm tm;
    SYSTEMTIME wtm;
    GetLocalTime(&wtm);
    tm.tm_year = wtm.wYear - 1900;
    tm.tm_mon = wtm.wMonth - 1;
    tm.tm_mday = wtm.wDay;
    tm.tm_hour = wtm.wHour;
    tm.tm_min = wtm.wMinute;
    tm.tm_sec = wtm.wSecond;
    tm. tm_isdst = -1;
    clock = _mktime32(&tm);
    tp->tv_sec = clock;
    tp->tv_usec = wtm.wMilliseconds * 1000;
    tzp = tzp;
    return YH_SUCCESS; 
#elif defined(LINUX)
    return gettimeofday(tp, tzp);
#else
    return YH_UNKNOWN_ERR;
#endif    
}

_int32 yh_localtime_r(const long *timep, struct tm *result)
{
#if defined(WIN32)
    __time32_t long_time = *timep;
    return (_localtime32_s(result, &long_time) == 0) ? YH_SUCCESS : YH_UNKNOWN_ERR;
#elif defined(LINUX)
    return (localtime_r(timep, result) != NULL) ? YH_SUCCESS : YH_UNKNOWN_ERR;
#else
    return YH_UNKNOWN_ERR;
#endif
}

_int32 yh_current_thread_id(unsigned long  *id)
{
    return yh_get_current_thread_id(id);
}
