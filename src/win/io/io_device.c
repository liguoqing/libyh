#include "io/io_device.h"
#include "io/iocp.h"
#include "base/list.h"


_int32 yh_device_create(EventLoop *loop, 
						YHHandle fd, 
						IODeviceType type, 
						IODevice **device)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(loop);

	YH_MALLOC(*device, IODevice *, sizeof(IODevice));

	(*device)->loop = loop;
	(*device)->_fd = fd;
	(*device)->_type = type;
	
    if (fd != INVALID_HANDLE_VALUE) {
	    _ret = yh_iocp_associate_fd(loop->_iocp, fd, 0);
	    if (_ret != YH_SUCCESS) {
		    YH_DELETE_OBJ(*device);
		    *device = NULL;
			YH_ASSERT(FALSE);
		    return YH_ERR_WIN_IOCP_ASSOCIATE;
	    }
    }

	return _ret;
}


/**
 * 本质上该函数的用意就是修改device中的fd，可能的用途有：
 * 1、网络设备中重传的时候可能需要更新socket fd
 * 2、文件设备中在open的时候才真正创建文件，create的时候只能将fd置为无效
 **/
_int32 yh_device_reset_fd(IODevice *device, YHHandle fd)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(device);
    YH_ASSERT(fd != INVALID_HANDLE_VALUE);

	device->_fd = fd;

    _ret = yh_iocp_associate_fd(device->loop->_iocp, fd, 0);
    if (_ret != YH_SUCCESS) {
        return YH_ERR_WIN_IOCP_ASSOCIATE;
    }

    return _ret;
}


_int32 yh_device_destroy(IODevice *device)
{
	YH_CHECK_PARAM_VALID(device);

	YH_DELETE_OBJ(device);
	return YH_SUCCESS;
}
