#include "io/io_request.h"
/**
 * 这里面的实现感觉有很多可以通过宏来简化，后续再优化吧
 * NOTE:
 * 这里面的函数device都声明成IODevice类型的，但是传进来的都强制
 * 为IOXXDevice，所以在使用的时候要注意！既容易出错又不优雅，FIXME
 **/


// NOTE:
// 这里的operation统一在IOCP完成时释放
// 这样的实现貌似有点不优雅，不知道怎么才更好
#define DESTROY_SOCK_REQ(req)												   \
	do {																	   \
		if (req->_base) {												   \
			YH_DELETE_OBJ(req->_base);									   \
		}																	   \
		YH_DELETE_OBJ(req);													   \
	} while(0)


/**
 * NOTE:
 * 默认OverlappedWrapper中的data是这里构造出来的req
 **/
_int32 yh_create_sock_conn_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
                               const struct sockaddr_in *addr,
                               _uint32 addr_len,
							   void *user_data,
							   SockConnReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(device);
    YH_CHECK_PARAM_VALID(handler);
    YH_CHECK_PARAM_VALID(addr);

	YH_MALLOC(*req, SockConnReq *, sizeof(SockConnReq));
	YH_MALLOC((*req)->_base, IORequest *, sizeof(IORequest));

	(*req)->_base->_device = device;
	(*req)->_base->_callback = callback;
    memcpy(&((*req)->_conn_addr), addr, addr_len);
	(*req)->_base->_user_data = user_data;

	_ret = yh_create_overlapped_wrapper(op, MT_SOCK_CONNECT, handler, NULL,
		(void *)(*req));
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(*op != NULL);
	YH_ASSERT((*op)->_msg != NULL);
	YH_ASSERT((*op)->_msg->_data == (void *)(*req));
	YH_ASSERT((*op)->_msg->_handler == handler);
	YH_ASSERT((*op)->_msg->_delete_handler == NULL);

	(*req)->_base->_op = *op;

	return _ret;
}


_int32 yh_destroy_sock_conn_req(SockConnReq *req)
{
	YH_CHECK_PARAM_VALID(req);
	DESTROY_SOCK_REQ(req);
	return YH_SUCCESS;
}


_int32 yh_create_sock_acce_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
							   SockAcceReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_MALLOC(*req, SockAcceReq *, sizeof(SockAcceReq));
	YH_MALLOC((*req)->_base, IORequest *, sizeof(IORequest));

	(*req)->_base->_device = device;
	(*req)->_base->_callback = callback;

	(*req)->_acc_sock_fd = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 
		0, WSA_FLAG_OVERLAPPED);
	if ((*req)->_acc_sock_fd == INVALID_SOCKET) {
		YH_DELETE_OBJ((*req)->_base);
		YH_DELETE_OBJ(*req);
		return YH_ERR_SOCKET_CREATE;
	}

	// 第一次发送的字节数为0
	// 只存有两个地址的长度
	YH_MALLOC((*req)->_buffer, char *, 2*(sizeof(struct sockaddr_in) + 16));

	_ret = yh_create_overlapped_wrapper(op, MT_SOCK_ACCEPT, handler, NULL,
		(void *)(*req));
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(*op != NULL);
	YH_ASSERT((*op)->_msg != NULL);
	YH_ASSERT((*op)->_msg->_data == (void *)(*req));
	YH_ASSERT((*op)->_msg->_handler == handler);
	YH_ASSERT((*op)->_msg->_delete_handler == NULL);

	(*req)->_base->_op = *op;

	return _ret;
}


_int32 yh_destroy_sock_acce_req(SockAcceReq *req)
{
	YH_CHECK_PARAM_VALID(req);

	if (req->_buffer) {
		YH_DELETE_OBJ(req->_buffer);
	}

	/**
	 * TODO
	 * 这里的acc_sock_fd是要后续使用的，明显不能在这里关掉。。。
	 * 那这个socket的生命周期是怎样的？
	 **/
	/*if (req->_acc_sock_fd != INVALID_SOCKET) {
		closesocket(req->_acc_sock_fd);
		req->_acc_sock_fd = INVALID_SOCKET;
	}*/

	DESTROY_SOCK_REQ(req);

	return YH_SUCCESS;
}


_int32 yh_create_sock_send_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
							   void *user_data,
							   SockSendReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_MALLOC(*req, SockSendReq *, sizeof(SockSendReq));
	YH_MALLOC((*req)->_base, IORequest *, sizeof(IORequest));

	(*req)->_base->_device = device;
	(*req)->_base->_callback = callback;
	(*req)->_base->_user_data = user_data;

	_ret = yh_create_overlapped_wrapper(op, MT_SOCK_SEND, handler, NULL,
		(void *)(*req));
	YH_CHECK_VALUE(_ret);

	(*req)->_base->_op = *op;

	return _ret;
}


_int32 yh_destroy_sock_send_req(SockSendReq *req)
{
	YH_CHECK_PARAM_VALID(req);

	DESTROY_SOCK_REQ(req);

	return YH_SUCCESS;
}


_int32 yh_create_sock_recv_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
							   void *user_data,
							   SockRecvReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_MALLOC(*req, SockRecvReq *, sizeof(SockRecvReq));
	YH_MALLOC((*req)->_base, IORequest *, sizeof(IORequest));

	(*req)->_base->_device = device;
	(*req)->_base->_callback = callback;
	(*req)->_base->_user_data = user_data;

	_ret = yh_create_overlapped_wrapper(op, MT_SOCK_RECV, handler, NULL,
		(void *)(*req));
	YH_CHECK_VALUE(_ret);

	(*req)->_base->_op = *op;

	return _ret;
}


_int32 yh_destroy_sock_recv_req(SockRecvReq *req)
{
	YH_CHECK_PARAM_VALID(req);

	DESTROY_SOCK_REQ(req);

	return YH_SUCCESS;
}


_int32 yh_create_sock_clos_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
                               void *user_data,
							   SockClosReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(device);

	YH_MALLOC(*req, SockClosReq *, sizeof(SockClosReq));
	YH_MALLOC((*req)->_base, IORequest *, sizeof(IORequest));	

	(*req)->_base->_device = device;
	(*req)->_base->_callback = callback;
    (*req)->_base->_user_data = user_data;

	_ret = yh_create_overlapped_wrapper(op, MT_SOCK_CLOSE, handler, NULL,
		(void *)(*req));
	YH_CHECK_MALLOC_VALID(*op);

	(*req)->_base->_op = *op;

	return _ret;
}


_int32 yh_destroy_sock_clos_req(SockClosReq *req)
{
	YH_CHECK_PARAM_VALID(req);

	if (req->_base) {
		YH_DELETE_OBJ(req->_base);
	}
	
	YH_DELETE_OBJ(req);

	return YH_SUCCESS;
}


/*
_int32 yh_create_sock_disc_req(OverlappedWrapper **op, 
							   yh_message_handler handler,
							   IODevice *device,
							   void *callback,
							   IOSockDiscReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(device);

	return _ret;
}*/


_int32 yh_create_fs_open_req(OverlappedWrapper **op, 
							 yh_message_handler handler,
							 IODevice *device,
							 void *callback,
                             void *user_data,
							 FsOpenReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(device);

	YH_MALLOC(*req, FsOpenReq *, sizeof(FsOpenReq));
	YH_MALLOC((*req)->_base, IORequest *, sizeof(IORequest));

	(*req)->_base->_device = device;
	(*req)->_base->_callback = callback;
    (*req)->_base->_user_data = user_data;

	_ret = yh_create_overlapped_wrapper(op, MT_FS_OPEN, handler, NULL,
		(void *)(*req));
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(*op != NULL);

	(*req)->_base->_op = *op;

	LOG("op:0x%x, req:0x%x.", *op, *req);

	return _ret;
}


_int32 yh_destroy_fs_open_req(FsOpenReq *req)
{
	YH_CHECK_PARAM_VALID(req);
	DESTROY_SOCK_REQ(req);
	return YH_SUCCESS;	
}


_int32 yh_create_fs_close_req(OverlappedWrapper **op, 
							  yh_message_handler handler,
							  IODevice *device,
							  void *callback,
                              void *user_data,
							  FsCloseReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(device);
	YH_CHECK_PARAM_VALID(handler);

	YH_MALLOC(*req, FsCloseReq *, sizeof(FsCloseReq));
	YH_MALLOC((*req)->_base, IORequest *, sizeof(IORequest));

	(*req)->_base->_device = device;
	(*req)->_base->_callback = callback;
    (*req)->_base->_user_data = user_data;

	_ret = yh_create_overlapped_wrapper(op, MT_FS_CLOSE, handler, NULL,
		(void *)(*req));
	YH_CHECK_MALLOC_VALID(*op);

	(*req)->_base->_op = *op;

	return _ret;
}


_int32 yh_destroy_fs_close_req(FsCloseReq *req)
{
	YH_CHECK_PARAM_VALID(req);
	DESTROY_SOCK_REQ(req);
	return YH_SUCCESS;		
}

_int32 yh_create_fs_read_req(OverlappedWrapper **op, 
							 yh_message_handler handler,
							 IODevice *device,
							 _uint64 start_pos,
							 void *callback,
                             void *user_data,
							 FsReadReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(device);
	YH_CHECK_PARAM_VALID(handler);

	YH_MALLOC(*req, FsReadReq *, sizeof(FsReadReq));
	YH_MALLOC((*req)->_base, IORequest *, sizeof(IORequest));

	(*req)->_base->_device = device;
	(*req)->_base->_callback = callback;
    (*req)->_base->_user_data = user_data;
    (*req)->_start_pos = start_pos;
    (*req)->_read_len = 0;

	_ret = yh_create_overlapped_wrapper(op, MT_FS_READ, handler, NULL,
		(void *)(*req));
    YH_CHECK_VALUE(_ret);
	YH_CHECK_MALLOC_VALID(*op);

	(*req)->_base->_op = *op;

	return _ret;
}


_int32 yh_destroy_fs_read_req(FsReadReq *req)
{
	YH_CHECK_PARAM_VALID(req);
	DESTROY_SOCK_REQ(req);
	return YH_SUCCESS;	
}


_int32 yh_create_fs_write_req(OverlappedWrapper **op, 
							  yh_message_handler handler,
							  IODevice *device,
							  _uint64 start_pos,
							  void *callback,
							  void *user_data,
							  FsWriteReq **req)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(device);
	YH_CHECK_PARAM_VALID(handler);

	YH_MALLOC(*req, FsWriteReq *, sizeof(FsWriteReq));
	YH_MALLOC((*req)->_base, IORequest *, sizeof(IORequest));

	(*req)->_base->_device = device;
	(*req)->_base->_callback = callback;
    (*req)->_base->_user_data = user_data;
    (*req)->_start_pos = start_pos;
    (*req)->_write_len = 0;

	_ret = yh_create_overlapped_wrapper(op, MT_FS_WRITE, handler, NULL,
		(void *)(*req));
	YH_CHECK_MALLOC_VALID(*op);

	(*req)->_base->_op = *op;

	return _ret;
}


_int32 yh_destroy_fs_write_req(FsWriteReq *req)
{
	YH_CHECK_PARAM_VALID(req);
	DESTROY_SOCK_REQ(req);
	return YH_SUCCESS;	
}

