#include "io/event_loop.h"
#include "io/iocp.h"


/**
 * 发现直接判断loop里面的stop_flag是不合适的，因为可能存在这种情况：
 * 1、外部线程停止loop线程；
 * 2、loop线程完成停止行为，此时外部线程需要等待loop线程的完成；
 * 3、当loop线程完成时，loop其实应该已经无效了（如果不是无效，可能就得
 * 分出两个接口，一个stop，一个destory，这种做法也可以）
 **/
static BOOL gLoopIsStopping = FALSE;


/**
 * 统一处理loop中的op接口
 * @param loop
 * @param op
 */
static _int32 yh_loop_handle_overlappedop(EventLoop *loop, 
										  OverlappedWrapper *op)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(loop);
    YH_CHECK_PARAM_VALID(op);

	LOG("op:0x%x, type:%d, handler:0x%x.", op, op->_msg->_type, 
		op->_msg->_handler);

    if (op->_msg->_type == MT_LOOP_STOP) {
        loop->_stop_flag = 1;
    }

    if (op->_msg->_handler) {
        _ret = op->_msg->_handler((void *)op);
    }

	// op在这里统一销毁
    _ret = yh_destroy_overlapped_wrapper(op);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


_int32 yh_loop_create(EventLoop **loop)
{
    _int32 _ret = YH_SUCCESS;

	YH_MALLOC(*loop, EventLoop *, sizeof(EventLoop));

    _ret = yh_iocp_create(&((*loop)->_iocp), 0);
    YH_CHECK_VALUE(_ret);

    _ret = yh_mq_init(&((*loop)->_queue));
    YH_CHECK_VALUE(_ret);

    return YH_SUCCESS;
}


_int32 yh_loop_run(EventLoop *loop)
{
    _int32 _ret = YH_SUCCESS;
	DWORD _last_err = 0;
    BOOL _iocp_ret = FALSE;
    DWORD _io_bytes = 0;
	ULONG _completion_key = 0;
	LPOVERLAPPED _overlapped = NULL;
	OverlappedWrapper *_op = NULL;

    YH_CHECK_PARAM_VALID(loop);

    while (loop->_stop_flag == 0) {
        _iocp_ret = GetQueuedCompletionStatus(loop->_iocp, &_io_bytes, 
			&_completion_key, &_overlapped, INFINITE);

		if (_iocp_ret) {
			_ret = yh_overlaped_to_overlapped_wrapper(_overlapped, &_op);
			YH_CHECK_VALUE(_ret);
            YH_ASSERT(_op != NULL);

			LOG("got a valide completion, io_bytes:%d, _overlapped:%p, type:%d."
				, _io_bytes, _overlapped, _op->_msg->_type);
			
			_op->_trans_bytes = _io_bytes;

            _ret = yh_loop_handle_overlappedop(loop, _op);
            YH_CHECK_VALUE(_ret);
		}
        else {
			_last_err = GetLastError();

			LOG("GetQueuedCompletionStatus return FALSE, errcode:%d.", 
				_last_err);

            if (_overlapped) {
				_ret = yh_overlaped_to_overlapped_wrapper(_overlapped, &_op);
				YH_CHECK_VALUE(_ret);
				YH_ASSERT(_op != NULL);

				LOG("got a valide completion, io_bytes:%d, _overlapped:%p, "
					"type:%d.", _io_bytes, _overlapped, _op->_msg->_type);

				_op->_err_code = _last_err;
				// 当有错误发生时，这里可能也会有部分数据的产生
				// 简单处理，就是不理会，直接关掉链接
				_op->_trans_bytes = _io_bytes;

                _ret = yh_loop_handle_overlappedop(loop, _op);
				YH_CHECK_VALUE(_ret);
            }
			else {
				YH_ASSERT(FALSE);
			}
        }
    }

	/**
	 * FIXME
	 * 这个销毁放到外面做可能更合适！
	 * 如果放在这里，销毁时需要判断loop是否真正停止，所以可能在destroy
	 * 之后还会用到loop，崩溃啊。。
	 **/
    LOG("loop stop running.");
    /*yh_loop_destroy(loop);*/

    return YH_SUCCESS;
}


_int32 yh_loop_destroy(EventLoop *loop)
{
	if (loop) {
		LOG("loop:0x%x.", loop);
        if (loop->_stop_flag == 0) {
            loop->_stop_flag = 1;
        }
		
        YH_ASSERT(loop->_stop_flag == 1);

        if (loop->_iocp) {
            CloseHandle(loop->_iocp);
            loop->_iocp = INVALID_HANDLE_VALUE;
        }

        if (loop->_queue) {
            yh_mq_uninit(loop->_queue);
            loop->_queue = NULL;
        }

        YH_DELETE_OBJ(loop);
	}

    return YH_SUCCESS;
}


_int32 yh_loop_is_running(EventLoop *loop, BOOL *running)
{
	YH_CHECK_PARAM_VALID(loop);
	YH_CHECK_PARAM_VALID(running);

	*running = (loop->_stop_flag == 1 ? FALSE : TRUE);

    return YH_SUCCESS;
}


_int32 yh_simulate_handle_async_op(EventLoop *loop,
								   OverlappedWrapper *op_wrapper)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(loop);
	YH_CHECK_PARAM_VALID(op_wrapper);

	LOG("op:0x%x, type:%d, handler:0x%x.", op_wrapper, op_wrapper->_msg->_type, 
		op_wrapper->_msg->_handler);

	_ret = yh_post_queued_completion_status(loop->_iocp, 0, 0, 
		&(op_wrapper->_overlapped));
	YH_CHECK_VALUE(_ret);

	return _ret;
}
