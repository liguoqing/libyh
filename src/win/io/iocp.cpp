#include "io/iocp.h"


_int32 yh_iocp_create(YHHandle *iocp, ULONG_PTR c_key)
{
    YH_CHECK_PARAM_VALID(iocp);

    *iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, c_key, 0);
    if (*iocp == NULL) {
		LOG("CreateIoCompletionPort failure, errcode:%d", GetLastError());
        return YH_ERR_CANOT_CREATE_IOCP;
    }

    return YH_SUCCESS;
}


_int32 yh_iocp_associate_fd(YHHandle iocp, YHHandle fd, ULONG_PTR c_key)
{
    YHHandle _tmp_iocp = NULL;
	int _api_ret = 0;

    YH_CHECK_HANDLE_VALID(iocp, YH_ERR_INVALID_PARAM);
	YH_CHECK_HANDLE_VALID(iocp, YH_ERR_INVALID_PARAM);

    // If this parameter is zero, the system allows as many concurrently 
	// running threads as there are processors in the system.
    _tmp_iocp = CreateIoCompletionPort(fd, iocp, c_key, 0);
    if (_tmp_iocp == NULL) {
		_api_ret = GetLastError();
		LOG("CreateIoCompletionPort failure, errcode %d", _api_ret);
        return YH_ERR_CANOT_CREATE_IOCP;
    }
    
    YH_ASSERT(_tmp_iocp == iocp);

    return YH_SUCCESS;
}


_int32 yh_get_queued_completion_status(YHHandle iocp, 
                                       LPDWORD num_of_bytes_ptr, 
                                       PULONG_PTR c_key,
                                       LPOVERLAPPED *overlapped_ptr,
                                       DWORD millisecond)
{
    BOOL _api_ret = FALSE;

    YH_CHECK_HANDLE_VALID(iocp, YH_ERR_INVALID_PARAM);

    _api_ret = GetQueuedCompletionStatus(iocp, num_of_bytes_ptr, c_key, 
		overlapped_ptr, millisecond);
    if (!_api_ret) {
		LOG("GetQueuedCompletionStatus failure, errcode %d", GetLastError());
        return YH_ERR_CANOT_GET_IOCP_STATUS;
    }

    return YH_SUCCESS;
}


_int32 yh_post_queued_completion_status(YHHandle iocp,
                                        DWORD num_of_bytes,
                                        ULONG_PTR c_key,
                                        LPOVERLAPPED overlapped_ptr)
{
    BOOL _api_ret = FALSE;

    if (iocp == INVALID_HANDLE_VALUE) return YH_ERR_INVALID_PARAM;

    _api_ret = PostQueuedCompletionStatus(iocp, num_of_bytes, c_key, 
		overlapped_ptr);
    if (!_api_ret) {
		LOG("PostQueuedCompletionStatus failure, errcode %d", GetLastError());
        return YH_ERR_CANOT_POST_IOCP_STATUS;
    }

    return YH_SUCCESS;
}


_int32 yh_create_overlapped_wrapper(OverlappedWrapper **o_wrapper, 
									MessageType type,
									yh_message_handler handler,
									yh_message_data_delete_handler d_handler,
									void *data)
{
    _int32 _ret = YH_SUCCESS;

	YH_ASSERT(type >= MT_UNKNOWN && type < MT_MAX);
    
    YH_MALLOC(*o_wrapper, OverlappedWrapper *, sizeof(OverlappedWrapper));

    _ret = yh_message_alloc(&((*o_wrapper)->_msg));
    if (_ret == YH_SUCCESS) {
        (*o_wrapper)->_msg->_type = type;
		(*o_wrapper)->_msg->_handler = handler;
		(*o_wrapper)->_msg->_delete_handler = d_handler;
        (*o_wrapper)->_msg->_data = data;
    }

    return _ret;
}


_int32 yh_destroy_overlapped_wrapper(OverlappedWrapper *o_wrapper)
{
    YH_CHECK_PARAM_VALID(o_wrapper);

    yh_message_delete(o_wrapper->_msg);
    YH_DELETE_OBJ(o_wrapper);

    return YH_SUCCESS;
}


_int32 yh_overlaped_to_overlapped_wrapper(OVERLAPPED *overlaped, 
										  OverlappedWrapper **o_wrapper)
{
    *o_wrapper = CONTAINING_RECORD(overlaped, OverlappedWrapper, _overlapped);
    return YH_SUCCESS;
}
