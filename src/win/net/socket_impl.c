#include "net/socket_interface.h"
#include "base/define.h"
#include "io/event_loop.h"
#include "net/winsock.h"


#define YH_CHECK_SOCKET_VALIDE(fd)                  \
    do {                                            \
        if (fd == INVALID_SOCKET)                   \
            return YH_ERR_SOCKET_FD_INVALID;        \
    } while(0)


_int32 yh_socket_startup()
{
    _int32 _ret = YH_SUCCESS;
    WSADATA _wsadata;

    _ret = WSAStartup(MAKEWORD(2,2), &_wsadata);
    YH_ASSERT(_ret == 0);

    if (_ret != 0) {
        LOG("[error]WSAStartup return value:%d.", _ret);
        _ret = YH_ERR_SOCKET_ENV_INVALID;
    }

	_ret = yh_init_wsock_env();
	if (_ret != 0) {
		LOG("[error]yh_init_wsock_env return error:%d.", _ret);
		_ret = YH_ERR_SOCKET_ENV_INVALID;
	}

    return _ret;
}


_int32 yh_socket_cleanup()
{
    _int32 _ret = YH_SUCCESS;

    _ret = WSACleanup();
    YH_ASSERT(_ret == 0);

    if (_ret != 0) {
        LOG("[error]WSACleanup return value:%d, err_code:%d.", _ret, 
			WSAGetLastError());
        _ret = YH_ERR_SOCKET_ENV_INVALID;
    }

    return _ret;
}


_int32 yh_socket_create(SocketType type, Socket **sock)
{
    _int32 _ret = YH_SUCCESS;
    int _type = SOCK_STREAM;
    int _protocol = IPPROTO_TCP;
	BOOL _reuse = TRUE;

	YH_ASSERT(*sock == NULL);

    switch (type) {
        case YH_TCP:
            _type = SOCK_STREAM;
            _protocol = IPPROTO_TCP;
            break;
        case YH_UDP:
            _type = SOCK_DGRAM;
            _protocol = IPPROTO_UDP;
            break;
        default:
            break;
    }

	*sock = (Socket *)malloc(sizeof(Socket));
	memset(*sock, 0, sizeof(Socket));

    (*sock)->_fd = WSASocket(AF_INET, _type, _protocol, NULL, 0, 
		WSA_FLAG_OVERLAPPED);
    if ((*sock)->_fd == INVALID_SOCKET) {
		// FIXME
		// 如果创建不成功，理应返回一个NULL
        _ret = YH_ERR_SOCKET_CREATE;
        LOG("[error]WSASocket create error, err_code:%d.", WSAGetLastError());
    }
    else {
		setsockopt((*sock)->_fd, SOL_SOCKET, SO_REUSEADDR, (LPCSTR)&_reuse, 
			sizeof(BOOL));

        LOG("Create new socket, fd:%d.", (*sock)->_fd);
    }

	(*sock)->_type = type;

    return _ret;
}


_int32 yh_socket_create_with_fd(SocketType type, YHSocket fd, Socket **sock)
{
	YH_ASSERT(fd != INVALID_SOCKET);

	*sock = (Socket *)malloc(sizeof(Socket));
	YH_CHECK_MALLOC_VALID(*sock);
	memset(*sock, 0, sizeof(Socket));

	(*sock)->_fd = fd;
	(*sock)->_type = type;

	return YH_SUCCESS;
}


_int32 yh_socket_connect(Socket *sock, const char *ip, _uint16 port)
{
    _int32 _ret = YH_SUCCESS;
    struct sockaddr_in _addr = {0};

    YH_CHECK_PARAM_VALID(sock);
    YH_CHECK_PARAM_VALID(ip);

    YH_CHECK_SOCKET_VALIDE(sock->_fd);
    
    _addr.sin_family = AF_INET;
    _addr.sin_addr.s_addr = inet_addr(ip);
    _addr.sin_port = htons(port);
    _ret = WSAConnect(sock->_fd, (struct sockaddr *)&_addr, sizeof(struct sockaddr), 
        NULL, NULL, NULL, NULL);
    if (_ret != 0) {
        LOG("[error]WSAConnect return error, err_code:%d.", WSAGetLastError());
        _ret = YH_ERR_SOCKET_OP_FAILURE;
    }
    else {
        LOG("Connect to %s:%d success.", ip, port);
    }

    return _ret;
}


_int32 yh_socket_listen(Socket *sock, const char *ip, _uint16 port)
{
    _int32 _ret = YH_SUCCESS;
    struct sockaddr_in _tmp_addr = {0};

    YH_CHECK_PARAM_VALID(sock);
    YH_CHECK_SOCKET_VALIDE(sock->_fd);

	_tmp_addr.sin_family = AF_INET;
	_tmp_addr.sin_port = htons(port);
	_tmp_addr.sin_addr.s_addr = inet_addr(ip);
	_ret = bind(sock->_fd, (struct sockaddr *)&_tmp_addr, sizeof(struct sockaddr));
	if (_ret == 0) {
		_ret = listen(sock->_fd, 5);
		if (_ret != 0) {
			LOG("[error]listen return error, err_code:%d.", WSAGetLastError());
			_ret = YH_ERR_SOCKET_OP_FAILURE;
		}
	}
	else {
		// TODO
		// 这里可以分析原因，换端口尝试绑定
		LOG("[error]bind return error, err_code:%d.", WSAGetLastError());
		_ret = YH_ERR_SOCKET_OP_FAILURE;
	}

    return _ret;
}


_int32 yh_socket_accept(Socket *sock, 
						Socket **sock_new,
						struct sockaddr_in *sock_new_addr)
{
    _int32 _ret = YH_SUCCESS;
    struct sockaddr_in _new_addr = {0};
    int _new_addr_len = sizeof(struct sockaddr_in);
    YHSocket _new_sock_fd = INVALID_SOCKET;

    YH_CHECK_PARAM_VALID(sock);
    YH_CHECK_SOCKET_VALIDE(sock->_fd);

    _new_sock_fd = accept(sock->_fd, (struct sockaddr *)&_new_addr, 
		&_new_addr_len);

    if (_new_sock_fd != INVALID_SOCKET) {
		YH_MALLOC(*sock_new, Socket *, sizeof(Socket));
        
        (*sock_new)->_fd = _new_sock_fd;
        (*sock_new)->_type = YH_TCP;

		memcpy(sock_new_addr, &_new_addr, sizeof(_new_addr));
    }
    else {
        LOG("[error]accept return error, err_code:%d.", WSAGetLastError());
        _ret = YH_ERR_SOCKET_OP_FAILURE;
        *sock_new = NULL;
    }

    return _ret;
}


_int32 yh_socket_close(Socket *sock)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(sock);

	if (sock->_fd != INVALID_SOCKET) {
		_ret = closesocket(sock->_fd);
		if (_ret != 0) {
			LOG("[error]closesocket return error, err_code:%d.", 
				WSAGetLastError());
			_ret = YH_ERR_SOCKET_OP_FAILURE;
		}
		sock->_fd = INVALID_SOCKET;	
	}    

	YH_DELETE_OBJ(sock);

    return _ret;
}


/**
 * 在socket上同步的发送数据
 * @param sock 
 * @param data
 * @param len
 * @return 错误码或者修改sock的_sock_send_bytes
 **/
static _int32 yh_socket_send_in_syn(Socket *sock, const char *data, _uint32 len)
{
	_int32 _ret = YH_SUCCESS;

	if (!data || (len == 0)) return _ret;

	_ret = send(sock->_fd, data, len, 0);
    if (_ret == SOCKET_ERROR) {
        _ret = YH_ERR_SOCKET_OP_FAILURE;

        LOG("socket:%d send error, err_code:%d.", sock->_fd, 
			WSAGetLastError());
    }
    else {
        _ret = YH_SUCCESS;

        LOG("socket:%d send success, upto %d.", sock->_fd, len);
    }

    return _ret;
}


/**
 * 在socket上异步的发送数据
 * @param sock 
 * @param data
 * @param len
 **/
static _int32 yh_socket_send_in_asyn(Socket *sock, 
                                     const char *data, _uint32 len)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(sock);
    YH_CHECK_PARAM_VALID(data);
    YH_ASSERT(len > 0);

    return _ret;
}


_int32 yh_socket_send(Socket *sock, const char *data, _uint32 len)
{
    _int32 _ret = YH_SUCCESS;
    
    YH_CHECK_PARAM_VALID(sock);
    YH_CHECK_PARAM_VALID(data);
    YH_CHECK_SOCKET_VALIDE(sock->_fd);

	if (!sock->_poll) {
		LOG("send in synchronously.");
		_ret = yh_socket_send_in_syn(sock, data, len);
	}
	else {
		LOG("send in asynchronously.");
		_ret = yh_socket_send_in_asyn(sock, data, len);
	}

	return _ret;
}


/**
 * @return 如果对方关闭，返回YH_SUCCESS，接收到的字节数为0
 **/
static _int32 yh_socket_recv_in_syn(Socket *sock, 
									void *buffer, 
									_uint32 len,
									_int32 *errcode)
{
	_int32 _ret = YH_SUCCESS;

    if (!buffer || (len == 0)) return _ret;

	*errcode = 0;

    _ret = recv(sock->_fd, buffer, len, 0);
    if (_ret == SOCKET_ERROR) {
		*errcode = WSAGetLastError();
		LOG("socket:%d recv error, err_code:%d.", sock->_fd, WSAGetLastError());
		_ret = YH_ERR_SOCKET_OP_FAILURE;        
    }
	else if (_ret == 0) {
		LOG("socket:%d closed.", sock->_fd);
		_ret = YH_ERR_TCP_CONN_RMT_CLOSED;
	}
    else {
		LOG("socket:%d recv success, upto:%d.", sock->_fd, len);
        _ret = YH_SUCCESS;        
    }

    return _ret;
}


static _int32 yh_socket_recv_in_asyn(Socket *sock, void *buffer, _uint32 len)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(sock);
    YH_CHECK_PARAM_VALID(buffer);
    YH_ASSERT(len > 0);

    return _ret;
}


_int32 yh_socket_recv(Socket *sock, char *buffer, _uint32 len, _int32 *errcode)
{
	_int32 _ret = YH_SUCCESS;
	_int32 _errcode = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(sock);
	YH_CHECK_PARAM_VALID(buffer);
	YH_CHECK_SOCKET_VALIDE(sock->_fd);

	if (!sock->_poll) {
		LOG("recv in synchronously.");
		_ret = yh_socket_recv_in_syn(sock, buffer, len, &_errcode);
	}
	else {
		LOG("recv in asynchronously.");
		_ret = yh_socket_recv_in_asyn(sock, buffer, len);
	}

	*errcode = _errcode;

	return _ret;
}
