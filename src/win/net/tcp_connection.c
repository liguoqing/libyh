#include "net/tcp_connection.h"


struct ConnectionOpenUserData_s {
    TcpConnection                       *_conn;
	yh_tcp_connection_open_callback		_callback;
};


struct ConnectionSendUserData_s {
    TcpConnection                       *_conn;
    yh_tcp_connection_send_callback     _callback;
};

struct ConnectionRecvUserData_s {
    TcpConnection                       *_conn;
    yh_tcp_connection_recv_callback     _callback;
};

struct ConnectionCloseUserData_s {
    TcpConnection                       *_conn;
    yh_tcp_connection_close_callback    _callback;
};


typedef struct ConnectionOpenUserData_s ConnOpenUserData;
typedef struct ConnectionSendUserData_s ConnSendUserData;
typedef struct ConnectionRecvUserData_s ConnRecvUserData;
typedef struct ConnectionCloseUserData_s ConnCloseUserData;


static _int32 _yh_tcp_connection_handle_connect(SockConnReq *req);
static _int32 _yh_tcp_connection_handle_send(SockSendReq *req);
static _int32 _yh_tcp_connection_handle_recv(SockRecvReq *req);
static _int32 _yh_tcp_connection_handle_close(SockClosReq *req);


_int32 yh_tcp_connection_create(struct EventLoop_s *loop, 
                                TcpDevice *device,
                                void *user_data,
								TcpConnection **conn)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(loop);

	YH_MALLOC(*conn, TcpConnection *, sizeof(TcpConnection));
	/**
	 * [NOTE][TODO]
	 * 一条连接64K固定的缓存，有必要吗？真心没必要
	 * 在多连接的时候，这地方需要好好想想！
	 **/
	YH_MALLOC((*conn)->_recv_buf, char *, DEFAULT_CONNECTION_RECV_BUF_SIZE);

    if (!device) {
	    _ret = yh_tcp_device_create(loop, INVALID_SOCKET, &((*conn)->_device));
	    YH_CHECK_VALUE(_ret);
    }
    else {
        (*conn)->_device = device;
    }

	(*conn)->_user_data = user_data;

	YH_TCP_CONN_ENTER_STATE((*conn), TCS_IDLE);

	return _ret;
}


_int32 yh_tcp_connection_destroy(TcpConnection *conn)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(conn);
	YH_ASSERT(conn->_state == TCS_CLOSED ||	conn->_state == TCS_FAILED);
	
	_ret = yh_tcp_device_destroy(conn->_device);
	YH_CHECK_VALUE(_ret);

	YH_DELETE_OBJ(conn->_recv_buf);
	YH_DELETE_OBJ(conn);

	return _ret;
}


_int32 yh_tcp_connection_open(TcpConnection *conn,
							  const struct sockaddr_in *addr, 
							  _uint32 addr_len,
							  yh_tcp_connection_open_callback callback)
{
	_int32 _ret = YH_SUCCESS;
	ConnOpenUserData *_user_data = NULL;

	YH_CHECK_PARAM_VALID(conn);
	YH_CHECK_PARAM_VALID(addr);
	YH_CHECK_PARAM_VALID(callback);
	YH_ASSERT(addr_len > 0);

	YH_MALLOC(_user_data, ConnOpenUserData *, sizeof(ConnOpenUserData));
	_user_data->_callback = callback;
    _user_data->_conn = conn;

	_ret = yh_tcp_device_connect(conn->_device, addr, addr_len, 
		(void *)_user_data, _yh_tcp_connection_handle_connect);
	YH_CHECK_VALUE(_ret);

	YH_TCP_CONN_ENTER_STATE(conn, TCS_CONNECTING);

	return _ret;
}


_int32 yh_tcp_connection_close(TcpConnection *conn,
                               yh_tcp_connection_close_callback callback)
{
    _int32 _ret = YH_SUCCESS;
    ConnCloseUserData *_user_data = NULL;

    YH_CHECK_PARAM_VALID(conn);
    YH_CHECK_PARAM_VALID(callback);

    YH_MALLOC(_user_data, ConnCloseUserData *, sizeof(ConnCloseUserData));
    _user_data->_callback = callback;
    _user_data->_conn = conn;

    _ret = yh_tcp_device_close(conn->_device, _user_data, 
        _yh_tcp_connection_handle_close);
    YH_CHECK_VALUE(_ret);

    YH_TCP_CONN_ENTER_STATE(conn, TCS_CLOSING);

    return _ret;
}


/**
 * 这里比较关键，需要确保缓冲区中的数据发送完整
 **/
_int32 yh_tcp_connection_send(TcpConnection *conn,
                              char *data, 
							  _uint32 data_len, 
                              yh_tcp_connection_send_callback callback)
{
    _int32 _ret = YH_SUCCESS;
    ConnSendUserData *_user_data = NULL;

    YH_CHECK_PARAM_VALID(conn);
    YH_CHECK_PARAM_VALID(data);
    YH_CHECK_PARAM_VALID(callback);
    YH_ASSERT(data_len > 0);

	conn->_send_buf = data;
	conn->_exp_send_len = data_len;

    YH_MALLOC(_user_data, ConnSendUserData *, sizeof(ConnSendUserData));
    _user_data->_callback = callback;
    _user_data->_conn = conn;

	conn->_device->_send_buf = conn->_send_buf;
	conn->_device->_expect_send_len = conn->_exp_send_len;
    
	_ret = yh_tcp_device_send(conn->_device, conn->_send_buf, 
		conn->_exp_send_len, (void *)_user_data, 
		_yh_tcp_connection_handle_send);
	YH_CHECK_VALUE(_ret);
    
    return _ret;
}


_int32 yh_tcp_connection_recv(TcpConnection *conn,
							  _uint32 exp_recv_len,
							  yh_tcp_connection_recv_callback callback)
{
	_int32 _ret = YH_SUCCESS;
	ConnRecvUserData *_user_data = NULL;

	YH_CHECK_PARAM_VALID(conn);
	YH_CHECK_PARAM_VALID(callback);
	YH_ASSERT(exp_recv_len > 0);

	conn->_exp_recv_len = exp_recv_len;

	YH_MALLOC(_user_data, ConnRecvUserData *, sizeof(ConnRecvUserData));
	_user_data->_callback = callback;
	_user_data->_conn = conn;

	_ret = yh_tcp_device_recv(conn->_device, conn->_recv_buf, 
		conn->_exp_recv_len, (void *)_user_data, 
		_yh_tcp_connection_handle_recv);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 _yh_tcp_connection_handle_connect(SockConnReq *req)
{
    _int32 _ret = YH_SUCCESS;
    ConnOpenUserData *_user_data = NULL;
    
    YH_CHECK_PARAM_VALID(req);
    YH_ASSERT(req->_base->_user_data != NULL);

    if (req->_base->_err_code != YH_SUCCESS) {
        LOG("connect fail, errcode:%d.", req->_base->_err_code);
    }

    _user_data = (ConnOpenUserData *)req->_base->_user_data;
    
    _ret = _user_data->_callback(_user_data->_conn->_user_data, 
        req->_base->_err_code);

	YH_DELETE_OBJ(_user_data);

    return _ret;
}


static _int32 _yh_tcp_connection_handle_send(SockSendReq *req)
{
	_int32 _ret = YH_SUCCESS;
	ConnSendUserData *_user_data = NULL;

	YH_CHECK_PARAM_VALID(req);
	YH_ASSERT(req->_base->_user_data != NULL);

	if (req->_base->_err_code != YH_SUCCESS) {
		LOG("send fail, errcode:%d.", req->_base->_err_code);
	}

	_user_data = (ConnSendUserData *)req->_base->_user_data;

	_ret = _user_data->_callback(_user_data->_conn->_user_data, 
		req->_base->_err_code);

	YH_DELETE_OBJ(_user_data);

	return _ret;
}


/**
 * 这里要等待收到的长度跟你期望接收的长度一致方才往上回调
 **/
static _int32 _yh_tcp_connection_handle_recv(SockRecvReq *req)
{
	_int32 _ret = YH_SUCCESS;
	ConnRecvUserData *_user_data = NULL;
	TcpDevice *_device = NULL;

	YH_CHECK_PARAM_VALID(req);
	YH_ASSERT(req->_base->_user_data != NULL);

	_user_data = (ConnRecvUserData *)req->_base->_user_data;
	_device = (TcpDevice *)req->_base->_device;

	if (req->_base->_err_code == YH_SUCCESS) {
		LOG("recvd:%d.", req->_recvd_bytes);

        if (req->_recvd_bytes == 0) {
            LOG("remote close connection.");
            req->_base->_err_code = YH_ERR_TCP_CONN_RMT_CLOSED;
        }
		else if (req->_recvd_bytes != _user_data->_conn->_exp_recv_len) {
			LOG("recved uncompleted.");

            _ret = yh_tcp_device_recv(_device, 
				_user_data->_conn->_recv_buf + req->_recvd_bytes, 
				_user_data->_conn->_exp_recv_len - req->_recvd_bytes,
				_user_data, _yh_tcp_connection_handle_recv);
            YH_CHECK_VALUE(_ret);

			_user_data->_conn->_exp_recv_len -= req->_recvd_bytes;

            return _ret;
		}
		else {
			LOG("recvd completed.");
		}
	}

	_ret = _user_data->_callback(_user_data->_conn->_user_data, 
		req->_base->_err_code);
	YH_CHECK_VALUE(_ret);

	YH_DELETE_OBJ(_user_data);

	return _ret;
}


static _int32 _yh_tcp_connection_handle_close(SockClosReq *req)
{
    _int32 _ret = YH_SUCCESS;
    ConnCloseUserData *_user_data = NULL;
    TcpConnection *_conn = NULL;

    YH_CHECK_PARAM_VALID(req);
    YH_ASSERT(req->_base->_user_data != NULL);

    LOG("req 0x%x, errcode %d.", req, req->_base->_err_code);

    _user_data = (ConnCloseUserData *)req->_base->_user_data;
    YH_ASSERT(_user_data != NULL);

    _conn = _user_data->_conn;
    YH_ASSERT(_conn != NULL);

    if (req->_base->_err_code != YH_SUCCESS) {
        LOG("close fail, errcode:%d.", req->_base->_err_code);

        _conn->_state = TCS_FAILED;
    }
    else {
        _conn->_state = TCS_CLOSED;
    }

    _ret = _user_data->_callback(_user_data->_conn->_user_data, 
        req->_base->_err_code);

	YH_DELETE_OBJ(_user_data);

    return _ret;
}
