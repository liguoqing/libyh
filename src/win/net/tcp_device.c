#include "net/tcp_device.h"
#include "base/singnal.h"
#include "io/io_request.h"
#include "net/socket_utility.h"


/**
 * NOTE
 * 各种socket操作在消息系统中的回调，与应用层获取到的回调有区别
 * 所以回调的路线是这样的：
 * 应用层提交request --> tcp_device --> iocp --> tcp_device --> 应用层回调
 * 这样设计的目的是iocp向上回调时的接口统一，这里的void *都是投递下去的
 * op，而tcp_device向上回调的接口可以定制
 **/
static _int32 _yh_tcp_device_handle_connect(void *data);
static _int32 _yh_tcp_device_handle_accept(void *data);
static _int32 _yh_tcp_device_handle_send(void *data);
static _int32 _yh_tcp_device_handle_recv(void *data);
static _int32 _yh_tcp_device_handle_close(void *data);
static _int32 yh_tcp_device_handle_disconnect(void *data);


#define TCP_DEVICE_ENTER_STATE(device, state)	device->_state = state


static _int32 yh_tcp_device_get_dumy_local_port(_uint16 *port) 
{
    static _uint16 _port = (_uint16)DUMY_LOCAL_TCP_PORT;

	// [TODO]
    // 确保线程安全性
    *port = _port+10;

    return YH_SUCCESS;
}


_int32 yh_tcp_device_create(EventLoop *loop, YHSocket fd, TcpDevice **device)
{
    _int32 _ret = YH_SUCCESS;
	Socket *_sock = NULL;

	YH_CHECK_PARAM_VALID(loop);

	LOG("loop: 0x%x, fd: %d", loop, fd);
	
	YH_MALLOC(*device, TcpDevice *, sizeof(TcpDevice));

	if (fd == INVALID_SOCKET) {
		_ret = yh_socket_create(YH_TCP, &_sock);
		YH_CHECK_VALUE(_ret);
	}
	else {
		_ret = yh_socket_create_with_fd(YH_TCP, fd, &_sock);
		YH_CHECK_VALUE(_ret);
	}	
	YH_ASSERT(_sock != NULL);
	YH_ASSERT(_sock->_fd != INVALID_SOCKET);

	_ret = yh_device_create(loop, (YHHandle)_sock->_fd, TCP_DEVICE,
		&((*device)->_device));
	YH_CHECK_VALUE(_ret);
	YH_ASSERT((*device)->_device != NULL);

    _ret = yh_queue_create(&((*device)->_pending_reqs));
	YH_CHECK_VALUE(_ret);
	YH_ASSERT((*device)->_pending_reqs != NULL);

	(*device)->_sock = _sock;
    (*device)->_wait_close = CreateEvent(NULL, FALSE, FALSE, NULL);
    (*device)->_state = TDS_IDLE;

    return _ret;
}


_int32 yh_tcp_device_destroy(TcpDevice *device)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(device);

	if (device->_state != TDS_CLOSED && device->_state != TDS_CLOSING) {
		LOG("[error] wrong state of tcp device, it is: %d", device->_state);
		return YH_ERR_TCP_DEVICE_WRONG_STATE;
	}

	// [TODO]
	// 需要确认device上是否还有待执行的操作
	if (device->_state == TDS_CLOSING) {
		LOG("waiting for tcp device stop.");
		_ret = yh_wait(device->_wait_close);
	}

	CloseHandle(device->_wait_close);
	device->_wait_close = INVALID_HANDLE_VALUE;

	_ret = yh_queue_destroy(device->_pending_reqs);
	YH_CHECK_VALUE(_ret);

	_ret = yh_device_destroy(device->_device);
	YH_CHECK_VALUE(_ret);

	_ret = yh_socket_close(device->_sock);
	YH_CHECK_VALUE(_ret);

	YH_DELETE_OBJ(device);

	return _ret;
}


_int32 yh_tcp_device_reset_socket(TcpDevice *device)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(device);
	YH_ASSERT(device->_state != TDS_TRANSMITED);

    _ret = yh_socket_close(device->_sock);
    YH_CHECK_VALUE(_ret);

    device->_sock = NULL;

    _ret = yh_socket_create(YH_TCP, &(device->_sock));
    YH_CHECK_VALUE(_ret);

    _ret = yh_device_reset_fd(device->_device, 
		(YHHandle)device->_sock->_fd);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


_int32 yh_tcp_device_connect(TcpDevice *device, 
							 const struct sockaddr_in *addr, 
							 _uint32 addr_len, 
							 void *user_data,
							 yh_tcp_device_connect_callback callback)
{
    _int32 _ret = YH_SUCCESS;
	MSWSockApi _wsock_api_table = {0};
	BOOL _api_ret = FALSE;
	int _wsa_last_err = 0;
	OverlappedWrapper *_op = NULL;
	SockConnReq *_req = NULL;
	struct sockaddr_in _local_addr = {0};
    _uint16 _dumy_local_port = 0;

    YH_CHECK_PARAM_VALID(device);
	YH_CHECK_PARAM_VALID(addr);
	YH_CHECK_PARAM_VALID(callback);

    YH_ASSERT(device->_state == TDS_IDLE);
    
	_ret = yh_get_wsock_api_table(&_wsock_api_table);
	YH_CHECK_VALUE(_ret);

	_ret = yh_create_sock_conn_req(&_op, _yh_tcp_device_handle_connect, 
		(IODevice *)device, (void *)callback, addr, addr_len, user_data, &_req);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_op != NULL);
	YH_ASSERT(_req != NULL);
	YH_ASSERT(_op->_msg->_data == _req);
	YH_ASSERT(_req->_base->_device == (IODevice *)device);
	YH_ASSERT(_req->_base->_user_data == user_data);
	YH_ASSERT(_req->_base->_op == _op);

	// NOTE:
	// 1, bind之后竟然会直接监听在本端口上？--出现问题后验证得知是这样的
	// 2, ConnectEx需要bind一次，根本原因是啥？
	_ret = yh_socket_ip_str2ulong("127.0.0.1", &(_local_addr.sin_addr.s_addr));
	YH_CHECK_VALUE(_ret);

	_ret = yh_tcp_device_get_dumy_local_port(&_dumy_local_port);
	YH_CHECK_VALUE(_ret);

	_local_addr.sin_family = AF_INET;
	_local_addr.sin_port = htons(_dumy_local_port);

	_ret = bind(device->_sock->_fd, (SOCKADDR *)&_local_addr, 
		sizeof(_local_addr));
	if (_ret == SOCKET_ERROR) {
		// TODO: do clean then return
		return YH_ERR_TCP_DEVICE_INNER_FAILURE;
	}

	_api_ret = _wsock_api_table._connectex(device->_sock->_fd,
		(SOCKADDR *)addr, addr_len, NULL, 0, NULL, &(_op->_overlapped));
	if (!_api_ret) {
		_wsa_last_err = WSAGetLastError();
		if (_wsa_last_err != ERROR_IO_PENDING) {
			_op->_err_code = WSAGetLastError();
			_ret = yh_simulate_handle_async_op(device->_device->loop, _op);
			YH_CHECK_VALUE(_ret);

			_ret = YH_ERR_SOCKET_OP_FAILURE;
		}
	}

    if (_ret == YH_SUCCESS) {
        TCP_DEVICE_ENTER_STATE(device, TDS_CONNECTING);
    }

    return _ret;
}


_int32 yh_tcp_device_reconnect(TcpDevice *device, SockConnReq *req)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(device);
    YH_CHECK_PARAM_VALID(req);

    YH_ASSERT(device->_state == TDS_CONNECT_ERR);

	// [TODO]
	// 重连后续再完善
    YH_ASSERT(FALSE);

    return _ret;
}


_int32 yh_tcp_device_listen(TcpDevice *device, 
							const struct sockaddr *addr,
                            _uint32 addr_len, 
							_int32 backlog, 
							_int32 *err_code)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(device);
	YH_ASSERT(addr != NULL);
	YH_ASSERT(addr_len != 0);

    *err_code = 0;

    if (device->_state != TDS_IDLE) {
        YH_ASSERT(FALSE);
        return YH_ERR_TCP_DEVICE_WRONG_STATE;
    }

    _ret = bind(device->_sock->_fd, addr, addr_len);
    if (_ret == SOCKET_ERROR) {
        *err_code = WSAGetLastError();
        return YH_ERR_TCP_DEVICE_INNER_FAILURE;
    }

    _ret = listen(device->_sock->_fd, backlog);
    if (_ret == SOCKET_ERROR) {
        *err_code = WSAGetLastError();
        return YH_ERR_TCP_DEVICE_INNER_FAILURE;
    }

    device->_state = TDS_LISTENING;

    return _ret;
}


_int32 yh_tcp_device_accept(TcpDevice *device, 
                            yh_tcp_device_accept_callback callback)
{
    _int32 _ret = YH_SUCCESS;
    MSWSockApi _wsock_api_table = {0};
    BOOL _api_ret = FALSE;
    OverlappedWrapper *_op = NULL;
	SockAcceReq *_req = NULL;
    DWORD _acc_bytes = 0;

    YH_CHECK_PARAM_VALID(device);
	YH_CHECK_PARAM_VALID(callback);
    
    YH_ASSERT(device->_state == TDS_LISTENING || 
		device->_state == TDS_ACCEPTED);

    _ret = yh_get_wsock_api_table(&_wsock_api_table);
    YH_CHECK_VALUE(_ret);

	_ret = yh_create_sock_acce_req(&_op, _yh_tcp_device_handle_accept, 
		(IODevice *)device, (void *)callback, &_req);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_op != NULL);
	YH_ASSERT(_req != NULL);

    _api_ret = _wsock_api_table._acceptex(device->_sock->_fd,
        _req->_acc_sock_fd, _req->_buffer, 0, sizeof(struct sockaddr_in) + 16,
        sizeof(struct sockaddr_in) + 16, &_acc_bytes, &_op->_overlapped);
    if (_api_ret == SOCKET_ERROR) {
        if (WSAGetLastError() != ERROR_IO_PENDING) {
			_op->_err_code = WSAGetLastError();
			_ret = yh_simulate_handle_async_op(device->_device->loop, _op);
			YH_CHECK_VALUE(_ret);

            _ret = YH_ERR_SOCKET_OP_FAILURE;
        }
    }

    return _ret;
}


_int32 yh_tcp_device_send(TcpDevice *device, 
						  char *data,
						  _uint32 len,
						  void *user_data,
                          yh_tcp_device_send_callback callback)
{
	_int32 _ret = YH_SUCCESS;
	int _api_ret = FALSE;
	OverlappedWrapper *_op = NULL;
	SockSendReq *_req = NULL;
	WSABUF _data_buf = {0};
	DWORD _sent_bytes = 0;

	YH_CHECK_PARAM_VALID(device);
	YH_CHECK_PARAM_VALID(data);
	YH_ASSERT(len > 0);

    YH_ASSERT(device->_state == TDS_CONNECTED || 
		device->_state == TDS_TRANSMITED);

	device->_send_buf = data;
	device->_expect_send_len = len;

	_ret = yh_create_sock_send_req(&_op, _yh_tcp_device_handle_send, 
		(IODevice *)device, (void *)callback, user_data, &_req);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_op != NULL);
	YH_ASSERT(_req != NULL);

	_data_buf.buf = device->_send_buf;
	_data_buf.len = device->_expect_send_len;

	_api_ret = WSASend(device->_sock->_fd, &_data_buf, 1, &_sent_bytes, 
		0, &(_op->_overlapped), NULL);
	if (_api_ret == SOCKET_ERROR) {
		if (WSAGetLastError() != ERROR_IO_PENDING) {			
			_op->_err_code = WSAGetLastError();
			_ret = yh_simulate_handle_async_op(device->_device->loop, _op);
			YH_CHECK_VALUE(_ret);

			_ret = YH_ERR_SOCKET_OP_FAILURE;
		}
	}

	// 成功有两种情况：
	// 1, _api_ret == 0，此时_sent_bytes != 0；这个结果这里不要，只等待iocp
	// 的通知；
	// 2, _api_ret == SOCKET_ERROR，此时WSAGetLastError() == ERROR_IO_PENDING；
	if (_ret == YH_SUCCESS) {
		LOG("WSASend ret:%d, WSAGetLastError() :%d, send %d bytes.", 
			_api_ret, WSAGetLastError(), _sent_bytes);
		device->_state = TDS_TRANSMITED;
	}

	return _ret;
}


_int32 yh_tcp_device_recv(TcpDevice *device, 
						  char *buffer,
						  _uint32 exp_recv_len,
						  void *user_data,
                          yh_tcp_device_recv_callback callback)
{
	_int32 _ret = YH_SUCCESS;
	int _api_ret = 0;
	OverlappedWrapper *_op = NULL;
	SockRecvReq *_req = NULL;
	WSABUF _data_buf = {0};
	DWORD _recv_bytes = 0, _flags = 0;

	YH_CHECK_PARAM_VALID(device);
	YH_CHECK_PARAM_VALID(buffer);
	YH_CHECK_PARAM_VALID(callback);

	YH_ASSERT(device->_state == TDS_IDLE || device->_state == TDS_CONNECTED || 
		device->_state == TDS_TRANSMITED);

	device->_recv_buf = buffer;
	device->_expect_recv_len = exp_recv_len;

	_ret = yh_create_sock_recv_req(&_op, _yh_tcp_device_handle_recv, 
		(IODevice *)device, (void *)callback, user_data, &_req);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_op != NULL);
	YH_ASSERT(_req != NULL);

	_data_buf.buf = device->_recv_buf;
	_data_buf.len = device->_expect_recv_len;

	_api_ret = WSARecv(device->_sock->_fd, &_data_buf, 1, &_recv_bytes, 
		&_flags, &(_op->_overlapped), NULL);
	if (_api_ret == SOCKET_ERROR) {
		if (WSAGetLastError() != ERROR_IO_PENDING) {
			_op->_err_code = WSAGetLastError();
			_ret = yh_simulate_handle_async_op(device->_device->loop, _op);
			YH_CHECK_VALUE(_ret);

			_ret = YH_ERR_SOCKET_OP_FAILURE;
		}
	}

	if (_ret == YH_SUCCESS) {
		LOG("WSARecv ret:%d, WSAGetLastError() :%d, recv %d bytes.", 
			_api_ret, WSAGetLastError(), _recv_bytes);
		device->_state = TDS_TRANSMITED;
	}

	return _ret;
}


_int32 yh_tcp_device_close(TcpDevice *device, 
                           void *user_data,
                           yh_tcp_device_close_callback callback)
{
	_int32 _ret = YH_SUCCESS;
	OverlappedWrapper *_op = NULL;
	SockClosReq *_req = NULL;

	YH_CHECK_PARAM_VALID(device);
	YH_CHECK_PARAM_VALID(callback);

	_ret = yh_create_sock_clos_req(&_op, _yh_tcp_device_handle_close, 
		(IODevice *)device, (void *)callback, user_data, &_req);
	YH_CHECK_VALUE(_ret);

	// TODO
	// 这里使用硬关闭
	// close socket gracefully
	// @see http://msdn.microsoft.com/en-us/library/ms738547(v=vs.85).aspx
	_ret = closesocket(device->_sock->_fd);
	if (_ret == SOCKET_ERROR) {
		_req->_base->_err_code = WSAGetLastError();
		_ret = YH_ERR_SOCKET_OP_FAILURE;		
	}

	device->_sock->_fd = INVALID_SOCKET;

	/**
	 * 同步关闭，这里模拟异步回调
	 **/
	_ret = yh_simulate_handle_async_op(device->_device->loop, _op);
	YH_CHECK_VALUE(_ret);

	if (_ret == YH_SUCCESS) {
		device->_state = TDS_CLOSING;
	}

    return YH_SUCCESS;
}


static _int32 _yh_tcp_device_handle_connect(void *data)
{
	_int32 _ret = YH_SUCCESS;
    OverlappedWrapper *_op = (OverlappedWrapper *)data;
	SockConnReq *_req = NULL;
    TcpDevice *_device = NULL;
	yh_tcp_device_connect_callback _callback = NULL;
	
	YH_CHECK_PARAM_VALID(data);

	_req = (SockConnReq *)_op->_msg->_data;
    _device = (TcpDevice *)_req->_base->_device;
	_req->_base->_err_code = _op->_err_code;
	/**
	 * NOTE:
	 * 真正的_op内容会被事件循环处理完后释放，所以后续不能再继续使用
	 * 所以所有的外部引用都应该失效，故此这里置空，避免外部再次引用
	 * 当然，理论上_op是一个内部使用结构，不应该对外暴露！
	 **/
	_req->_base->_op = NULL;

    if (_op->_err_code == YH_SUCCESS) {
        TCP_DEVICE_ENTER_STATE(_device, TDS_CONNECTED);
    }
    else {
		// [TODO]
		// 重连会有问题！
		YH_ASSERT(FALSE); 
        // 需要重连，那么本地的socket就需要重用
        // 通常都会有问题，所以重建一个
        _ret = yh_tcp_device_reset_socket(_device);
        YH_CHECK_VALUE(_ret);

        TCP_DEVICE_ENTER_STATE(_device, TDS_CONNECT_ERR);
    }

	LOG("_req:0x%x, errcode:%d.", _req, _req->_base->_err_code);

	_callback = (yh_tcp_device_connect_callback)_req->_base->_callback;
	YH_ASSERT(_callback != NULL);

	_ret = _callback(_req);

	_ret = yh_destroy_sock_conn_req(_req);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 _yh_tcp_device_handle_accept(void *data)
{
    _int32 _ret = YH_SUCCESS;
	OverlappedWrapper *_op = (OverlappedWrapper *)data;
    SockAcceReq *_req = NULL;
    TcpDevice *_device = NULL;
	yh_tcp_device_accept_callback _callback = NULL;

    YH_CHECK_PARAM_VALID(data);

	_req = (SockAcceReq *)_op->_msg->_data;
    _device = (TcpDevice *)(_req->_base->_device);
	_req->_base->_err_code = _op->_err_code;
	_req->_base->_op = NULL;

    if (_op->_err_code == YH_SUCCESS) {
        _device->_state = TDS_ACCEPTED;
    }
	else {
		_device->_state = TDS_FAILED;
	}

	LOG("_req:0x%x, errcode:%d.", _req, _req->_base->_err_code);
	
	_callback = (yh_tcp_device_accept_callback)_req->_base->_callback;
	YH_ASSERT(_callback != NULL);

	_ret = _callback(_req);

	_ret = yh_destroy_sock_acce_req(_req);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 _yh_tcp_device_handle_send(void *data)
{
    _int32 _ret = YH_SUCCESS;
	OverlappedWrapper *_op = (OverlappedWrapper *)data;
	SockSendReq *_req = NULL;
    TcpDevice *_device = NULL;
	yh_tcp_device_send_callback _callback = NULL;

	YH_CHECK_PARAM_VALID(data);

	_req = (SockSendReq *)_op->_msg->_data;
    _device = (TcpDevice *)_req->_base->_device;
	_req->_base->_err_code = _op->_err_code;
	_req->_base->_op = NULL;

	LOG("_req:0x%x, errcode:%d, send:%d.", 
		_req, _req->_base->_err_code, _op->_trans_bytes);

    if (_op->_err_code == YH_SUCCESS) {
        _device->_state = TDS_TRANSMITED;
	}
	else {
		_device->_state = TDS_FAILED;
	}

	YH_ASSERT(_op->_trans_bytes == _device->_expect_send_len);

	_callback = (yh_tcp_device_send_callback)_req->_base->_callback;
	YH_ASSERT(_callback != NULL);

	_ret = _callback(_req);

	_ret = yh_destroy_sock_send_req(_req);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 _yh_tcp_device_handle_recv(void *data)
{
    _int32 _ret = YH_SUCCESS;
	OverlappedWrapper *_op = (OverlappedWrapper *)data;
	SockRecvReq *_req = NULL;
    TcpDevice *_device = NULL;
	yh_tcp_device_recv_callback _callback = NULL;

	YH_CHECK_PARAM_VALID(data);

	_req = (SockRecvReq *)_op->_msg->_data;
    _device = (TcpDevice *)_req->_base->_device;
	_req->_base->_err_code = _op->_err_code;
	_req->_recvd_bytes = _op->_trans_bytes;
	_req->_base->_op = NULL;

	LOG("_req:0x%x, errcode:%d, recvd:%d.", 
		_req, _req->_base->_err_code, _op->_trans_bytes);

    if (_op->_err_code == YH_SUCCESS) {
		if (_op->_trans_bytes == 0) {
			_req->_base->_err_code = YH_ERR_TCP_CONN_RMT_CLOSED;
			_device->_state = TDS_HALF_CLOSED;
		}
		else {
			_device->_state = TDS_TRANSMITED;
		}
	}
	else {
		_device->_state = TDS_FAILED;
	}

	_callback = (yh_tcp_device_recv_callback)_req->_base->_callback;
	YH_ASSERT(_callback != NULL);

	_ret = _callback(_req);
	
	_ret = yh_destroy_sock_recv_req(_req);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 _yh_tcp_device_handle_close(void *data)
{
    _int32 _ret = YH_SUCCESS;
	OverlappedWrapper *_op = (OverlappedWrapper *)data;
	SockClosReq *_req = NULL;
    TcpDevice *_device = NULL;
	yh_tcp_device_close_callback _callback = NULL;

	YH_CHECK_PARAM_VALID(data);

	_req = (SockClosReq *)_op->_msg->_data;
    _device = (TcpDevice *)_req->_base->_device;
	_req->_base->_op = NULL;
	_req->_base->_err_code = _op->_err_code;

	LOG("_req:0x%x, errcode:%d.", _req, _req->_base->_err_code);

    if (_op->_err_code == YH_SUCCESS) {
        _device->_state = TDS_CLOSED;
	}
	else {
		_device->_state = TDS_FAILED;
	}

    _ret = yh_signal(_device->_wait_close);

	_callback = (yh_tcp_device_close_callback)_req->_base->_callback;
	YH_ASSERT(_callback != NULL);

	_ret = _callback(_req);

	_ret = yh_destroy_sock_clos_req(_req);
	YH_CHECK_VALUE(_ret);

	return _ret;
}


/**
 * [TODO]
 **/
static _int32 yh_tcp_device_handle_disconnect(void *data)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(data);

	return _ret;
}
