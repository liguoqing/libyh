#include "net/winsock.h"


/**
 * ������չsocket api
 **/
static _int32 yh_load_mswsockapi(MSWSockApi *mswsock_api);
static _int32 yh_unload_mswsockapi(MSWSockApi *mswsock_api);
static _int32 yh_get_extension_wsock_api(SOCKET sock, const GUID *guid, 
										 DWORD guid_size, LPVOID fn, 
										 DWORD fn_size, DWORD *bytes, 
										 int *errcode);


static BOOL gMSWsockEnvInit = FALSE;
static MSWSockApi gMSWsockApi = {0};


_int32 yh_init_wsock_env()
{
	_int32 _ret = yh_load_mswsockapi(&gMSWsockApi);
	YH_CHECK_VALUE(_ret);

	gMSWsockEnvInit = TRUE;

	return _ret;
}


_int32 yh_unit_wsock_env()
{
	return YH_SUCCESS;
}


static _int32 yh_get_extension_wsock_api(SOCKET sock, const GUID *guid, 
										 DWORD guid_size, LPVOID fn, 
										 DWORD fn_size, DWORD *bytes, 
										 int *errcode)
{
	_int32 _ret = YH_SUCCESS;

	YH_ASSERT(sock != INVALID_SOCKET);

	_ret = WSAIoctl(sock, SIO_GET_EXTENSION_FUNCTION_POINTER, (LPVOID)guid, 
		guid_size, fn, fn_size, bytes, NULL, NULL);

	if (_ret != 0) {
		*errcode = WSAGetLastError();
		_ret = YH_ERR_SOCKET_OP_FAILURE;
	}

	return _ret;
}


static _int32 yh_load_mswsockapi(MSWSockApi *mswsock_api)
{
	_int32 _ret = YH_SUCCESS;
	int _api_ret = 0;
	DWORD _bytes = 0;
	const GUID guid_connex = WSAID_CONNECTEX;
	const GUID guid_acceptex = WSAID_ACCEPTEX;
	const GUID guid_disconex = WSAID_DISCONNECTEX;

	/* Dummy socket needed for WSAIoctl */
	YHSocket sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)
		return YH_ERR_SOCKET_ENV_INVALID;

	_ret = yh_get_extension_wsock_api(sock, &guid_connex, sizeof(guid_connex), 
		(LPVOID)(&(mswsock_api)->_connectex), sizeof(LPFN_CONNECTEX), &_bytes, 
		&_api_ret);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT((mswsock_api)->_connectex != NULL);

	_ret = yh_get_extension_wsock_api(sock, &guid_acceptex, 
		sizeof(guid_acceptex), (LPVOID)(&(mswsock_api)->_acceptex), 
		sizeof(LPFN_ACCEPTEX), &_bytes, &_api_ret);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT((mswsock_api)->_acceptex != NULL);

	_ret = yh_get_extension_wsock_api(sock, &guid_disconex, 
		sizeof(guid_disconex), (LPVOID)(&(mswsock_api)->_disconnectex), 
		sizeof(LPFN_DISCONNECTEX), &_bytes, &_api_ret);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT((mswsock_api)->_disconnectex != NULL);

	_ret = closesocket(sock);
	YH_CHECK_VALUE(_ret);

	if (_ret != YH_SUCCESS) {
		_ret = YH_ERR_SOCKET_ENV_INVALID;
	}

	return _ret;
}

_int32 yh_get_wsock_api_table(MSWSockApi *table)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(table);

	if (gMSWsockEnvInit) {
		*table = gMSWsockApi;
	}
	else {
		YH_ASSERT(FALSE);
		memset(table, 0, sizeof(MSWSockApi));
		_ret = YH_ERR_SOCKET_ENV_INVALID;
	}

	return _ret;
}
