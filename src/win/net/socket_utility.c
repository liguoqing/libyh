#include "net/socket_utility.h"
#include "base/define.h"


_int32 yh_socket_ip_ulong2str(_ulong lip, char *sip, _uint32 size)
{
    _int32 _ret = YH_SUCCESS;
    struct sockaddr_in _tmp_addr = {0};
    char *_tmp_ip = NULL;

    if (!sip || (size == 0)) return YH_ERR_INVALID_PARAM;

    _tmp_addr.sin_addr.s_addr = lip;

    _tmp_ip = inet_ntoa(_tmp_addr.sin_addr);
    if (_tmp_ip) {
        if (size < strlen(_tmp_ip)) return YH_ERR_INVALID_PARAM;

        memcpy(sip, _tmp_ip, strlen(_tmp_ip));
        
        LOG("origin ip:%lu, convert to:%s.", lip, sip);
    }
    else {
        _ret = YH_ERR_SOCKET_OP_FAILURE;
    }

    return _ret;
}


_int32 yh_socket_ip_str2ulong(const char *sip, _ulong *lip)
{
    _int32 _ret = YH_SUCCESS;

    if (!sip || !lip) return YH_ERR_INVALID_PARAM;

    *lip = inet_addr(sip);
    if (*lip == INADDR_NONE || *lip == INADDR_ANY) {
        _ret = YH_ERR_SOCKET_OP_FAILURE;
    }

    return _ret;
}
