#include "utility.h"
#include "io/iocp.h"
#include "net/socket_interface.h"


/**
 * 封装外部投递函数所用到的参数
 * @param _param 待执行的函数所需要的参数
 * @param _func 待要执行的函数
 **/
struct PostFunctionInnerParam_s {
	void						*_param;
	yh_post_function_handler	_func;
};

typedef struct PostFunctionInnerParam_s PostFunctionInnerParam;


static _int32 yh_post_function_msg_handler(void *param);


_int32 yh_post_function(EventLoop *reactor, 
						void *param, 
						yh_post_function_handler func)
{
	_int32 _ret = YH_SUCCESS;
	OverlappedWrapper *_func_op = NULL;
	PostFunctionInnerParam *_user_param = NULL;

	YH_MALLOC(_user_param, PostFunctionInnerParam *, 
		sizeof(PostFunctionInnerParam));
	_user_param->_param = param;
	_user_param->_func = func;

	_ret = yh_create_overlapped_wrapper(&_func_op, MT_POST_FUNC, 
		yh_post_function_msg_handler, 
		yh_message_default_delete_handler, (void *)_user_param);
	YH_CHECK_VALUE(_ret);

	_ret = yh_post_queued_completion_status(reactor->_iocp, 0, 0, 
		(LPOVERLAPPED)&(_func_op->_overlapped));
	YH_CHECK_VALUE(_ret);

	return _ret;
}


static _int32 yh_post_function_msg_handler(void *param)
{
	OverlappedWrapper *_op = NULL;
	PostFunctionInnerParam *_user_param = NULL;

	YH_CHECK_PARAM_VALID(param);
	
	_op = (OverlappedWrapper *)param;
	_user_param = (PostFunctionInnerParam *)(_op->_msg->_data);
	YH_ASSERT(_user_param != NULL);

	return _user_param->_func(_user_param->_param);
}
