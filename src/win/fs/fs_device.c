#include "fs/fs_device.h"
#include "base/singnal.h"


static _int32 _yh_fs_device_handle_open(void *data);
static _int32 _yh_fs_device_handle_close(void *data);
static _int32 _yh_fs_device_handle_read(void *data);
static _int32 _yh_fs_device_handle_write(void *data);


struct CreateFileFlag_s {
    DWORD   _access;
    DWORD   _disposition;
};

typedef struct CreateFileFlag_s CreateFileFlag;

/**
{FO_READ, {GENERIC_READ, OPEN_EXISTING}}, 
{FO_WRITE, {GENERIC_WRITE, CREATE_ALWAYS}},
{FO_APPEND, {FILE_GENERIC_WRITE, OPEN_ALWAYS}},
{FO_READ_U, {FILE_GENERIC_READ | FILE_GENERIC_WRITE, OPEN_EXISTING}},
{FO_WRITE_U, {FILE_GENERIC_READ | FILE_GENERIC_WRITE, CREATE_ALWAYS}},
{FO_APPEND_U, {FILE_GENERIC_READ | FILE_GENERIC_WRITE, OPEN_ALWAYS}}
 **/
static CreateFileFlag FSDeviceCreateFlagWinCreateFlagMap[] = {
    {GENERIC_READ, OPEN_EXISTING}, 
    {GENERIC_WRITE, CREATE_ALWAYS},
    {FILE_GENERIC_WRITE, OPEN_ALWAYS},
    {FILE_GENERIC_READ | FILE_GENERIC_WRITE, OPEN_EXISTING},
    {FILE_GENERIC_READ | FILE_GENERIC_WRITE, CREATE_ALWAYS},
    {FILE_GENERIC_READ | FILE_GENERIC_WRITE, OPEN_ALWAYS}};


_int32 yh_fs_device_create(EventLoop *loop, 
						   void *user_data, 
                           FSDevice **device)
{
	_int32 _ret = YH_SUCCESS;

	YH_CHECK_PARAM_VALID(loop);

	YH_MALLOC(*device, FSDevice *, sizeof(FSDevice));

    _ret = yh_device_create(loop, INVALID_HANDLE_VALUE, FS_DEVICE, 
        &((*device)->_device));
    YH_CHECK_VALUE(_ret);

    _ret = yh_queue_create(&((*device)->_pending_reqs));
    if (_ret != YH_SUCCESS) {
        YH_DELETE_OBJ(*device);
        *device = NULL;
        return YH_ERR_QUEUE_CREATE_FAILURE;
    }

    (*device)->_wait_close = CreateEvent(NULL, FALSE, FALSE, NULL);
    (*device)->_state = FDS_IDLE;
	(*device)->_user_data = user_data;

	LOG("_ret:%d, device:0x%x.", _ret, *device);

    return _ret;
}


_int32 yh_fs_device_destroy(FSDevice *device)
{
    _int32 _ret = YH_SUCCESS;

    YH_CHECK_PARAM_VALID(device);

    if (device->_state != FDS_CLOSED && device->_state != FDS_CLOSING) {
        return YH_ERR_FS_DEVICE_WRONG_STATE;
    }

    if (device->_state == FDS_CLOSING) {
        _ret = yh_wait(device->_wait_close);
    }

    CloseHandle(device->_wait_close);

    _ret = yh_queue_destroy(device->_pending_reqs);
    YH_CHECK_VALUE(_ret);

	if (device->_device->_fd != INVALID_HANDLE_VALUE) {
		CloseHandle(device->_device->_fd);
	}
    
    _ret = yh_device_destroy(device->_device);
    YH_CHECK_VALUE(_ret);

    YH_DELETE_OBJ(device);

    return _ret;
}


_int32 yh_fs_device_open(FSDevice *device, 
                         FSDeviceCreateFlag flag,
                         YHString file_path, 
                         YHString file_name,
                         void *user_data,
                         yh_fs_device_open_callback handler)
{
    _int32 _ret = YH_SUCCESS;
	DWORD _desired_access = 0;
    DWORD _create_disposition = 0;
    HANDLE _fd = INVALID_HANDLE_VALUE;
	_int32 _errcode = YH_SUCCESS;
    char _tmp_file_full_path[MAX_FILE_FULL_PATH_LEN] = {0};
	OverlappedWrapper *_op = NULL;
	FsOpenReq *_req = NULL;
	LARGE_INTEGER _file_size = {0};
	BOOL _api_ret = FALSE;

#if defined(_DEBUG)
	{
		char _tmp_file_name[MAX_FILE_FULL_PATH_LEN] = {0};
		memcpy(_tmp_file_name, file_name._str, file_name._len);
		LOG("device:0x%x, file_name:%s.", device, _tmp_file_name);
	}	
#endif

    YH_CHECK_PARAM_VALID(device);
    YH_CHECK_PARAM_VALID(file_name._str);
    YH_CHECK_PARAM_VALID(file_path._str);
    YH_CHECK_PARAM_VALID(handler);
    YH_ASSERT(file_name._len + file_path._len < MAX_FILE_FULL_PATH_LEN);

	_desired_access = FSDeviceCreateFlagWinCreateFlagMap[flag]._access;
    _create_disposition = FSDeviceCreateFlagWinCreateFlagMap[flag]._disposition;

    memcpy(_tmp_file_full_path, file_path._str, file_path._len);
    memcpy(_tmp_file_full_path+file_path._len, file_name._str, file_name._len);

    // FIXME
    // 偶然看到了一篇文章说是文件缓存对异步操作的性能影响比较大(吞吐量)
    // 本质原因是缓存的数据不能异步，读取的时候还是会阻塞线程导致性能低
    // 所以需要在这里加上FILE_FLAG_NO_BUFFERING
    _fd = CreateFile(_tmp_file_full_path, _desired_access, 0, NULL, 
        _create_disposition, FILE_FLAG_OVERLAPPED, 0);
    //YH_ASSERT(_fd != INVALID_HANDLE_VALUE);

	if (_fd == INVALID_HANDLE_VALUE) {
		_errcode = GetLastError();
		_ret = YH_ERR_CANOT_OPEN_FILE;
	}
	else {
		_ret = yh_device_reset_fd(device->_device, _fd);
		YH_CHECK_VALUE(_ret);
		YH_ASSERT(_fd == device->_device->_fd);

		_api_ret = GetFileSizeEx(_fd, &_file_size);
		YH_ASSERT(_api_ret == TRUE);
		if (!_api_ret) {
			_errcode = GetLastError();
			LOG("GetFileSizeEx error:%d.", _errcode);
			_ret = YH_ERR_CANOT_GET_FILE_SIZE;
		}

		device->_file_size = (_uint64)_file_size.QuadPart;
	}

	_ret = yh_create_fs_open_req(&_op, NULL, (IODevice *)device, 
        (void *)handler, user_data, &_req);
	YH_CHECK_VALUE(_ret);
	YH_ASSERT(_op != NULL);

	_op->_err_code = _errcode;
    _op->_trans_bytes = 0;

    _ret = _yh_fs_device_handle_open((void *)_op);
    YH_CHECK_VALUE(_ret);

	_ret = yh_destroy_overlapped_wrapper(_op);
	YH_CHECK_VALUE(_ret);

    return _ret;
}


_int32 yh_fs_device_read(FSDevice *device, 
                         _uint64 start_pos, 
                         _uint32 exp_read_len,
                         char *buffer, 
                         _uint32 buf_len, 
                         void *user_data,
                         yh_fs_device_read_callback handler)
{
    _int32 _ret = YH_SUCCESS;
    BOOL _api_ret = FALSE;
    OverlappedWrapper *_op = NULL;
    FsReadReq *_req = NULL;
    DWORD _read_len = 0, _last_err = 0;

    LOG("device:0x%x, start_pos:%llu, exp_read_len:%u.", device, 
        start_pos, exp_read_len);

    YH_CHECK_PARAM_VALID(device);
    YH_CHECK_PARAM_VALID(buffer);
    YH_CHECK_PARAM_VALID(handler);
    YH_ASSERT(buf_len > 0);
    YH_ASSERT(exp_read_len > 0);
    YH_ASSERT(device->_state == FDS_WORKING);

	device->_exp_read_len = exp_read_len;
	device->_read_buf = buffer;

    _ret = yh_create_fs_read_req(&_op, _yh_fs_device_handle_read, 
        (IODevice *)device, start_pos, (void *)handler, user_data, &_req);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_op != NULL);
    YH_ASSERT(_req != NULL);

    _op->_overlapped.Offset = (DWORD)(start_pos & 0x0000000FFFFFFFF);
    _op->_overlapped.OffsetHigh 
        = (DWORD)((start_pos & 0xFFFFFFFF00000000) >> 32);

	_api_ret = ReadFile(device->_device->_fd, (LPVOID)device->_read_buf, 
        (DWORD)exp_read_len, &_read_len, &(_op->_overlapped));

	if (!_api_ret) {
		_last_err = GetLastError();
		if (_last_err != ERROR_IO_PENDING) {
            goto READ_ERROR;
		}
	}

    return _ret;

READ_ERROR:
    LOG("read error:%d.", _last_err);

    _op->_err_code = _last_err;
    _op->_trans_bytes = 0;

    _ret = _yh_fs_device_handle_read((void *)_op);
    YH_CHECK_VALUE(_ret);

    _ret = yh_destroy_overlapped_wrapper(_op);
    YH_CHECK_VALUE(_ret);

    _ret = YH_ERR_READ_FILE_FAIL;

    return _ret;
}


_int32 yh_fs_device_write(FSDevice *device, 
                          _uint64 start_pos,
                          _uint32 exp_write_len,
                          char *data, 
                          void *user_data,
                          yh_fs_device_write_callback handler)
{
    _int32 _ret = YH_SUCCESS;
    BOOL _api_ret = FALSE;
    OverlappedWrapper *_op = NULL;
    FsWriteReq *_req = NULL;
    DWORD _write_len = 0, _last_err = 0;

    YH_CHECK_PARAM_VALID(device);
    YH_CHECK_PARAM_VALID(data);
    YH_ASSERT(exp_write_len > 0);
    YH_ASSERT(device->_state == FDS_WORKING);
    YH_ASSERT(device->_device->_fd != INVALID_HANDLE_VALUE);

    LOG("device:0x%x, start_pos:%llu, exp_write_len:%u.", 
        device, start_pos, exp_write_len);

	device->_exp_write_len = exp_write_len;
	device->_write_buf = data;

    _ret = yh_create_fs_write_req(&_op, _yh_fs_device_handle_write, 
        (IODevice *)device, start_pos, (void *)handler, user_data, &_req);
    YH_CHECK_VALUE(_ret);
    YH_ASSERT(_op != NULL);
    YH_ASSERT(_req != NULL);

    _op->_overlapped.Offset = (DWORD)(start_pos & 0x0000000FFFFFFFF);
    _op->_overlapped.OffsetHigh 
        = (DWORD)((start_pos & 0xFFFFFFFF00000000) >> 32);

    _api_ret = WriteFile(device->_device->_fd, (LPVOID)device->_write_buf, 
        (DWORD)exp_write_len, &_write_len, &(_op->_overlapped));

    if (!_api_ret) {
        _last_err = GetLastError();
        if (_last_err != ERROR_IO_PENDING) {
            goto WRITE_ERROR;
        }
    }

    return _ret;

WRITE_ERROR:
    LOG("write error:%d.", _last_err);

    _op->_err_code = _last_err;

    _ret = _yh_fs_device_handle_write((void *)_op);
    YH_CHECK_VALUE(_ret);

    _ret = yh_destroy_overlapped_wrapper(_op);
    YH_CHECK_VALUE(_ret);

    _ret = YH_ERR_WRITE_FILE_FAIL;
    
    return _ret;
}


_int32 yh_fs_device_close(FSDevice *device,
                          void *user_data,
                          yh_fs_device_close_callback handler)
{
    _int32 _ret = YH_SUCCESS;
    BOOL _api_ret = FALSE;
    OverlappedWrapper *_op = NULL;
    FsCloseReq *_req = NULL;
    DWORD _last_err = 0;

    YH_CHECK_PARAM_VALID(device);
    YH_CHECK_PARAM_VALID(handler);

    _ret = yh_create_fs_close_req(&_op, _yh_fs_device_handle_close, 
        (IODevice *)device, (void *)handler, user_data, &_req);
    YH_CHECK_VALUE(_ret);
	_req->_base->_user_data = device->_user_data;

    if (device->_state == FDS_WORKING) {
        /**
		 * TODO：
		 * 处理iocp因为cancel io产生的错误
         * All I/O operations that are canceled complete with the error 
         * ERROR_OPERATION_ABORTED, and all completion notifications for 
         * the I/O operations occur normally.
         **/
        _api_ret = CancelIo(device->_device->_fd);
        if (_api_ret == FALSE) {
            _ret = YH_ERR_FS_DEVICE_CANT_CANCEL;
			_last_err = GetLastError();
			LOG("CancelIo error, errcode:%d.", _last_err);
        }
    } 

	if (_ret == YH_SUCCESS) {
		_api_ret = CloseHandle(device->_device->_fd);
		if (_api_ret) {
			device->_device->_fd = INVALID_HANDLE_VALUE;
		}
		else {
			_ret = YH_ERR_CLOSE_FILE_FAIL;
			_last_err = GetLastError();
			LOG("CloseHandle error, errcode:%d.", _last_err);
		}
	}

	/**
	 * 向上层通知错误
	 **/
    if (_ret != YH_SUCCESS) {
        _op->_err_code = _last_err;
		device->_state = FDS_CLOSING;
    }
	else {
		device->_state = FDS_CLOSED;
	}

    _ret = _yh_fs_device_handle_close((void *)_op);
    YH_CHECK_VALUE(_ret);

    _ret = yh_destroy_overlapped_wrapper(_op);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 _yh_fs_device_handle_open(void *data)
{
	_int32 _ret = YH_SUCCESS;
    OverlappedWrapper *_op = NULL;
    FsOpenReq *_req = NULL;
    FSDevice *_device = NULL;
    yh_fs_device_open_callback _callback = NULL;

    YH_CHECK_PARAM_VALID(data);
    
    _op = (OverlappedWrapper *)data;
    _req = (FsOpenReq *)_op->_msg->_data;
    YH_ASSERT(_req != NULL);
    
	_req->_base->_op = NULL;
	_device = (FSDevice *)_req->_base->_device;
	YH_ASSERT(_device != NULL);

    _req->_base->_err_code = _op->_err_code;

	LOG("device:0x%x, _req:0x%x, errcode:%d, send:%d.", 
        _device, _req, _req->_base->_err_code, _op->_trans_bytes);

    if (_op->_err_code != YH_SUCCESS) {
        _device->_state = FDS_FAILED;
    }
	else {
		_device->_state = FDS_WORKING;
	}

    _callback = (yh_fs_device_open_callback)_req->_base->_callback;
    YH_ASSERT(_callback != NULL);

    _ret = _callback(_req);

    _ret = yh_destroy_fs_open_req(_req);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 _yh_fs_device_handle_close(void *data)
{
    _int32 _ret = YH_SUCCESS;
    OverlappedWrapper *_op = (OverlappedWrapper *)data;
    FsCloseReq *_req = NULL;
    FSDevice *_device = NULL;
    yh_fs_device_close_callback _callback = NULL;

    YH_CHECK_PARAM_VALID(data);

    _req = (FsCloseReq *)_op->_msg->_data;
    _device = (FSDevice *)_req->_base->_device;
    _req->_base->_op = NULL;
    _req->_base->_err_code = _op->_err_code;

    LOG("_req:0x%x, errcode:%d, send:%d.", 
        _req, _req->_base->_err_code, _op->_trans_bytes);

    if (_op->_err_code != YH_SUCCESS) {
        _device->_state = FDS_FAILED;
    }
    else {
        _device->_state = FDS_CLOSED;
    }

    _callback = (yh_fs_device_close_callback)_req->_base->_callback;
    YH_ASSERT(_callback != NULL);

    _ret = _callback(_req);

    _ret = yh_destroy_fs_close_req(_req);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 _yh_fs_device_handle_read(void *data)
{
    _int32 _ret = YH_SUCCESS;
    OverlappedWrapper *_op = (OverlappedWrapper *)data;
    FsReadReq *_req = NULL;
    FSDevice *_device = NULL;
    yh_fs_device_read_callback _callback = NULL;

    YH_CHECK_PARAM_VALID(data);

    _req = (FsReadReq *)_op->_msg->_data;
    _device = (FSDevice *)_req->_base->_device;
    _req->_base->_op = NULL;
    _req->_base->_err_code = _op->_err_code;
	_req->_read_len = _op->_trans_bytes;

	YH_ASSERT(_device != NULL);
	YH_ASSERT(_device->_state == FDS_WORKING);

    LOG("_req:0x%x, errcode:%d, read:%d.", 
        _req, _req->_base->_err_code, _op->_trans_bytes);

    if ((_op->_err_code != YH_SUCCESS) && 
        (_op->_err_code != ERROR_HANDLE_EOF)) {
        _device->_state = FDS_FAILED;
    }

    if (_op->_err_code == ERROR_HANDLE_EOF) {
        _req->_base->_err_code = YH_ERR_READ_FILE_EOF;
    }

    YH_ASSERT(_op->_trans_bytes <= _device->_exp_read_len);

    _callback = (yh_fs_device_read_callback)_req->_base->_callback;
    YH_ASSERT(_callback != NULL);

    _ret = _callback(_req);

    _ret = yh_destroy_fs_read_req(_req);
    YH_CHECK_VALUE(_ret);

    return _ret;
}


static _int32 _yh_fs_device_handle_write(void *data)
{
    _int32 _ret = YH_SUCCESS;
    BOOL _api_ret = FALSE;
    OverlappedWrapper *_op = (OverlappedWrapper *)data;
    FsWriteReq *_req = NULL;
    FSDevice *_device = NULL;
    yh_fs_device_write_callback _callback = NULL;

    YH_CHECK_PARAM_VALID(data);

    _req = (FsWriteReq *)_op->_msg->_data;
    _device = (FSDevice *)_req->_base->_device;
    _req->_base->_op = NULL;
    _req->_base->_err_code = _op->_err_code;
	_req->_write_len = _op->_trans_bytes;

    LOG("_req:0x%x, errcode:%d, send:%d.", 
        _req, _req->_base->_err_code, _op->_trans_bytes);

    if (_op->_err_code != YH_SUCCESS) {
        _device->_state = FDS_FAILED;
    }
    else {
        _api_ret = FlushFileBuffers(_device->_device->_fd);
        YH_ASSERT(_api_ret == TRUE);
    }

    YH_ASSERT(_device->_state == FDS_WORKING);
    YH_ASSERT(_op->_trans_bytes <= _device->_exp_write_len);

    _callback = (yh_fs_device_write_callback)_req->_base->_callback;
    YH_ASSERT(_callback != NULL);

    _ret = _callback(_req);

	// NOTE
	// 这之后不要再操作device了，因为device可能已经无效了
	// 一定要弄清楚各个对象的生命周期，这非常关键！
    _ret = yh_destroy_fs_write_req(_req);
    YH_CHECK_VALUE(_ret);

    return _ret;
}
